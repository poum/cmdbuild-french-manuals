# GIS

## Gestion des icones

Cette fonctionnalité vous permet de créer et de mettre à jour des icones qui sont utilisés pour représenter des marqueurs.

![](../administrator_manual/19-GIS-1.md)

Il est possible de réaliser les opérations suivantes:

- importer un nouvel icone ![](../administrator_manual/19-GIS-2.md)
- modifier les propriétés d'un icone existant ![](../administrator_manual/19-GIS-3.md)
- supprimer un icone ![](../administrator_manual/19-GIS-4.md)

## Services externes

Cette fonctionnalité vous permet d'activer des services externes nécessaires pour la représentation sur une carte.

Les services externes actuellement pris en charge sont:

- Open Street Map ,Google Maps et Yahoo! Maps
- Serveur GIS (GeoServer) pour importer et afficher des données raster et vectorielles (plans, etc.)

Pour les services cartographiques, il est possible de fixer des niveaux de zoom minimum et maximum disponibles dans CMDBuild.

Pour utiliser GeoServer, vous devez indiquer:

- l'URL
- le nom de l'espace de travail
- l'identifiant de l'administrateur
- le mot de passe de l'administrateur

![](../administrator_manual/19-External_services-1.md)

## Ordre des calques

Cette fonctionnalité vous permet de définir l'ordre des calques (il y un calque pour chaque attribut géographique défini).

L'ordre des calques peut être modifié en déplaçant les lignes dans la grille (par tirer-déplacer).

![](../administrator_manual/19-Layout_order-1.md)

## Calque GeoServer

Cette fonctionnalité vous permet de:

- importer dans GeoServer les calques sur vous voulez afficher dans CMDBuild
- modifier l'ordre des calques

Pour importer un nouveau calque, vous devez saisir:

- le nom
- la description
- le fichier calque
- le type de fichier (GeoTiff, WorldImage, Shape)
- le niveau minimum de zoom
- le niveau maximum de 
- les classes et fiches de CMDBuild pouvant être associées à ce calque, ce qui est utile dans le module de gestion pour insérer le calque dans la liste des éléments contenus dans un élément du menu de navigation hiérarchique de GIS, qui est décrit dans le paragraphe suivant (ex: pour trouver le calque GeoServer parmi les éléments liés à une fiche "Etage")

L'ordre des calques peut être modifié en déplaçant les lignes dans la grille (par tirer-déplacer).

![](../administrator_manual/19-Geoserver_layer-1.md)

## Navigation GIS

La fonction permet la définition de l'arbre du domaine, qui sera ensuite fourni à l'utilisateur dans le module de gestion de façon à l'aider à naviguer dans les différentes types de classes / fiches géoréférencés sur la carte.

En d'autres mots, cette fonction définit la structure du menu hiérarchique de façon à aller d'une classe à celles qu'elles contient (en termes spatiaux).

Une classe de plus haut niveau peut inclure plusieurs classes de plus bas niveau, par exemple une pièce peut inclure des éléments d'information, des jobs (entendus comme des affectations ou des regoupement logiques d'éléments), des fournitures (tables, chaises, armoires, équipements techniques, images), etc.

En fournissant les domaines nécessaires, vous trouverez ci-dessous un exemple de configuration constituée des classes suivantes:

"Batiment" =>
  "Etage" => 
    "Pièce" =>
      "Element"

Via le contrôle de "niveau par défaut", il est exigé que vous marquiez le calque "développement vertical" qui est celui qui peut réaliser des surcharges et a besoin d'être affiché sélectivement (hbituellement lié au calque "Etages").

![](../administrator_manual/19-GIS_navigation-1.md)
