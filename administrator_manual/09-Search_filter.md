# Filtres de recherche

Les filtres de recherche peuvent être utilisés dans le module de gestion de façon à n'afficher que les fiches de données qui correspondent aux critères définis.

Dans le module de gestion, chaque utilisateur peut enregistrer de manière autonomes ces filtres de recherche utilisés pour se référer les fiches de données d'une classe et les trouver en accédant à l'application.

Cette fonction du module d'administration permet à l'administrateur système pour créer des filtres de recherche qui peuvent être activés pour un ou plusieurs groupes d'utilisateurs (ils se trouveront dans les filtres disponibles dans le module de gestion) selon l'une des modalités suivantes:

- en réutilisant et en clonant un filtre défini par un utilisateur
- en définissant un nouveau filtre de recherche, i.e. en définissant des critères de recherche sur les attributs de la classe source indiquée

Il est possible de réaliser les opérations suivantes:

- initier la création d'un nouveau filtre ![](../administrator_manual/08-Filter_based_on_sql-1.jpg)
- modifier un filtre pré-existant ![](../administrator_manual/08-Filter_based_on_sql-2.jpg)
- supprimer un filtre pré-existant ![](../administrator_manual/08-Filter_based_on_sql-3.jpg)
- ouvrir une fenêtre surgissante dans laquelle choisir un filtre utilisateur ou en créer un nouveau ![](../administrator_manual/08-Filter_based_on_sql-4.jpg)
- supprimer les critères actuels de filtre ![](../administrator_manual/08-Filter_based_on_sql-5.jpg)

Pour créer ou modifier un filtre de recherche, les informations suivantes sont requises:

- "Nom" - nom du filtre (ex: "PCMarqueDell")
- "Description" - description du filtre (ex: "PC de marque DELL")
- "Classe source" - classe sur laquelle le filtre peut être appliqué (ex: PC)

Un nouveau filtre de recherche ne peut pas être utilisé si les droits d'accès n'ont pas été définis, e indiquant quel(s) groupe(s) peut/peuvent les utiliser.

Une telle opération doit être réalisée en utilisant la fonction "Utilisateurs et groupes" / "Droits" / "Filtres de recherche".

Ci-dessous vous trouverez deux captures d'écran qui décrivent l'interface utilisateur fournie pour la gestion des filtres de recherche.

![](../administrator_manual/09-Search_filters-1.jpg) 

![](../administrator_manual/09-Search_filters-2.jpg) 
