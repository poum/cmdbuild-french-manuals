# Gestion des processus

## Informations générales

CMDBuild est un système de gestion pour le département IT et prend en charge les "bonnes pratiques" ITIL; CMDBuild modélise et gère la base de données de gestion de la configuration et prend en charge les processus liés.

Etant donné le nombre d'options des processus, les procédures organisationnelles et la flexibilité recherchée par le projet CMDBuild, nous avons choisi de ne pas implémenter une série de processus rigides et prédéfinis, mais plutôt un moteur générique de processus pour modéliser les processus au cas par cas.

Le système de gestion de processus est une fonctionnalité importante de CMDBuild qui fournit:

- une interface standardisée pour les utilisateurs
- une mise à jour sécurisée de la CMDB
- un outil pour superviser les services fournis
- un dépôt pour les données d'activité, utile pour contrôler les SLA

## Modes de représentation

Dans CMDBuild, chaque processus contient des informations concernant:

- les séquences d'activités, avec des flux conditionnels
- les données visibles que l'utilisateur doit renseigner à chaque étape
- les tâches exécutées (processus démarrés, mise à jour des bases de données, envoi de mail, etc.)
- les rôles autorisés à exécuter chaque étape des processus

Les processus sont conçus en utilisant l'éditeur graphique JPEd, un outil externe open source qui utilise le standard XPDL puis importés dans CMDBuild et exécutés avec le moteur Enhydra Shark.

Le "glossaire" utilisé dans CMDBuild comprend les termes suivants:

- processus (ou workflow): séquence d'étapes ("activités") pour exécuter une action spécifique conformément à des règles spécifiques à l'aide d'un assistant
- activité: une étape de processus
- instance de processus: processus actif créé en exécutant la première étape
- instance d'activité: création d'une activité, accomplie automatiquement ou par un opérateur

Les termes précédents sont arrangés dans CMDBuild comme suit:

- chaque processus est représenté par une classe spéciale configurée en utilisant le menu "Processus" dans le module d'administration; la classe comporte tous les attributs des activités planifiées
- chaque instance de processus correspond à la fiche courante, plus la liste de ses versions (activités achevées)
- chaque instance d'activité correspond à une fiche (activité en cours) ou à une version historisée (activité achevée)

Modéliser un nouveau processus dans CMDBuild nécessite la configuration d'une nouvelle classe "processus" qui comporte tous les attributs des activités planifiées.

Pour tous les détails concernant la configuration des processus, référez-vous au manuel "Tutoriel relatif aux processus".

Ce manuel ne décrit que la façon de configurer une classe "Processus", en décrivant en particulier comment utiliser les onglets "XPDL" et "Planification"; ces onglets constituent la principale difféence entre une classe standard et une classe de processus.

![](../administrator_manual/06-Representation_modes-1.jpg)

## Onglet Propriétés

L'onglet de propriété des processus diffère de celui des propriétés des classes en ce qu'il fournit d'autres sections de données qui permettent d'importer/exporter des fichiers XPDL; vous pouvez utiliser cet onglet pour télécharger la définition XPDL initiale et, une fois le processus conçu, importer votre fichier.

![](../administrator_manual/06-Properties_tab-1.jpg)

### Importer XPDL

Cette fonction vous permet d'importer le fichier XPDL que vous avez conçu et configuré en utilisant l'éditeur externe TWE (ou JPEd).

Les informations prises en charge par CMDBuild sont:

- processus arrêtable par l'utilisateur (activé / désactivé)
- fichier XPDL
- schéma du processus (image)

Une fois terminée les opérations d'import décrites précédemment, le nouveau processus est inclus dane le module de gestion de telle sorte qu'il peut être exécuté par le moteur de processus "Enhydra Shark".

Vous pouvez modifier un processus que vous avez déjà importé.

### Télécharger un modèle XPDL

Cette fonctionnalité vous permet d'exporter le modèle XPDL pour commencer à travailler la structure de processus en utilisant un éditeur externe (JPEd).

Le fichier exporté comprend:

- nom du processus
- la liste des attributs du processus (correspondant à la liste des attributs définis dans la classe)
- la liste des méthodes disponibles (pour l'application CMDBuild)
- la liste des "acteurs" (utilisateurs) qui interagissent avec le processus (le rôle "Système" est automatiquement créé pour identifier les activités du système); la liste correspond aux groupes d'utilisateurs définis dans CMDBuild
- description du type de données pour les attributs personnalisés qui sont des listes de valeurs ou des références

Les informations gérées pour cette fonction sont:

- nom de la classe du processus (mode lecture seule)
- processus arrêtable par l'utilisateur (activé / désactivé)

La même fonction peut être utilisée pour télécharger le processus courant et modifier les étapes du processus en utilisant un éditeur externe.

### Onglet du gestionnaire de tâches

L'onglet "Gestionnaire de tâches" vous permet d'accéder aux fonctions de l'entrée de menu de façon à programmer l'exécution du processus courant.

![](../administrator_manual/06-Task_manager_tab-1.jpg)

Par la commande "Ajouter un processus", vous basculerez vers la fonction de Gestionnaire de tâches  et son assistant pour la programmation du processus actuellement démarré.

Reportez-vous au chapitre concernant le gestionnaire de tâches pour plus de détails à ce sujet.

