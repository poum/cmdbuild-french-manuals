# Rapport

CMDBuild vous permet d'importer des rapports personnalisés avec des outils externes puis de les incure dans le module de gestion.

## JasperReports

CMDBuild peut importer des rapports créés avec iReport (éditeur graphique) qui fait partie du projet open source JasperReports.

Les fonctionnalités iReport comprennent:

- options de mise en forme du texte (polices de caractères, alignement du texte, espace, couleurs, etc.)
- définition des éléments standards (titres, pied de page, titre des colonnes, sommaire, etc.)
- regroupement des données
- évaluation d'expressions
- champs calculés
- gestion avancée des sous-rapports
- images et graphiques
- formats PDF, HTML, XLS et CSV

La fonction d'import gère également les rapports contenant des sous-rapports et images, dont le chargement est géré en utilisant un assistant.

![](../administrator_manual/13-Report-1.jpg)

Pour créer un nouveau rapport, vous devez saisir les informations suivantes:

- "Nom du rapport" - le nom apparaîtra dans la liste des rapports disponibles
- "Description du rapport" - la description apparaîtra dans la liste des rapports disponibles
- "Groupes autorisés" - les groupes (en plus de l'administrateur système) autorisés à exécuter le rapport
- "Rapport maître" - le fichier modèle créé avec iReport

Si le rapport maître contient des sous-rapports ou des images (tous les deux gérés par iReport en tant que fichiers externes), il sera demandé à l'administrateur d'importer les fichiers requis.

Le rapport que vous trouvez dans l'instance de démonstration comporte deux logos pour lesquels des images sont requises:

![](../administrator_manual/13-Report-2.jpg)

Une fois l'import réalisé, le rapport peut être exécuté en utilisant le module de gestion du menu Rapport.

Chaque rapport peut également être inséré dans le menu de navigation des groupes retenus pour l'utiliser. Il pourra alors être exécuté directement depuis ce menu.
