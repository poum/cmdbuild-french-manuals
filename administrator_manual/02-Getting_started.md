#Démarrage rapide

##La philosophie de CMDBuild

Une CMDB (base de données de gestion de la configuration ou Configuration management database) est un système de stockage et de consultation qui gère les éléments d'information d'une compagnie.
C'est le dépôt central officiel qui fournit une vue cohérente des services technologiques (IT).
C'est un sustème dynamique qui représente la situation actuelle et la connaissance des éléments d'information technologiques et les entités associées: matériel (ordinateurs, périphériques, réseau, équipements téléphoniques), logiciels (de base, d'environnement, logiciels applicatifs) documents (projets, contrats, manuels) et autres ressources, internes et externes.

C'est un système de supervision des processus exécutés, décrits et gérés via les fonctions de workflow).

CMDBuild est une solution de CMDB robuste, personnalisable et extensible.

Fournir une solution extensible signifie fournir un système ouvert et dynamique qui peut facilement être conçu, configuré et étendu par l'administrateur système en différentes phases en termes d'objets à gérer, d'attributs et de relations.

Puisqu'il n'y a pas deux organisations qui fonctionnent exactement sur le même ensemble d'objets (éléments ou assets) et, pour chaque objet, sur les mêmes informations, nous avons décidé de définir, en tant que fonctionnalité primordiale de CMDBuild, la flexibilité du système en développant des fonctionnalités permettant de configurer le système entier: modèle de données, processus, rapports, connecteurs aux systèmes externes, etc.

En particulier, les mécanismes de configuration implémentés dans CMDBuild et gérés via le module d'administration permettent de:

- ajouter vos propres nouvelles "classes", i.e. de nouvelles typologies d'éléments ou de processus (correspondant tous les deux à des tables en base de données)
- ajouter / modifier les "attributs" d'une classe, y compris géométriques (colonnes de la base de données)
- définir et importer des processus personnalisés à la fois pour la gestion des processus IT et les assistants de gestion des données
- ajouter / modifier les widgets à placer sur la gestion des fiches grâces auxquels les fonctions d'application - utiles pour ce type d'objets - peuvent être exécutées
- ajouter des "domaines" ou "relations" entre les classes (associations N:M entre tables)
- ajouter / modifier les "attributs" d'un "domaine"
- créer des filtres prédéfinis utilisés pour faire référence à des données de diverses classes
- créer des "vues", en utilisant soit des filtres, soit des requêtes SQL, qui peuvent être utilisées d'une façon similaire aux classes
- définir des arbres de navigation (sous-éléments du graphe des "domaines")
- créer des tables-listes (lookup ou listes de valeurs) pour gérer des attributs avec des valeurs prédéfinies
- afficher les détails du modèle de données
- configurer des tableaux de bord
- définir et importer des rapports personnalisés
- configurer des menus personnalisés pour différents groupes d'utilisateurs
- définir des rôles et des droits pour diverses catégories de "classes"
- configurer les diverses opérations exécutées en tâche de fond par le gestionnaire de tâches
- définir des modèles de notification par messagerie électronique
- configurer des classes géoréférencées GIS et BIM
- définir des options et des paramètres du système

Voici un schéma qui explique les termes et les concepts introduit ci-dessus en relation avec la configuration du modèle de données.

![Concepts de CMDBuild](../administrator_manual/02-concepts.png)

##Critères de conception

D'abord, il est important de:

- choisir un niveau de détail adapté aux besoins de l'organisation, des ressources humaines, financières, aux informations et aux technologies disponibles.
- identifier et impliquer l'équipe qui configurera (administrateurs) et mettre à jour (opérateurs) les informations dans le système; un système qui n'est pas à jour produit des coûts et aucuns résultats
- introduire de nouvelles applications dans un système organisationnel basé sur des procédures, des rôles et des responsabilités qui aident la structure IT à gérer correctement l'information
- prendre en considération le modèle des "bonnes pratiques" ITIL, un modèle qui est devenu, en peu de temps, un "standard de facto" pour la gestion des services

Un projet de CMDB qui réussit doit prendre en considération les impacts et les modifications introduites par le système et doit obtenir l'approbation explicite des dirigeants de l'organisation.

##Méthodologie de travail

Mettre en oeuvre CMDBuild nécessite une conception préliminaire pour gérer le schéma initial, i.e. identifier les sujets d'intérêt principaux et les options; ensuite, le système peut facilement être étendu selon les besoins à une date ultérieure.

![MCD](../administrator_manual/02-mcd.png)

Nous vous recommandons de commencer en gérant un ensemble complet et précis d'objets et de leurs relations puis l'étendre le système une fois que vous êtes devenu plus familiers avec les règles et usages de CMDBuild. En particulier, vous devriez identifier:

- les types d'objets ("classes"): matériel (ordinateurs, imprimantes, écrans, etc.), logiciel (systèmes d'exploitation, applications métier, logiciels de productivité), services (assistance aux utilisateurs, licences, etc.), entités (fournisseurs, sites géographiques, etc.)
- "attributs": utiles pour définir chaque classe (code, nom, type d'écran, taille de disque dur, etc.) - chaque attribut possède un type de donnée spécifique (chaîne de caractères, texte long, entier, nombre décimal, nombre à virgule flottante, date, liste "lookup", référence) et peut être un attribut géographique
- relations entre les classes
- les "attributs" sont utiles pour décrire chaque "domaine" (dépendance entre deux applications logicielles, nombre de ports d'un switch connectés à un serveur, relations entre un administrateur système et des serveurs, etc.) - chaque attribut possède un type de données spécifique (chaîne de caractères, texte long, entier, nombre décimal, nombre à virgule flottante, date, "lookup")
- processus organisationnels que le système doit gérer, décrits en terme d'activités, de règles de transition et "attributs" impliqués
- comptes utilisateurs

Une autre chose à prendre en considération est la hiérarchie des classes puisque, avec CMDBuild, il est possible de définir des classes génériques ("superclasses", par exemple "ordinateur") puis d'en dériver des sous-classes (par exemple, "poste de travail", "portable", "serveur"). Ces sous-classes partageront certaines informations communes (attributs de la superclasse) et définiront certains attributs spécifiques que la superclasse ne possède pas.

Il est important d'identifier une hiérarchie qui correspond aux besoins de l'organisation puisqu'une classe ne peut pas être automatiquement convertie en superclasse.
Une fois que le modèle entité-relation a été défini, vous devez définir des classes et les attributs et les types de données associés.

A la fin de l'opération, vous devriez:

- utiliser le module d'administration pour modéliser le système que vous avez conçu en utilisant les outils E-R
- utiliser le module de gestion pour insérer, mettre à jour et afficher des fiches

##Informations techniques

Pour davantage d'informations concernant le mappage entre le modèle de données logique de CMDBuild et le modèle physique de la base de données, voir le "Manuel technique".
Ce même manuel explique également comment installer l'application.

