# Tableau de bord

CMDBuild vous permet de configurer dans le système des pages "Tableaux de bord" qui contiennent chacuns plusieurs graphiques de différentes sortes; puis ils seront consultés dans le module de gestion (à la fois dans un nouveau menu accordéon spécifique et dans le menu de navigation).

Chaque tableau de bord peut adresser différents types d'aspects qui doivent être contrôlés: situation des éléments, performance du service d'assistance des utilisateurs, allocation des coûts, etc.

Chaque tableau de bord est constitué d'un certain nombre de graphiques qui peuvent adopter différents types de graphiques: camemberts, histogrammes, courbes, jauges.

Les données affichées dans les graphiques sont extraites du système en exécutant les fonctions PostgreSQL adaptées définies par l'administrateur selon un modèle particulier décrit ci-après.

## Onglet Propriétés

L'onglet "Propriétés" vous permet de créer un nouveau tableau de bord affichant des noms et des groupes pouvant le consulter.

![](../administrator_manual/12-Dashboard-1.jpg)

Il est possible de réaliser les opérations suivantes:

- créer un nouveau tableau de bord ![](../administrator_manual/12-Dashboard-2.jpg)
- modifier un tableau de bord pré-existant ![](../administrator_manual/12-Dashboard-3.jpg)
- supprimer un tableau de bord pré-existant ![](../administrator_manual/12-Dashboard-4.jpg)

Pour créer un nouveau tableau de bord, vous devez indiquer les informations suivantes:

- "Nom"
- "Description"
- "Groupes autorisés" - les groupes (en plus de l'administrateur système) autorisés à afficher le tableau de bord

## Onglet Graphique

L'onglet "Graphiques" vous permet de configurer les graphiques que vous afficher dans le tableau de bord choisi.

![](../administrator_manual/12-Dashboard-5.jpg)

Il est possible de réaliser les opérations suivantes:

- ajouter un nouveau graphique ![](../administrator_manual/12-Dashboard-6.jpg)
- modifier un graphique pré-existant ![](../administrator_manual/12-Dashboard-7.jpg)
- supprimer un graphique pré-existant ![](../administrator_manual/12-Dashboard-8.jpg)
- afficher un aperçu du graphique ![](../administrator_manual/12-Dashboard-9.jpg)

Pour configurer chaque graphique, vous devez:

- afficher certaines informations de base
- mettre en évidence la source de données (une fonction PostgreSQL qui doit déjà avoir été créée dans la base de données via les instructions montrées plus bas), dans laquelle le système liraa les données de façon à peupler le graphique
- configurer le mappage entre les paramètres de sortie de la fonction PostgreSQL et les paramètres d'entrée fournis par la typologie du graphique (affichant également s'ils peuvent être saisis au clavier et les widgets éventuels qui doivent être utilisés)
- indiquer le type de graphique (camembert, histogramme, courbe, jauge) qui détermine quels autres paramètres spécifiques seront demandés

L'exemple suivant (divisé en deux captures d'écrans) définit un ggraphique histogramme:

![](../administrator_manual/12-Dashboard-10.jpg)

![](../administrator_manual/12-Dashboard-11.jpg)

Voici un échantillon de résultat de la fonction "Aperçu":

![](../administrator_manual/12-Dashboard-12.jpg)

Les informations détaillées requises pour la configuration d'un graphique comprennent:

- "Nom" du nouveau graphique
- "Description" du nouveau graphique
- "Active", indique si le graphique est activé ou a été désactivé
- "Chargé automatiquement" indique s'il peut être immédiatement affiché sans la saisie de paramètres au clavier
- "Source de données" affiche la liste des fonctions PostgreSQL disponibles dans la définition du graphique
- la liste des paramètres de la source de données, avec la possibilité pour chacun d'entre eux d'indiquer:
   - s'il est obligatoire
   - son type (chaîne de caractères, entier, décimal, donnéee)
   -  dans le cas d'un paramètre chaîne de caractères, un sous-type est requis parmi: chaîne libre, "Id" d'une classe CMDBuild, "Id" d'une liste de valeurs et choix de la liste de valeurs concernée, "Id" d'une fiche et choix de la classe concernée
   - la valeur par défaut (valeur libre ou valeur à partir d'une liste, selon le type de données)
- "type de graphique", qui peut prendre l'une des valeurs suivantes: camembert, histogramme, courbe, jauge
- "afficher la légende" (si nécessaire, elle sera située sous le graphique)
- autres paramètres spécifiques à chaque type de graphique:
    - pour les camemberts:
        - champ valeur (pour le dimensionnement des zones)
        - champ libellé (cité dans les zones)
    - pour les histogrammes:
        - orientation (horizontal ou vertical)
        - titre du tableau de catégorie (libellé)
        - valeur du tableau de catégorie (information représentée par chaque barre)
        - titre du tableau de valeur (libellé)
        - tableau de valeur valeur / valeurs (pour le dimensionnement des barres, peut-être avec plus de séries superposées)
    - pour les courbes:
        - orientation (horizontal ou vertical)
        - titre du tableu de catégorie (libellé)
        - valeur du tableau de catégorie (information représentée par chaque point de la courbe)
        - titre du tableau de valeur (libellé)
        - tableau de valeur valeur / valeurs (pour la hauteur de la courbe, peut-être avec plus de séries superposées)
     - pour les jauges:
        - la valeur d'échelle maximum
        - la valeur d'échelle minimum (zéro si pas indiquée)
        - nombre d'intervals discrets
        - couleur de dessin
        - couleur de fond
        - champ valeur (de façon à définir l'indicateur affiché)

## Définition de la source de données (fonction PostgreSQL)

Afin que la configuration du système concernant les graphiques décrits précédemment fonctionne comme il convient, vous devez vous intéressez en particuliers à la définition de la fonction PostgreSQL qui représente la source de données.

En particulier;

- la fonction doit rapporter le commentaire "TYPE: function"
- la définition de la fonction doit inclure les paramètres en entrée et en sortie, affichant clairement le nom de chacun
- les paramètres en entrée et en sortie doivent être choisis parmi les suivants: "character varying", "boolean", "integer", "numeric", "double precision", "date", "time", "timestamp", "text" (pas "bigint")
- dans le cas où la fonction retourne plus de "tuples" que les valeurs de sortie, vous devez utiliser la syntaxe "Returns setof record"

A la fin de l'opération, vous devez exécuter la fonction "Configuration" > "Gestion du serveur" > "Effacer le cache" ou bien redémarrer Tomcat.

Ci-dessous se trouvent deux échantillons de fonctions PostgreSQL correctes, utilisées dans la base de données de démonstration:


    CREATE OR REPLACE FUNCTION cmf_active_cards_for_class(IN "ClassName" character varying,
    OUT "Class" character varying, OUT "Number" integer)
    RETURNS SETOF record AS
    $BODY$
    BEGIN
    RETURN QUERY EXECUTE
    'SELECT _cmf_class_description("IdClass") AS "ClassDescription", COUNT(*)::integer
    AS "CardCount"' ||
    '
    FROM ' || quote_ident($1) ||
    '
    WHERE "Status" = ' || quote_literal('A') ||
    '
    AND _cmf_is_displayable("IdClass")' ||
    '
    AND "IdClass" not IN (SELECT _cm_subtables_and_itself(_cm_table_id(' ||
    quote_literal('Activity') || ')))'
    '
    GROUP BY "IdClass"' ||
    '
    ORDER BY "ClassDescription"';
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100
    ROWS 1000;
    ALTER FUNCTION cmf_active_cards_for_class(character varying) OWNER TO postgres;
    COMMENT ON FUNCTION cmf_active_cards_for_class(character varying) IS 'TYPE: function';

    CREATE OR REPLACE FUNCTION cmf_count_active_cards(IN "ClassName" character varying, OUT
    "Count" integer)
    RETURNS integer AS
    $BODY$
    BEGIN
    EXECUTE 'SELECT count(*) FROM ' || quote_ident("ClassName") || ' WHERE "Status" = ' ||
    quote_literal('A') INTO "Count";
    END
    $BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100;
    ALTER FUNCTION cmf_count_active_cards(character varying) OWNER TO postgres;
    COMMENT ON FUNCTION cmf_count_active_cards(character varying) IS 'TYPE: function';

## Onglet Agencement

Quand les graphiques du tableau de bord sont définis, il est possible de définir leur agencement.

En particulier, vous pouvez choisir s'ils seront répartis sur une, deux ou trois colonnes et vous pourrez déplacer les graphiques présents dans ces colonnes (en utilisant la méthode drag'n'drop).

Il est également possible de déplacer un graphique d'un tableau de bord à un autre en le choisissant et en le tirant sur le nom de son nouveau tableau de bord (dans le menu accordéon à gauche).

Les fonctions disponibles sont les suivantes:

- ajouter une nouvelle colonne ![](../administrator_manual/12-Dashboard-13.jpg)
- supprimer une colonne, si elle est vide ![](../administrator_manual/12-Dashboard-14.jpg)
- personnaliser la distribution de l'espace entre les colonnes fournies ![](../administrator_manual/12-Dashboard-15.jpg)

Voici un exemple d'agencement en 3 colonnes:

![](../administrator_manual/12-Dashboard-16.jpg)

