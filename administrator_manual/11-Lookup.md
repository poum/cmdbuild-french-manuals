# Liste de valeurs

Comme décrit précédemment, un des types de données disponible dans CMDBuild est le type "liste de valeurs" ("lookup"), i.e. un attribut qui affiche un champ de sélection d'une valeur parmi une liste prédéfinie.

De ce fait, l'utilisateur doit choisir une option à partir d'une liste de valeurs possibles (par exemple, un type d'écran peut être "CRT", "LCD" ou "Plasma" - la marque d'un écran peut être "HP", "Dell", "Philips", etc.).

Vous pouvez également créer des attributs de type liste de valeur multi-niveaux (par exemple: macro catégorie => catégorie).

## Onglet Propriétés

En utilisant l'onglet "Propriétés", vous pouvez gérer les listes de valeurs.
Il est possible de réaliser les opérations suivantes:

- ajouter une nouvelle liste de valeurs ![](../administrator_manual/11-Lookup-1.jpg)
- modifier une liste de valeurs existante (description, seulement) ![](../administrator_manual/11-Lookup-2.jpg)

Pour créer ou modifier une liste de valeurs, les informations suivantes sont requises:

- "Description" - le nom de la liste (par exemple: "Type d'écran" ou "Marque")
- "Parent" - le parent de la liste actuelle (ne peut être défini que lors de la création)

![](../administrator_manual/11-Lookup-3.jpg)

## Liste des listes de valeurs

Avec l'onglet "Liste des listes de valeurs", vous pouvez créer de nouvelles entrées dans la liste ou modifier les existantes.
Il est possible de réaliser les opérations suivantes:

- créer une nouvelle entrée ![](../administrator_manual/11-Lookup-4.jpg)
- modifier une nouvelle entrée ![](../administrator_manual/11-Lookup-5.jpg)
- supprimer une entrée ![](../administrator_manual/11-Lookup-6.jpg)

![](../administrator_manual/11-Lookup-7.jpg)

Pour définir une nouvelle entrée, vous devez indiquer les informations suivantes:

- "Code" de la nouvelle entrée
- "Description" de la nouvelle entrée
- "Description de la liste mère" - choisir une entrée dans la liste mère, s'il y en a
- "Notes" - toute information complémentaire concernant l'entrée
- "Active" - indique si l'élément est actif ou a été supprimé (suppression logique)

L'ordre des entrées peut être modifié en déplaçant les lignes dans la grille (drag'n'drop).
