# Gérer les domaines

## Onglet Propriétés

Le premier onglet, "Propriétés", vous permet de créer de nouveaux domaines et d'en modifier les détails. Un domaine représente des relations à enregistrer dans la CMDB (les fournisseurs fournissent des éléments, les utilisateurs utilisent un ordinateur, des ordinateurs ont des logiciels, etc.). Le domaine est représenté par une liste de relations et des attribués associés en utilisant une table créée automatiquement par CMDBuild dans la base de données pour enregistrer les rapports ajoutés.

![](../administrator_manual/07-Properties_tab-1.jpg)

Cette fonctionnalité vous permet de définir des relations ("domaines") entre des classes et implique les étapes suivantes:

- visualisation des domaines configurés
- création d'un nouveau domaine ![](../administrator_manual/05-Domains_tab-1.jpg)
- modification d'un domaine existant ![](../administrator_manual/05-Domains_tab-2.jpg)
- suppression d'un domaine existant ![](../administrator_manual/05-Domains_tab-3.jpg)

Pour chaque domaine, vous devez définir:

- "Nom" - le nom de la table en base de données
- "Description" - le bouton  maître-détails (si applicable) dans le module de gestion 
- "Origine" - tel que "Fournisseur"
- "Destination" - tel que "Element"
- "Description directe" - par exemple, l'élément "a été fourni par" un fournisseur
- "Description inverse" - par exemple, le fournisseur "a fourni" l'élément
- "Cardinalité" - une contrainte du nombre de fois q'une entité peut apparaître dans une relation (1:1, 1:N, N:1, N:N) qui est utilisée par l'application pour valider de nouvelles relations
- "Maître Détail" - active la fonctionnalité maître-détail, i.e. les éléments de la classe détail (côté "N") présenté comme lignes de la classe principale (côté "1")
- "Active", indique si le domaine est actif ou a été supprimée (suppression logique)

## Onglet Attributs

L'onglet "Attributs" vous permet de gérer les attributs des domaines.
Cet onglet est utilisé pour ajouter des informations à un nouveau domaine ou mettre à jour un domaine existant.

![](../administrator_manual/07-Attributes_tab-1.jpg)

Il est possible de réaliser les informations suivantes:

- créer un nouvel attribut ![](../administrator_manual/05-Attributes_tab-2.jpg)
- modifier un attribut existant (modifier les champs disponibles) ![](../administrator_manual/05-Attributes_tab-4.jpg)
- supprimer un attribut existant (si la classe ne contient aucune donnée) ![](../administrator_manual/05-Attributes_tab-5.jpg)

L'ordre des attributs peut être modifié en déplaçant les lignes dans la grille (drag'n'drop).
Pour chaque attribut, vous devez indiquer:

- "Nom" - le nom de colonne en base de données
- "Description" - le nom de l'attribut dans l'application
- Types de données disponibles
  - "Booléen"
  - "Caractère"
  - "Date"
  - "Décimal"
  - "Double" (nombre à virgule flottante en double précision)
  - "Inet" (adresse IP)
  - "Entier"
  - "Liste de valeurs" ("Lookup", liste prédéfinie d'une liste de valeurs - peut être définie dans "Réglages" / "Liste de valeurs")
  - "Chaîne de caractères"
  - "Texte"
  - "Temps"
  - "Estampille"
- "Afficher dans la grille", pour afficher l'attribut dans les détais de la référence, dans le module de gestion
- "Unique" - pour indiquer que ce champ représente une clef unique
- "Obligatoire", pour rendre un champ obligatoire
- "Active", pour réactiver des attributs supprimés logiquement
- "Mode d'édition", qui peut être
  - Modifiable: attribut normalement modifiable
  - Lecture seule: visible mais non modifiable
  - Caché: attribut non visible

Chaque attribut peut avoir plus de champs, selon les mêmes règles que celles décrites pour les attributs des classes.



