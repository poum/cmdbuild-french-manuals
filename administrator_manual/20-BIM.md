# BIM

## Projets

Cette fonction permet de gérer des projets BIM suivis dans CMDBuild. Chaque projet est habituellement associé à un bâtiment ou une zone bâtie.

Un projet BIM correspond avec un fichier IFC présent en plusieurs versions.

Pour les fichiers IFC, CMDBuild utilise comme dépôt intégré l'outil open source BIMServer qui contient les fichiers importés depuis CMDBuild.

Il est possible de réaliser les opérations suivantes:

- créer un nouveau projet ![](../administrator_manual/20-Projects-1.jpg)
- modifier un projet ![](../administrator_manual/20-Projects-2.jpg)
- désactiver un projet ![](../administrator_manual/20-Projects-3.jpg)
- exporter un projet intégré contenant les informations gérées / modifiées dans CMDBuild

![](../administrator_manual/20-Projects-4.jpg)

De façon à créer un nouveau projet, vous devez saisir les informations suivantes:

- "Nom", nom du projet
- "Description", description du projet
- "Fichier IFC", ce qui nécessite l'import du fichier IFC de façon à l'envoyer à BIMServer
- "Actif", si le projet est actif
- "Connection à la fiche", ce qui nécessite de choisir la fiche de la classe de CMDBuild pour la connexion du projet; c'est habituellement un bâtiment ou une zone bâtie (qyi doit coïncider avec la classe CMDBuild indiquée comme "Racine" dans la fonction suivante de gestion des calques)

## Calque

L'IHM affiche la liste des classes de CMDBuild et "coche" les colones comme suit:

- "Active", où vous pouvez choisir les classes de CMDBuild ayant une correspondance avec les entités du fichier IFC
- "Racine", où vous pouvez choisir une classe de CMDBuild correspondant à la classe "racine" du fichier IFC, par exemple la classe "bâtiment" (sélection unique)
- "Export" où vous pouvez choisir les classes exportables de CMDBuild, de telles sorte que toutes les modifications exécutées dans CMDBuild seront reportées dans l'outil externe de conception architecturale (elles diffèrent des autres classes par l'ajout de la gestion des coordinées géométriques)
- "Conteneur" où vous pouvez choisir la classe CMDBuild utilisée comme "conteneur" pour les objets inclus dans le fichier IFC, par exemple la classe "pièce" (sélection unique)

![](../administrator_manual/20-Layer-1.jpg)
