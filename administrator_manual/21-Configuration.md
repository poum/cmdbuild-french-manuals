# Configuration

## Page de configuration

Cette page contient les réglages principaux de l'application.

![](../administrator_manual/21-Page_setup-1.jpg)

Dans la section "Général", vous pouvez définir:

- "Nom de l'instance" - un nom (chaîne de caractères) pour distinguer une instance (tel que "Test" pour distinguer des installations de production et de test)
- "Classe par défaut" - la classe chargée au démarrage
- "Nombre maximum de lignes" - indique le nombre maximum de relations affichées séparemment; s'il est dépassé, les relations sont regroupées
- "Limite des références et des sélections des listes de valeurs" - indique le nombre maximum d'options affichées dans les boîtes de sélection; s'il est dépassé, les options sont affichées dans une fenêtre surgissante avec une pagination et des filtres
- "Limite des relations"
- "Hauteur du paneau des fiches en pourcents" - indique la gestion de l'espace entre les zones de la grille et de la feuille de données  (50 par défaut, à définir par exemple à 90 pour donner la priorité à la zone de la feuille de données)
- "Position des onglets du sous-panneau dans le panneau des fiches" vous permet d'indiquer si les titres des sous-panneaux doivent apparaître en haut ou en bas
- "Délai d'expiration de la session (secondes)"

Dans la section "Fenêtres surgissantes", vous pouvez définir:

- "Hauteur en pourcentage de la fenêtre surgissante" - la hauteur en pourcentage de la fenêtre surgissante
- "Largeur en pourcentage de la fenêtre surgissante" - la largeur en pourcentage de la fenêtre surgissante

Dans la section "Langue", vous pouvez définir:

- "Langue par défaut" - la langue par défaut pour tous les utilisateurs
- "Afficher le choix de la langue" - si cette option est activée, le système demande la langue à utiliser dans la fenêtre de connexion

Pour la section "Langues autorisées", chaque langue utilisée pour la localisation de l'interface standard de CMDBuild est affichée. Vous pouvez choisir les langues utilisées dans l'instance actuelle de CMDBuild. Puis vous pourrez traduire tous les éléments personnalisés de l'instance dans toutes les langues (noms, classes, attributs, domaines, menus, rapports, etc.)

Dans la section "Verrou en modification des fiches", vous pouvez définir:

- "Activé", pour activer (par défaut) ou désactiver la gestion du verrouillage des fiches de données en cours de modification
- "Afficher le nom de l'utilisateur qui a verrouillé la fiche", pour exclure une telle information (éventuel besoin de confidentialité) du message affiché par le système quand on demande à modifier une fiche réservée à cet effet par un autre opérateur
- "Limite de temps de verrouillage" pour indiquer le temps réservé à un opérateur avant de libérer la fiche en cours de modification

## Page Alfresco

Cette page contient les préférences principales pour l'intégration d'Alfresco:

- "Général"
- "Serveur de fichiers"
- "Dépôt"
- "Identifiants"

![](../administrator_manual/21-Alfresco_page-1.jpg)

Dans la section "Général", vous pouvez définir:

- "Actif" - active ou désactive l'interface
- "URL du serveur" - l'URL d'Alfresco
- "Délai des opérations" - délai pour s'assurer que l'import est effectif avant de lancer un rafraîchissement de la grille

Dans la section "Serveur de fichiers", vous pouvez définir:

- "Type" - défini à "AlfrescoFTP", non modifiable
- "Hôte" - hôte du serveur FTP Alfresco
- "Port" - Port du serveur FTP Alfresco

Dans la section "Dépôt", vous pouvez définir:

- "Chemin d'accès du serveur de fichiers"
- "Chemin d'accès des web services"
- "Applications"

Dans la section "Identifiants", vous pouvez définir:

- "Nom d'utilisateur" - nom d'utilisateur Alfresco
- "Mot de passe" - mot de passe Alfresco
- "Catégorie CMDBuild" - la liste de valeurs utilisées pour catégoriser les documents enregistrés dans Alfresco

## Page de gestion du serveur

Comporte les fonctions utilitaires suivantes:

- "Vider le cache" - vide le cache de CMDBuild; utile quand les éléments de la base de données PostgreSQL sont modifiés en dehors de l'application (vues de la base de données, fonctions système, etc.) et que vous ne voulez pas redémarrer Tomcat pour les rafraîchir
- "Supprimer les processus incohérents" - cette fonction réinitialise les tables de Shark; utile quand les processus ont été supprimés manuellement dans la base de données
- "Déverouiller toutes les fiches", pour déverrouiller les fiches verrouillées. CMDBuild permet à l'opérateur d'accéder à une fiche en modification (ceci est utile pour anticiper le déblocage du verrou dans le cas où un opérateur ayant ouvert une fiche en modification interrompt l'opération)

![](../administrator_manual/21-Server_management_page-1.jpg)

## Page du graphe des relations

La page contient les réglages du graphe des relations.

![](../administrator_manual/21-Relation_graph-1.jpg)

Vous pouvez définir:

- "Actif" - activer ou désactiver la visualisation du graphe
- "Niveau par défaut" - le nombre par défaut de niveaux automatiquement déployés
- "Niveau maximum" - le nombre maximum de niveaux que vous pouvez déployer
- "Seuil pour les noeuds en cluster" - le nombre maximum de fiches déployées en relation avec une fiche donnée (note: au-dessus de cette limite et en deça du paramètre "niveau maximum", les relations peuvent être étendues en utilisant une case à cocher

## Page des processus

Cette page contient les options de configuration des processus.

![](../administrator_manual/21-Workflow_page-1.jpg)

Dans la section "Général", vous pouevez définir:

- "Actif" - active ou désactive les processus
- "URL du serveur" - url du service de processus

Le bouton "Supprimer les processus incohérents" est utilisé pour supprimer du cache du moteur de processus Shark les processus qui ne sont plus dans CMDBuild.

Dans la section "Identifiants", vous pouvez définir:

- "Nom d'utilisateur"
- "Mot de passe"
- "Nom du moteur" - défini à "shark", non modifiable
- "Portée"

## Page GIS

Cette page contient les réglages GIS

![](../administrator_manual/21-GIS_page-1.jpg)

Vous pouvez définir:

- "Actif" - la base de données doit être configurée pour prendre en charge l'extension PostGIS
- "Latitude initiale" - latitude de la carte au démarrage
- "Longitude initiale" - longitude de la carte au démarrage
- "Niveau de zoom initial" - niveau de zoom au démarrage

## Page BIM

Elle comporte les réglages utilisés dans le système pour la gestion des modèles 3D BIM (standard IFC) et en particulier le compte de connexion au dépôt du serveur BIM utilisé par CMDBuild.

![](../administrator_manual/21-BIM_page-1.jpg)

Vous pouvez définir:

- "Actif" (nécessite une instance de BIMServer configurée)
- "URL" de l'instance de BIMServer
- "Nom d'utilisateur" de l'instance BIMServer
- "Mot de passe" de l'instance BIMServer
