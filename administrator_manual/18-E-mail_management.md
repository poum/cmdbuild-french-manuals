# Gestion des emails

Dans CMDBuild, les emails sont contrôlés depuis le noyau pour un meilleur contrôle et une mise à jour plus simple des paramètres.

Cette fonction vous permet d'enregistrer les informations concernant chacun des comptes et de pré-configurer certains modèles d'emails utilisés dans les mécanismes de CMDBuild:

- systèmes de processus
- widget
- gestionnaire de tâches

## Compte

Vous pouvez pré-configurer les comptes utilisés pour envoyer des emails.
Il est possible de réaliser les opérations suivantes:

- créer un nouveau compte ![](../administrator_manual/18-E-mail_management-1.jpg)
- modifier un compte ![](../administrator_manual/18-E-mail_management-2.jpg)
- supprimer un compte ![](../administrator_manual/18-E-mail_management-3.jpg)
- définir un compte comme compte par défaut ![](../administrator_manual/18-E-mail_management-4.jpg)

![](../administrator_manual/18-E-mail_management-5.jpg)

De façon à créer un nouveau compte, les informations suivantes sont nécessaires:

- "Compte"
    - "Nom", nom du compte
- "Identifiants"
    - "Nom d'utilisateur"
    - "Mot de passe"
- "Envoyer des emails"
    - "Adresse"
    - "Serveur SMTP"
    - "Port SMTP"
    - "Activation du SSL"
- "Boîte de réception"
    - "Adresse"
    - "Serveur IMAP"
    - "Port IMAP"
    - "Activation du SSL"
    - "Boîte de réception", dossier qui contient les emails à traiter (ceux qui correspondent à un modèle prédéfini)
    - "Emails traités", dossier pour les emails traités
    - "Emails rejetés", dossier pour les emails qui ne correspondent pas au modèle défini si l'indicateur suivant est activé
    - "Suppression des emails à ne pas traiter", avec cet indicateur, vous pouvez demander la suppression des emails qui ne correspondent pas au modèle indiqué

## Modèle de mail

Vous pouvez créer des modèles d'emails préconfigurés qui peuvent être réutilisés dans les fonctions de CMDBuild qui les utilisent.

Il est possible de réaliser les opérations suivantes:

- créer un nouveau modèle ![](../administrator_manual/18-Mail_template-1.jpg)
- modifier un modèle ![](../administrator_manual/18-Mail_template-2.jpg)
- supprimer un modèle ![](../administrator_manual/18-Mail_template-3.jpg)

![](../administrator_manual/18-E-mail_management-4.jpg)

De façon à créer un nouveau modèle, il faut indiquer les informations suivantes:

- "Propriétés de base"
    - "Nom", nom du modèle
    - "Description"
- "Modèle"
    - "A", adresse (ou liste de plusieurs adresses séparées par des virgules)
    - "Copie", adresse (ou liste de plusieurs adresses séparées par des virgules) des destinataires en copie
    - "Copie cachée", adresse (ou liste de plusieurs adresses séparées par des virgules) des destinataires en copie cachée
    - "Objet", titre de l'email (il peut inclure des champs paramétrés exprimés en CQL)
    - "Texte", corps de l'email (il peut inclure des champs paramétrés expriéms en CQL)

A l'aide d'expressions CQL, vous pouvez personnaliser votre email dans chaque champ du modèle avec des informations disponibles dans le contexte courant (i.e. variables de processus utilisés pour envoyer des emails)

