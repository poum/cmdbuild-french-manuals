# Arbres de navigation

## Onglet Propriétés

Les arbres de navigation dans CMDBuild sont utilisés pour mettre à disposition une vue hiérarchique de l'information dans certaines fonctions de gestion, parmi lesquelles:

- le choix d'un élément via l'interface arborescente dans le widget "Arbre de navigation"
- navigation GIS
- navigation BIM

Les arbres de navigation seront également utilisés dans les prochaines versions de CMDBuild de façon à améliorer les fonctions du graphe des relations.

L'onglet Propriétés permet de créer de nouveaux arbres de navigation et de modifier le nom d'un arbre pré-existant.

Il est possible de réaliser les opérations suivantes:

- initier la création d'un nouvel arbre de navigation ![](../administrator_manual/10-Navigation_trees-1.jpg) 
- modifier un arbre de navgation pré-existant ![](../administrator_manual/10-Navigation_trees-2.jpg) 
- supprimer un arbre de navigation pré-existant ![](../administrator_manual/10-Navigation_trees-3.jpg) 

 ![](../administrator_manual/10-Navigation_trees-4.jpg) 

De façon à créer un nouvel arbre de navigation, les informations suivantes sont requises:

- "Nom", nom de l'arbre de navigation
- "Description", description de l'arbre de navigation
- "Origine", classe racine de l'arbre
- "Actif", si l'arbre est activé

## Onglet arbre

Via l'onglet arbre, vous pouvez construire graphiquement l'arbre de navigation courant.
Pour chaque noeud, vous pouvez définir un éventuel filtre CQL de façon à limiter le nombre de fils sélectionnables

![](../administrator_manual/10-Navigation_trees-5.jpg) 
