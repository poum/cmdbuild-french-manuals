# Menu

## Menus personnalisés

Le système vous permet de définir des menus personnalisés pour tous les groupes ou pour des groupes particuliers.
Chaque menu consiste en éléments système (classes, processus, vues, rapports) et en répertoires personnalisés (utilisés pour organiser les entrées).
Ces menus sont disponibles dans le module de gestion en tant que menu principal de navigation (celui qui est configuré pour le groupe de connexion, celui par défaut sinono).
Les utilisateurs qui n'ont pas de menu de navigation peut toujours accéder au module de gestion mais le système n'affichera que les classes pour lesquelles ils ont des droits en lecture ou en écriture).

![](../administrator_manual/14-Menu-1.jpg)

Il est possible de réaliser les opérations suivantes:

- modifier le menu pour le groupe utilisateur choisi
- supprimer le menu pour le groupe utilisateur choisi ![](../administrator_manual/14-Menu-2.jpg)

La configuration d'un menu comprend les opérations suivantes:

- ajouter un nouveau dossier ![](../administrator_manual/14-Menu-3.jpg)
- glisser-déplacer les éléments de menu à partir d'une liste d'éléments disponibles
- supprimer des éléments du menu ![](../administrator_manual/14-Menu-4.jpg)

Avec une seule opération, vous pouvez insérer une super-classe contenant des sous-classes.
Une fois les modifications confirmées, le menu sera disponible dans le module gestion (se déconnecter et se reconnecter pour rafraîchir l'affichage).
