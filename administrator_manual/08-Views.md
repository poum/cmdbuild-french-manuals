# Vues

Grâce aux mécanismes des vues, des groupes de fiches d'une classe qui ne correspondent pas peuvent être fournis aux utilisateurs.

Les vues peuvent être définies comme sous-ensembles de fiches d'une unique classe (basés sur des filtres) ou comme des groupes de fiches de données constitués par des informations renseignées dans des classes différentes (basés sur des requêtes SQL).

Dans le premier cas, elles conservent leurs fonctionnalités communes de gestion des classes,dans le second, elles sont en lecture seule.

Les mécanismes des vues rappellent la fonctionnalité de restriction des droits d'accès d'une classe en mettant hors d'atteinte certaines lignes ou colonnes.

Alors que les restrictions des droits d'accès empêchent définitivement la consultation de certaines informations, pour un groupe d'utilisateurs particuliers on a la possibilité de définir une vue qui affiche certains groupes de données et une autre vue qui affiche d'autres données (additionnelles ou séparées).

Par exemple, quand vous utilisez un attribut "Etat" ou un attribut "Dépositaire", vous pouvez créer pour le même grouoe d'opérateurs une vue "Eléments utilisés" et une vue "Eléments disponibles" issues de la classe "Elément".

## Vues basées sur les filtres

Les administrateurs système peuvent créer une vue basée sur des fitres:

- en réutilisant et en clonant un filtre défini par un utilisateur
- en définissant un nouveau filtre de recherche, i.e. en définissant des critères de recherche sur des attributs de la classe source indiquée

Il est possible de réaliser les opérations suivantes:

- initier la création d'une nouvelle vue ![](../administrator_manual/08-Filter_based_view-1.jpg)
- modifier une vue pré-existante ![](../administrator_manual/08-Filter_based_view-2.jpg)
- supprimer une vue pré-existante ![](../administrator_manual/08-Filter_based_view-3.jpg)
- ouvrir une fenêtre surgissante dans laquelle vous pouvez choisir un filtre utilisateur ou créer un nouveau filtre ![](../administrator_manual/08-Filter_based_view-4.jpg)
- supprimer les critères de recherche courants ![](../administrator_manual/08-Filter_based_view-5.jpg)

Pour créer ou modifier un filtre de recherche, les informations suivantes sont requises:

- "Nom" - nom de la vue (ex: "ElementEnCoursDUtilisation")
- "Description" - description du filtre (ex: "Elément actuellement utilisé")
- "Classe source" - classe sur laquelle le filtre peut être appliqué (ex: Elément)

Une nouvelle vue ne peut pas être utilisée si les droits d'accès ne sont pas définis en indiquant quel groupe(s) d'utilisateurs peut/peuvent les utiliser.

Une telle opération peut être menée à bien en utilisant la fonction "Utilisateur et groupes" / "Droits" / "Vues".

Ci-dessous, vous trouverez deux captures d'écran qui décrivent l'interface utilisateur fournie pour la gestion des vues basées sur un filtre.

![](../administrator_manual/08-Filter_based_view-6.jpg)

![](../administrator_manual/08-Filter_based_view-7.jpg)

## Vues basées sur SQL

Des fiches de données peuvent être fournies aux opérateurs en collectant les attributs présents sur différentes classes.

Ces typologies de vues peuvent être en lecture seule, limitées à la fiche principale (mais avec les relations et l'historique).

D'un point de vue technique, la requête SQL doit être encapsulée dans une fonction PostgreSQL, définie avec certains critères qui permettront à CMDBuild de l'identifier et de l'utiliser. Pour une description de ces critères, voir le paragraphe concernant la définition des graphiques des tableaux de bord (définition de la source de données - fonction PostgreSQL).

Il est possible de réaliser les opérations suivantes:

- initier la création d'une nouvelle vue ![](../administrator_manual/08-Filter_based_view-1.jpg)
- modifier une vue pré-existante ![](../administrator_manual/08-Filter_based_view-2.jpg)
- supprimer une vue pré-existante ![](../administrator_manual/08-Filter_based_view-3.jpg)

Pour créer ou modifier une vue SQL, les informations suivantes sont nécessaires:

- "Nom" - nom de la vue (ex: "ElementEnUtilisation")
- "Description" - description du filtre (ex: "Elément actuellement utilisé")
- "Source de données" - fonction PostgreSQL dans laquelle la requête SQL est définie 

![](../administrator_manual/08-Filter_based_on_sql.jpg)

Notez que la fonction PostgreSQL doit satisfaire les limitations décrites dans le chapitre ayant trait aux tableaux de bord, en particulier dans les pages qui contiennent des exemples de fonctions SQL.

Dans ce cas également, une nouvelle vue ne peut pas être utilisée si les droits d'accès ne sont pas définis, en indiquant quel(s) groupe(s) d'utilisateurs peut/peuvent l'utiliser.

Une telle opération doit être réalisée en utilisant la fonction "Utilisateurs et groupes" / "Droits" / "Vues".

