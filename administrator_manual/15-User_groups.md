# Groupes utilisateurs

Les permissions de CMDBuild sont basées sur:

- les groupes utilisateurs
- les permissions des groupes sur les classes, les vues et les filtres de recherche (les permissions sur les tableaux de bord et les rapports sont gérés à la place dans les sections liées)
- les associations utilisateurs - groupes

C'est pourquoi il est possible de:

- configurer les groupes utilisateurs avec des permissions spécifiques pour chaque classe définie dans le système, ainsi que les lignes et les colonnes
- ajouter des utilisateurs à un ou plusieurs groupes; l'utilisateur hérite des permissions du groupe

D'autres fonctions, rassemblées dans l'onglet "Configuration de l'IHM" permettent de définir des réglages personnalisés de l'interface utilisateur en retirant certains fonctionnalités standard de CMDBuild et en simplifiant l'interaction avec l'interface pour certains groupes d'utilisateurs possèdant moins d'aptitudes techniques.

Ce chapitre décrit la gestion des groupes, le positionnement dans des groupes utilisateurs pré-existants. Le chapitre suivant, quant à lui, traite de la gestion des utilisateurs.

## Onglet Propriétés

L'onglet Propriétés vous permet de créer et de modifier des groupes.

Il est possible de réaliser les opérations suivantes:

- ajouter un nouveau groupe ![](../administrator_manual/15-Properties_tab-1.jpg)
- modifier un groupe existant ![](../administrator_manual/15-Properties_tab-2.jpg)
- désactiver un groupe ![](../administrator_manual/15-Properties_tab-3.jpg)

Pour créer un nouveau groupe, vous devrez saisir les informations suivantes:

- "Nom du groupe"
- "Description"
- "Email"
- "Est administrateur" - indique si le groupe est un groupe administrateur, i.e. qu possède un accès complet au système
- "Page de démarrage" - la page de démarrage par défaut quand on entre dans le module de gestion

Voici une capture d'écran d'exemple.

![](../administrator_manual/15-Properties_tab-4.jpg)

## Onglet Utilisateurs

L'onglet Utilisateurs vous permet d'associer un utilisateur à un ou plusieurs groupes.
Vous pouver tirer-glisser des utilisateurs de la liste des utilisateurs disponibles (à gauche) vers la liste des utilisateurs dans le groupe (à droite).

![](../administrator_manual/15-User_tab-1.jpg)

## Onglet Droits d'accès

L'onglet Droits d'accès vous permet de définir les permissions sur chaque classe / vue / filtre.

Il est possible de réaliser les opérations suivantes:

- pour chaque type d'élément (classe / vue / filtre de recherche), définir / modifier le type de droit d'accès selon l'une des valeurs possibles: aucun / lecture / modification. ![](../administrator_manual/15-Permissions_tab-1.jpg)
- seulement pour les classes, ouvrir une fenêtre surgissante dans laquelle vous pouvez définir un filtre sur des lignes de classe (si nécessaire en choisissant un filtre utilisateur) ou sur des colonnes. ![](../administrator_manual/08-Filter_based_view-4.jpg)
- seulement pour les classes, supprimer le filtre de recherche courant ![](../administrator_manual/08-Filter_based_view-5.jpg)

Voici la page ayant trait à la gestion des droit d'accès sur les classes (onglet classe):

![](../administrator_manual/15-Permissions_tab-2.jpg)

Vous pouvez modifier les droits d'accès en cliquant sur les cases à cocher aucun / lecture / modification.

Si vous voulez réduire les droits d'accès pour le groupe courant aux lignes et colonnes d'une classe, vuos devez utiliser le premier icone placé à la fin de chaque ligne dans la grille.
De cette façon, vous accèderez à une fenêtre surgissante qui présente deux onglets appelés "Droits d'accès sur les lignes" et "Droits d'accès sur les colonnes".

### Classes : droits d'accès sur les lignes

La gestion des droits d'accès sur les lignes peut être réalisée de deux manières:

- en réutilisant et en clonant un filtre pré-existant défini par un utilisateur
- en définissant un nouveau filtre de recherche.

Un nouveau filtre de recherche, à son tour, peut être défini de trois façons:

- en définissant des critères de recherche sur les attributs de la classe source indiquée (onglet attributs)
- en définissant des critères de recherche sur les relations de la classe source indiquée (onglet attributs)
- en utilisant une fonction PostgreSQL pré-définie qui doit être créée conformément aux critères décrits dans le chapitre concernant les tableaux de bord (onglet Fonction).

#### Onglet attributs

![](../administrator_manual/15-Attributes_tab-1.jpg)

#### Onglet "Relations"

![](../administrator_manual/15-Relations_tab-1.jpg)

#### Onglet Functions

![](../administrator_manual/15-Functions_tab-1.jpg)

### Classes: droits d'accès sur les colonnes

La fonction vous permet de choisir les attributs de la classe qui seront visibles pour les utilisateurs appartenant au groupe en cours de configuration.

![](../administrator_manual/15-Columns_priv.jpg)

### Classes: désactiver les boutons de l'IHM

Dans l'interface graphique de CMDBuild, vous pouvez désactiver les droits d'accès liés à l'ajout / la modification / le clonage / la suppression d'une fiche.

Cette option n'est valable que pour l'IHM de CMDBuild. Elle n'a pas de conséquence ailleurs (par exemple en requerrant les droits d'accès à une classe via les services web).

Voici une capture d'écran de la fenêtre surgissante fournie par le système.

![](../administrator_manual/15-GUI_buttons.jpg)

### Vues

Voici la page ayant trait à la définition des droits d'accès aux vues.

![](../administrator_manual/15-Permissions_view.jpg)

### Filtres de recherche

Voici la page ayant trait à la définition des droits d'accès aux filtres de recherche.

![](../administrator_manual/15-Permissions_filter.jpg)

## Onglet Configuration de l'IHM

L'onglet "Configuration de l'IHM" permet de personnaliser l'interface utilisateur d'un groupe d'utilisateur particulier, en excluant certains exemples ou en simplifiant leur travail global.

En particulier, vous pouvez travailler sur:

- les éléments affichés dans le menu accordéon de gauche, avec des détails additionnels pour les fonctions utilitaires
- les onglets disponibles pour la gestion des fiches
- les onglets disponibles pour la gestion des processus
- d'autres options

### Menu accordéon

Grâce aux boutons disponibles dans la première boîte, vous pouvez désactiver certains composants du menu principal de l'application (menu accordéon de gauche) pour le groupe actuel, en particylier:

- accordéon des fiches
- accordéon des processus
- accordéon des rapports
- accordéon des vues
- accordéon des tableaux de bord
- utilitaire : changer le mot de passe
- utilitaire : mises à jour de masse
- utilitaire : import de fichier CSV
- utilitaire : export de fichier CSV

### Onglet Fiches

Avec les boutons de la deuxième boîte, vous pouvez désactiver certains onglets disponibles pour la gestion des fiches pour le groupe courant:

- onglet "Détails"
- onglet "Notes"
- onglet "Relations"
- onglet "Historique"
- onglet "Pièces jointes"

### Onglet Processus

Avec les boutons de la troisième boîte, vous pouvez désactiver certains onglets disponibles pour la gestion des processus pour le groupe courant:

- onglet "Notes"
- onglet "Relations"
- onglet "Historique"
- onglet "Pièces jointes"

### Autres fonctions

Avec les boutons de la dernière boîte, vous pouvez:

- masquer le menu accordéon dans son intégralité du côté gauche de l'écran (vous pouvez le restaurer avec le contrôle graphique approprié)
- afficher en plein écran la gestion des fiches (la saisie d'une nouvelle fiche et la modification d'une fiche choisie dans la liste masquera temporairement la liste)
- demander l'affichage simplifié de l'historique des fiches (réduisant les informations récapitulatives affichées et bloquant la saisie d'une fiche complètement historisée)
- toujours garder actifs les widgets fournis pour l'état actuel

![](../administrator_manual/15-UI_conf.jpg)

Dans la section précédente liée aux droits d'accès se trouvent davantage d'informations sur la possibilité de désactiver des droits d'accès liés à l'ajout / la modification / le clonage / la suppression d'une fiche de l'IHM standard de CMDBuild.
