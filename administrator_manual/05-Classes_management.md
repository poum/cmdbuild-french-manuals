# Gestion des classes

## Onglet propriétés

Le premiet onglet, "Propriétés", vous permet de créer de nouvelles classes / superclasses et modifier certaines options. Une classe représente des objets à enregistrer dans la CMDB (ordinateurs, écrans, etc.). Le système crée automatiquement toutes les formulaires pour gérer les données des fiches. Les champs de saisie correspondent aux attributs définis dans la classe, et les données des fiches sont enregistrées dans la table associée de la base de donnée (automatiquement créée).

![Onglet propriétés](../administrator_manual/05-Properties_tab-1.jpg)

Il est possible de réaliser les opérations suivantes:

- ajouter une nouvelle classe ![](../administrator_manual/05-Properties_tab-2.jpg)
- imprimer le schéma du modèle de données ![](../administrator_manual/05-Properties_tab-3.jpg)
- modifier une classe existante en la choisissant depuis le menu ![](../administrator_manual/05-Properties_tab-4.jpg)
- supprimer une classe existante (suppression logique) ![](../administrator_manual/05-Properties_tab-5.jpg)
- afficher les détails de la classe courante ![](../administrator_manual/05-Properties_tab-6.jpg)

Pour créer une nouvelle classe, vous devez indiquer les informations suivantes:

- "Nom" - le nom de la table dans la base de données (i.e. "Contrat")
- "Description" - le nom de la classe utilisé dans l'application
- "Type" - peut être "Standard" (classe CMDBuild normale), "Simple" (classe sans héritage, historisation, relations, avec des attributs qui sont des "clefs étrangères" mais pas des attributs "référence" - voir la note plus bas)
- "Hérite de" - nom de la superclasse (par défaut, toutes les classes héritent de la superclasse "Classe", cependant vous pouvez choisir une superclasse spécifique, par exemple "Ecran" dérive de la superclasse "Elément")
- "Superclasse", cet indicateur indique si la classe contient des données (superclasse = non) ou définira certains attributs communs pour les sous-classes (superclasse = oui)
- "Active", indique si la classe est active ou a été supprimée (suppression logique)

Note: le type de classe "Simple" peut être utilisé pour enregistrer des flux de données venant d'autres systèmes, ce qui est utile par exemple pour enregistrer les données de la consommation électrique, les notifications d'alerte sur la disponibilité des services, les journaux des serveurs, etc.).

Localisation: la localisation de l'interface de base de CMDBuild (textes des menus, boutons standards, titres, pied de page, etc.) est gérée via un système de fichiers externe (format json). La localisation des éléments de l'application (noms de classe, attributs, listes de valeurs, etc.) est:

- gérée via des fenêtres surgissantes via un bouton spécifique défini dans chaque formulaire qui le requiert
- renseignée dans une nouvelle table système ("_Translation") dans la base de données CMDBuild

Dans le cas de la gestion des classes, à côté du champ "Description" se trouvent les icones suivantes:

- traduction dans d'autres langages ![](../administrator_manual/05-Properties_tab-7.jpg)

L'icone permet d'accéder à la fenêtre surgissante suivante dans laquelle vous pouvez indiquer la traduction du nom de la classe dans d'autres langues (voir le chapitre Configuration, page Options générales), qui peuvent être choisies parmi celles prises en charge par CMDBuild:

![](../administrator_manual/05-Properties_tab-8.jpg)

En choisissant une langue, le nom sera traduit dans la langue désirée.
Le même mécanisme se produit pour ces éléments avec la description d'une traduction possible:

- description de classe
- description de l'attribut
- description, description directe, description inverse de domaine
- description d'attribut de domaine
- description de vue
- description de filtre
- description de titre de liste de valeurs
- description de tableau de bord
- description de rapport
- description de titre de menu

Dans le manuel, vous pouvez vous référer à cette description à chaque fois que vous trouvez l'icone de traduction.

## Onglet Attributs

Le second onglet, "Attributs", vous permet de gérer les attributs des classes.
Cet onglet est utilisé pour ajouter des informations à une nouvelle classe ou mettre à jour une classe existante.
Dans l'exemple, les attributs de la classe "Ecran" comprennent:

- les attributs hérités de la superclasse "Element" (la classe "Ecran" hérite de "Element"); notez qu'il n'est pas possible de modifier les attributs hérités
- d'autres attributs spécifiques, par exemple "Type" et "Taille"

![](../administrator_manual/05-Attributes_tab-1.jpg)

Il est possible de réaliser les opérations suivantes:

- créer un nouvel attribut ![](../administrator_manual/05-Attributes_tab-2.jpg)
- choisir les attributs grâce auxquels vous souhaitez trier les fiches ![](../administrator_manual/05-Attributes_tab-3.jpg)
- modifier un attribut existant (modifier les champs disponibles) ![](../administrator_manual/05-Attributes_tab-4.jpg)
- supprimer un attribut existant (si la classe ne contient aucune donnée) ![](../administrator_manual/05-Attributes_tab-5.jpg)

L'ordre des attribut peut être modifié en déplaçant les lignes dans la grille (par drag'n'drop).

Pour chaque attribut, vous devez indiquer:

- "Code" : le nom de la colonne en base de données
- "Nom" : le nom de l'attribut dans l'application
- "Groupe" : utilisé pour regrouper des attributs et les organiser dans des onglets (par exemple, la classe "Ecran" possède plusieurs onglets d'attributs: "Général", "Données administratives" et "Données techniques")
- Type de donnée disponible:
  - "Booléan"
  - "Caractère"
  - "Date"
  - "Nombre décimal"
  - "Double" (nombre décimal en virgule flottante en double précision)
  - "Clef étrangère" (lien vers une autre classe, uniquement pour les classes de type "Simple")
  - "Inet" (adresse IP v4 ou v6, avec la possibilité d'indiquer une valeur de masque)
  - "Entier"
  - "Liste de valeurs" ("Lookup", liste de valeurs prédéfinies - peut être définie dans "Réglages" / "Listes de valeurs")
  - "Référence" (référence à une autre classe via un "domaine", seulement pour les classes de type "Normal")
  - "Chaîne de caractères"
  - "Texte"
  - "Temps"
  - "Estampille"
- "Afficher dans la grille" - pour déterminer si l'attribut doit apparaître dans la liste par défaut des attributs utilisée dans la grille de gestion
- "Uniqye" - pour indiquer que le champ représente une clef unique
- "Obligatoire", pour rendre le champ obligatoire
- "Actif", pour réactiver des attributs qui ont été supprimés de façon logique
- "Mode de modification", qui peut être
  - modifiable: attribut normalement modifiable
  - lecture seule: visible mais non modifiable
  - caché: attribut non visible

Certains attributs ont davantage de champs, en voici le détail.

### Attributs "Nombre décimal"

Pour les attributs de type "nombre décimal", ces informations complémentaires sont requises:

- "précision", indique le nombre total de chiffres
- "échelle", indique le nombre de chiffres de la partie décimale

### Attributs "Chaîne de caractères"  

Pour les attributs "chaîne de caractères", une information complémentaire est nécessaire:

- "Longueur", indique le nombre maximum de caractères autorisé

### Attributs "Texte"

Pour les attributs "Texte", une information complémentaire est nécessaire:

- "Type d'éditeur", peut être "Texte brut" ou "Html"

### Attributs "Liste de valeurs"

Les attributs "Lookup" vous permettent de choisir la valeur de l'attribut dans une liste prédéfinie de valeurs; cette liste est créée et modifiée en utilisant le menu "Listes de valeurs" du menu d'administration (voir le chapitre dédié).

Donc, quand vous choisissez un attribut "Liste de valeurs", il vous est demandé de choisir la liste de valeurs que vous voulez utiliser.

Note: des listes de valeurs multi niveaux sont également disponibles

### Attributs "Référence"

Les attributs "Référence" vous permettent de lier l'attribut à une fiche d'une autre classe via un domaine 1:N (où le côté "N" est celui de la classe courante)

Dans le module de gestion, il sera possible de choisir des valeurs à partir de la liste des références disponibles (qui contiennent l'attribut "Nom").

Un attribut référence, dans le module de gestion, crée ou modifie la relation entre deux fiches.

C'est pourquoi, quand vous définissez un attribut "Référence", il vous est demandé de choisir le "domaine" que vous voulez appliquer.

Une fonctionnalité avancée des attributs référence est le filtre CQL (le langage de requêtage de CMDBuild) qui permet de définir un filtre pour la liste. Le filtre peut utiliser des variables définies dans la fenêtre surgissante pour les méta données, visible en cliquant sur le bouton "Modifier les méta données" (voir l'appendice).

![](../administrator_manual/05-Attributes_tab-6.jpg)

## Onglet "Domaines"

Cette fonctionnalité vous permet de définir des relations ("domaine") entre les classes, et implique les étapes suivantes:

- aperçu des domaines déjà configurés
- création d'un nouveau domaine ![](../administrator_manual/05-Domains_tab-1.jpg)
- modification d'un domaine existant ![](../administrator_manual/05-Domains_tab-2.jpg)
- suppression d'un domaine existant ![](../administrator_manual/05-Domains_tab-3.jpg)

![](../administrator_manual/05-Domains_tab-4.jpg)

Tout comme avec les attributs, il est possible d'afficher dans la grille:

- seulement les domaines définis sur la classe courante
- les domaines définis sur la classe courante plus les domaine définis pour la superclasse

En choisissant les fonctions d'édition (insertion, modification, suppression), le système va modifier le menu (menu "accordéon" à gauche) de gestion des classes à gestion des domaines, si bien que vous pourrez modifier les informations du domaine et les attributs du domaine.

La description des onglets "Propriétés" et "Attributs" se trouve dans le chapitre relatif aux domaines.

## Onglet "Widget"

Cette fonctionnalité vous permet de configurer certaines fontionnalités par défaut qui seront accessibles via des "boutons" dans le module de gestion.

Actuellement, les widgets suivants sont disponibles:

- créer des rapports: il permet d'imprimer un rapport
- calendrier: il montre les échéances définies sur un calendrier
- arbre de navigation: il permet de choisir une ou plusieurs fiches de données via une interface basée sur un arbre de navigation pré-configurée (sous-élément du graphe des domaines)
- lancement de processus: il permet de démarrer le processus indiqué en utilisant une fenêtre surgissante (le processus avancera avec les fonctions communes de CMDBuild)
- ping: il exécute un ping sur la machine indiquée
- créer ou modifier une fiche: il permet d'ajouter / modifier une fiche de données dans une classe différente de l'actuelle.

D'autres widgets peuvent être utilisés pour les processus (voir le manuel des processus).

Il est possible de réaliser les opérations suivantes:

- afficher les widgets déja configurés
- configurer un nouveau widget ![](../administrator_manual/05-Widget_tab-1.jpg)
- modifier un widget existant![](../administrator_manual/05-Widget_tab-2.jpg)
- supprimer un widget existant![](../administrator_manual/05-Widget_tab-3.jpg)

![](../administrator_manual/05-Widget_tab-4.jpg)

Les paramètres suivants sont communs à tous les widgets (les autres dépendent du type de widget):

- bouton texte
- actif: quand un widget est supprimé, ceci est mis à faux
- toujours présent: cela signifie que le widget est disponible également dans le mode vue (et pas seulement en mode édition)

### Widget d'impression de rapport

Les paramètres spécifiques pour ce widget sont:

- rapport: choisir un rapport dans la liste des rapports disponibles
- forcer le format: forcer le type de sortie (PDF ou CSV)
- liste des paramètres: définir les données à saisir pour les paramètres requis pour le rapport

### Widget calendrier

Les paramètres spécifiques pour ce widget sont:

- classe de référence: classe à partir de laquelle vous obtiendrez les informations qui seront présentées dans le calendrier (c'est facultatif s'il existe un filtre CQL)
- date de début: nom de l'attribut qui contient la date de début de l'événement
- date de fin; nom de l'attribut qui contient la date de fin de l'événement (ce paramètre est facultatif, s'il est omis, l'événement sera considéré comme étant ponctuel, i.e. il commence et il finit au même moment)
- date par défaut: date par défaut du calendrier
- titre de l'événement: nom de l'attribut qui donnera le titre de l'événement
- filtre CQL: filtre de sélection des classes avec lesquelles traiter (facultatif mais s'il est défini, a la précédence sur "classe de référence")

### Widget de sélection à partir de l'arborescence

Les paramètres spécifiques pour ce widget sont:

- arbre de sélection, choisi parmi ceux qui sont configurés dans le graphique "domaines" de la fonction dédiée "arbres de navigation"

### Widget de lancement des processus

Les paramètres spécifiques pour ce widget sont:

- type de sélection (par nom ou via une requête CQL)
- processus (seulement si le type de sélection choisi est "par nom"): nom du processus à démarrer
- attributs du processus (seulement si le type de sélection choisi est "par nom"): réglage de la valeur par défaut pour chaque paramètre qui démarre le processus
- filtre CQL (seulement si le type de sélection choisi est "via une requête CQL"): vous pouvez définir une liste de processus de façon à choisir dynamiquement celui à démarrer en créant une classe d'application fictive comportenant le nom du processus et définissant un filtre de sélection CQL dans ce champ

### Widget qui exécute un "ping" sur une adresse IP

Les paramètres spécifiques pour ce widget sont:

- adresse IP: l'adresse IP à contrôler
- nombre de paquets de ping
- paramètres personnalisés concaténés pour la commande ping

### Widget de création / modification d'une fiche

Les paramètres spécifiques pour ce widget sont:

- classe de référence: cette classe appartient à la fiche sur laquelle vous devez travailler
- filtre CQL: filtre de sélection sur la fiche que vous avez à gérer, quelque chose comme la phrase "{client:nom_du_champ_du_formulaire}" (si le filtre ne retourne aucune fiche, le widget en créera une nouvelle)

## Onglet "Calques"

L'onglet "calques" affiche la liste des attributs géographiques disponibles (chacun sur un calque différent) et vous permet de choisir ceux à afficher en fond de la classe courante.

![](../administrator_manual/05-Layers_tab-1.jpg)

## Onglet "Attributs géographiques"

L'onglet "Attributs géographiques" vous permet de gérer les attributs géographique de la classe choisie.

Il est possible d'exécuter les opérations suivantes:

- créer un nouvel attribut géographique ![](../administrator_manual/05-Geographic_attributes_tab-1.jpg)
- modifier un attribut existant (modifier les champs disponibles) ![](../administrator_manual/05-Geographic_attributes_tab-2.jpg)
- supprimer un attribut géographique existant (si la classe ne contient pas de données) ![](../administrator_manual/05-Geographic_attributes_tab-3.jpg)

![](../administrator_manual/05-Geographic_attributes_tab-4.jpg)

L'ordre des attributs peut être modifié en déplaçant les lignes dans la grille (drag'n'drop).

Pour chaque attribut, vous devez indiquer:

- "Nom": le nom de la colonne en base de données
- "Description": le nom de l'attribut dans l'application
- "Zoom minimum": le niveau de zoom minimum requis pour afficher l'attribut (par exemple, si nous avons toute la France dans la vue, cela n'a aucun sens d'afficher les batiments des villes puisqu'ils seront tous les uns sur les autres)
- "Zoom maximum", le niveau de zoom maximum requis pour afficher l'attribut
- Types de données disponibles
  - "Point"
  - "Ligne"
  - "Polygone"

Certains attributs géographiques possèdent plusieurs champs, voici les détails.

### Attributs "Point"

Pour les attributs "Point", ces informations complémentaires sont requises:

- "Icone" - le nom de l'icone (qui doit être chargé en utilisant les pages GIS dans le module d'administration) pour l'afficher sur la carte
- "Rayon du point" - la taille de l'icone
- "Couleur de remplissage"
- "Opacité du remplissage"
- "Couleur du trait"
- "Opacité du trait"
- "Epaisseur du trait", de 1 à 10
- "Style du trait" (point, tiret, tiret-point, trait long, solide)

### Attribut "Ligne"

Pour les attributs "Ligne", ces informations complémentaires sont requises:

- "Couleur du trait"
- "Opacité du trait"
- "Epaisseur du trait", de 1 à 10
- "Style du trait" (point, tiret, tiret-point, trait long, solide)

### Attribut "Polygone"

Pour les attributs "Polygone", ces informations complémentaires sont requises:

- "Couleur de remplissage"
- "Opacité du remplissage"
- "Couleur du trait"
- "Opacité du trait"
- "Epaisseur du trait", de 1 à 10
- "Style du trait" (point, tiret, tiret-point, trait long, solide)

