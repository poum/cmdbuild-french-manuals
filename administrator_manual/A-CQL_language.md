# Annexe: le langage CQL

CMDBuild offre la possibilité de définir des conditions de filtre évaluées par le système, de façon
à donner à l'utilisateur moins de choix dans certains cas spécifiques fournis par l'application.

De telles condition de filtre doivent être exprimées avec le langage CQL (CMDBUild Query Language pour Langage de requête de CMDBuild) et
peuvent être utilisées pour:

- filtrer certaines fiches dans la liste de choix présentée de façon à définir un attribut de type référence
- filtrer certaines fiches dans le widget "lien des fiches"
- définir certaines sélection dans le widget "gérer les emails" (adresses, autorisation pour envoyer des emails, filtrage de certains paramètres systèmes)

## Comment utiliser CMDBuild

Ci-après vous trouverez les trois façons principales de l'utiliser avec la comparaison de la syntaxe SQL avec celle de CQL.

### Filtre simple

Le filtre simple permet de filtrer sur une valeur constante:

#### SQL:

```
SELECT * FROM “NomClasse” WHERE “Attribut”=Valeur
```

#### CQL:

```
from NomClasse where Attribut=Valeur
```
#### Exemple:

Choisir l'email dans le groupe "NomGroupe" dans l'attribut étendu manageEmail.

Dans la définition des attributs étendus, le paramètre adressesDestinataires est défini comme suit:

```
ToAddresses={cql:QueryCQLname.Email}
```

donc vous pouvez définir la requête CQL suivante:

```
QueryCQLname=”select Email from Role where Code='NomGroupe'”
```
### Filtre simple avec une variable côté serveur

Ce mode permet de réaliser un filtre sur une variable côté serveur:

#### SQL:

```
SELECT * FROM “NomClasse” WHERE “Attribut”=NomDeVariable
```

#### CQL:

```
from NomClasse where Attribut={server:NomDeVariable}
```

#### NOTES:

De façon à identifier le NomDeVariable de la variable (qui peut être affiché dans le formulaire), nous
avons utilisé la forme simple:

```
{server:NomDeVariable}
```

Pour les attributs tels que ceux de type LookUp ou référence, vous devez indiquer, avec la liste à puces, si
vous voulez l'Id ou la description:

```
{server:NomDeVariable.Id}
{server:NomDeVariable.Description}
```
#### EXEMPLE

Choisir l'email de l'utilisateur sélectionné via l'attribut "Requester" - de type de référence - , dans l'attribut étendu manageEmail.

Dans la définition de l'attribut étenu, le paramètre adressesDestinataires est défini comme suit:
```
ToAddresses={cql:QueryCQLname.Email}
```

de telle sorte que vous pouvez définir la requête CQL suivante:
```
nomeQueryCQL="select Email from Employees where Id={server:Requester.Id} & Status='A'"
```
### Filtre simple avec une variable côté client

Ce mode permet de filtrer sur une variable côté client, i.e. saisie dans le formulaire mais pas envoyée
au serveur.

Comme vous pouvez le voir, si le champ dans le formulaire n'est pas renseignée, la requête simple avec
une variable côté client signale une erreur puisque la valeur de la variable côté client n'a pas encore été définie.

#### SQL:

```
SELECT * FROM “ClassName” WHERE “Attribute”=ClientVariable
```

#### CQL:

``̀ 
from ClassName where Attribute={client:VariableName}
``̀ 

#### NOTES:

De façon à identifier la variable NomDeVariable (qui peut être affichée dans le formulaire), nous
utilisons la forme simple:

```
{client:NomDeVariable}
```

Pour des attributs de types tels que liste de valeurs (LookUp) ou référence, vous devez indiquer,
via la liste à puces, si vous voulez l'Id ou la description:

```
{client:VariableName.Id}
{client:VariableName.Description}
```

#### EXAMPLE:

L'exemple le plus courant est semblable à celui de la variable côté serveur (en remplaçant server par client).

## Exemples d'utilisation

Voci ci-dessous quelques exemples plus avancés d'utilisation de CQL.

### filtre les étages du bâtiment choisi (filtre sur référence)

#### CQL:

```
from Etage where [BatimentEtage].objects(Id=0{client.Batiment.Id})
```

#### NOTES:

[BatimentEtage] est le domaine entre la classe Batiment et la classe Etage (type 1:N).

### filtre les processus actifs (filtre sur référence)

### CQL:

```
from ordreDeTravail where EtageStatus='Started'
```

### récupère l'email de l'utilisateur qui ouvre un ticket (côté serveur)

#### CQL:

```
ToAddresses={cql:queryRequester.Email}
queryRequester="select Email from Employees where Id={server:Requester.Id} & Status='A'"
```

#### NOTES:

[Requester]est l'attribut de type référence du processus de gestion des tickets qui contient l'indication de qui a ouvert le ticket.

### récupère l'email de l'utilisateur qui ouvre un ticket (côté client) 

#### CQL:

```
ToAddresses={cql:queryRequester.Email}
queryRequester="select Email from Employees where Id={client:Requester.Id} & Status='A'"
```
#### NOTES:

[Requester] est l'attribut de référence du processus d'ouverture de ticket qui contient l'indication de qui a ouvert le ticket.

