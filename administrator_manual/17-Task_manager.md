# Gestionnaire de tâches

## Fonctions de base

Le gestionnaire de tâches regroupe toutes les fonctions de CMDBuild qui sont exécutées en arrière plan. De cette façon, vous pouvez avoir une vue générale sur les tâches actives et les paramètres associés.

Actuellement, les types de tâches concernées sont:

- gestion des emails entrant
- gestion des événements asynchrones
- gestion des événements synchrones
- planification des processus
- planification de l'assistant de connexion

![](../administrator_manual/17-Base_function-1.jpg)

Il est possible de réaliser les opérations suivantes:

- démarrer une tâche non active ![](../administrator_manual/17-Base_function-2.jpg)
- suspendre une tâche démarrée ![](../administrator_manual/17-Base_function-3.jpg)
- configurer une nouvelle tâche conformément aux typologies fournies ![](../administrator_manual/17-Base_function-4.jpg)
- modifier la configuration d'une tâche existante ![](../administrator_manual/17-Base_function-5.jpg)
- supprimer une tâche existante ![](../administrator_manual/17-Base_function-6.jpg)
- cloner une tâche existante ![](../administrator_manual/17-Base_function-7.jpg)

Avant de modifier une tâche démarrée, nous vous conseillons de la suspendre puis de la redémarrer.
La configuration actuelle du gestionnaire de tâches est stockée en base de données et à chaque fois que CMDBuild redémarre, les tâches actives sont automatiquement redémarrées.

Pour chaque typologie de tâche, il existe un assistant spécifique qui prend en charge sa configuration. Cela est décrit en détail dans les paragraphes suivants.

## Gestion de la boîte de réception

Pour cette tâche spécifique, vous devez attendre de recevoir un email dans un répertoire dédié d'un compte spécifique, vérifier qu'il correspond à ces critères de filtre préarrangés et réaliser les commandes recommandées.

De telles activités comprennent également:

- envoyer des notifications
- enregistrer des pièces jointes dans Alfresco
- démarrer des processus

Par exemple, vous pouvez démarrer cette fonction de façon à lancer des processus de gestion des incidents quand vous recevez un email des utilisateurs, vous pouvez également gérer toutes les réponses des utilisateurs dans l'étape suivante du même processus, etc.

L'assistant comporte quatre pages décrites ci-dessous.

### Page 1 de l'assistant

![](../administrator_manual/17-Inbox_management-1.jpg)

Les informations suivantes sont requises:

- "Description"
- "Démarrage à l'enregistrement" : la tâche démarre quand la configuration est enregistrée
- "Compte de messagerie" : compte de réception pour contrôler les emails reçus (voir la section dédiée de ce manuel)
- "Filtrer sur l'émetteur": comporte un éventuel filtre sur l'émetteur de l'email de façon à exclure les emails les moins intéressants; il est exprimé à l'aide d'une expression rationnelle et peut être défini via une fenêtre surgissante adaptée (dans la capture d'écran sur le filtre, il prendra en compte seulement les emails contenant la chaîne "example1.com" ou "example2.com"
- "Filtrer sur le sujet": il comporte un éventuel filtre sur le titre de l'email de façon à exclure les emails les moins intéressants; il est exprimé à l'aide d'une expression rationnelle et peut être défini via une fenêtre surgissante dédiée;

### Page 2 de l'assistant

![](../administrator_manual/17-Inbox_management-2.jpg)

Les informations liées aux façons de planifier la tâche de contrôle des emails reçus doivent être renseignées:

- "Mode simple", avec les options "toutes les heures", "chaque jour", "chaque mois", "chaque année"
- "Mode avancé", avec les mêmes options et la même syntaxe que l'outil de planification "cron" disponible sous Linux

### Page 3 de l'assistant

![](../administrator_manual/17-Inbox_management-3.jpg)

Les informations suivantes sont requises pour les groupes:

- "Analyse du corps du texte
    - "Délimiteur du début de mot clef": il permet d'indiquer le délimiteur de début d'un éventuel champ clef présent dans l'email
    - "Délimiteur de fin de mot clef:" il permet d'indiquer le délimiteur de fin d'un éventuel champ clef présent dans l'email
    - "Délimiteur de début de valeur": il permet d'indiquer le délimiteur de début des champs indiquant une valeur dans l'email
    - "Délimiteur de fin de valeur": il permet d'indiquer le délimiteur de fin des champs indiquant une valeur dans l'email
- "Envoi d'un email de notification"
    - "Modèle d'email": il nécessite de choisir un des modèles pré-configurés avec la fonction adaptée (menu de gestion des emails)
- "Enregistrer une pièce jointe dans Alfresco"
    - "Catégorie": nécessite de choisir la catégorie qui doit être associée à la pièce jointe (qui sera enregistrée dans le système de gestion de la documentation et associée avec la fiche de l'email)

#### Note

Le mécanisme d'analyse du corps de l'email vous permet de reconnaître et de gérer dans le processus tous les attributs dont les valeurs ont été insérées dans le corps de l'email, par exemple:

```
Corps du message
[clef]Demandeur[/clef]<valeur>Jean Dupont</valeur>
[clef]Description[/clef]<valeur>L'imprimante ne s'allume pas</valeur>
[clef]Priorité[/clef]<valeur>Moyenne</valeur>
```

### Page 4 de l'assistant

Elle ne contient qu'un test utile pour demander le démarrage instantané de la tâche.

## Evénements asynchrones

Pour ces tâches particulières, vous devez vérifier que certaines conditions sont remplies par les données de la CMDB en appliquant un filtre pré-défini et en réalisant les instructions recommandées.

De telles activités comprennent également:

- envoi de notifications
- démarrage de processus
- exécution de script

Cette fonction peut être utilisée pour configurer l'envoi d'un email si la garantie d'un élément est sur le point d'expirer ou de lancer un processus de demande si un ticket dépasse la durée autorisée pour sa résolution, etc.

Actuellement, vous pouvez configurer des événements pour des classes standards, par pour les processus.

L'assistant comporte trois pages décrites ci-dessous.

### Page 1 de l'assistant

![](../administrator_manual/17-Asynchronous_events-1.jpg)

Les informations suivantes sont nécessaires:

- "Description"
- "Démarrage à l'enregistrement" : la tâche démarre quand la configuration est enregistrée
- "Classe cible": vous devez contrôler les données de cette classe de façon à évaluer si les conditions suivantes sont vérifiées

### Page 2 de l'assistant

![](../administrator_manual/17-Asynchronous_events-2.jpg)

La deuxième page de l'assistant vous permet de définir un filtre de contrôle avec les mêmes mécanismes que ceux disponibles pour configurer les filtres standards de CMDBuild.
Pour une description complète du mode de configuration, voir la section dédiée dans ce manuel.

### Page 3 de l'assistant

Les informations liées au mode de planification de la tâche sont requises. La tâche contrôlera périodiquement les données pour évaluer si les conditions du filtre sont vérfiées:

- "Mode simple", avec les options "toutes les heures", "chaque jour", "chaque mois", "chaque année"
- "Mode avancé", avec les mêmes options et la même syntaxe que l'outil de planification "cron" disponible sous Linux

### Page 4 de l'assistant

Elle ne comporte que le test utile pour demander l'envoi d'un email de notification indiquant le compte et le modèle.

## Evénements synchrones

Pour cette tâche spécifique, vous devez intercepter les opérations de la CMDB et mener à bien les instructions recommandées.

De telles activités comprennent également:

- l'envoi de notifications
- le lancement de processus
- l'exécution de script

Cette fonction peut être utilisée pour configurer le lancement d'un email au cas où un nouvel employé a été ajouté dans CMDBuild pour lancer un processus de désapprovisionnement (suppression du compte, récupération des rôles détenus dans le cadre du travail, transfert des tâches à des collègues, etc.), si un employé démissionne, etc.

Actuellement, vous pouvez configurer des événéments sur les classes standard, pas sur les processus.

L'assistant comporte trois pages, décrites ci-dessous.

### Page 1 de l'assistant

![](../administrator_manual/17-Synchronous_events-1.jpg)

Les informations suivantes doivent être renseignées:

- "Description"
- "Lancement à l'enregistrement": la tâche démarre quand la configuration est enregistrée
- "Etape": il expose les données de contrôle (dès qu'une fiche est ajoutée, avant qu'une fiche ne soit modifiée, dès qu'une fiche a été modifiée, avant qu'une fiche ne soit supprimée)
- "Groupe": pour limiter le contrôle des modifications de certains groupes prédéfinis
- "Classe"; pour contrôler les opérations en cours de réalisation

### Page 2 de l'assistant

![](../administrator_manual/17-Synchronous_events-2.jpg)

La deuxième page de l'assistant vous permet de définir un filtre de contrôle doté des mêmes mécanisme que celui disponible pour la configuration des filtres standards de CMDBuild.

Pour une description complète du mode de configuration, voir la section dédiée de ce manuel.

### Page 3 de l'assistant

![](../administrator_manual/17-Synchronous_events-3.jpg)

Elle comporte le test très utile pour réclamer l'envoi d'un email de notification en indiquant son compte et le modèle.
Elle permet également le démarrage automatique d'un processus de gestion des changements, en indiquant:

- "Processus": le processus en cours de démarrage
- "Conformité de la liste des attributs", pour initialiser un ou plusieurs attributs de processus au démarrage

## Processus

Cette fonction vous permet de planifier un processus configuré dans CMDBuild
L'assistant comporte deux pages décrites ci-dessous.

### Page 1 de l'assistant

![](../administrator_manual/17-Workflow-1.jpg)

Les informations suivantes sont requises:

- "Description" de la nouvelle tâche
- "Lancement à l'enregistrement": la tâche démarre quand la configuration est enregistrée
- "Processus"
- "Conformité de la liste des attributs", pour initialiser un ou plusieurs attributs de processus

### Page 2 de l'assistant

Les informations liées aux façon de programmer les tâches est nécessaire:

- "Mode simple", avec les options "toutes les heures", "chaque jour", "chaque mois", "chaque année"
- "Mode avancé", avec les mêmes options et la même syntaxe que l'outil de planification "cron" disponible sous Linux

## Assistant pour les connecteurs

Cette fonction vous permet de planifier un connecteur de synchronisation de données entre des sources externes et CMDBuild.

La gestion des services IT par des compagnies et des institutions de grande et moyenne taille est nécessairement menée via des systèmes d'information plus spécialisés qui doivent être capables de coopérer dans la gestion de leurs activités et informations.

Collecter et le contrôle manuel des informations gérées dans la CMDB peut provoquer des problèmes de délais ou créer des incohérences lors de la mise à jour des données. C'est pourquoi c'est mieux de les mettre à jour automatiquement. 

C'est pourquoi la configuration des connecteurs vers les systèmes externes devient importante de façon à synchroniser les données dans CMDBuild (le système central de CMDB) qui sont principalement gérées dans d'autres applications spécialisées (inventaire, systèmes de supervision,dépôts LDAP, applications ERP et RH, etc.).

L'assistant connecteurs vous permet de résoudre les cas les plus simples dans lesquels les règles de "mappage" ne nécessitent pas un logique applicative spécifique. Pour des besoins plus complexes, vous pouvez utiliser le connecteur de base (Basic Connector) et le connecteur avancé (Advanced Connector).

Nous décrivons ci-dessous les formulaires de configuration de l'assistant pour les connecteurs. Voir le manuel des connecteurs CMDBuild pour une comparaison plus détaillée entre les trois solutions.

### Page 1 de l'assistant

![](../administrator_manual/17-Wizard_connector-1.jpg)

Les informations suivantes sont demandées:

- "Description" de la nouvelle tâche 
- "Lancement à l'enregistrement" : la tâche démarre quand la configuration est enregistrée
- "Envoyer une notification par email", en indiquant si nécessaire:
  - "compte"
  - "modèle" de l'email

### Page 2 de l'assistant

Les informations liées au mode de planification des tâches sont requises. La tâche contrôlera périodiquement les données à évaluer si les conditions du filtre sont vérifiées:

  - "Mode simple", avec les options "toutes les heures", "chaque jour", "chaque mois", "chaque année"
  - "Mode avancé", avec les mêmes options et la même syntaxe que l'outil de planification "cron" disponible sous Linux

### Page 3 de l'assistant

![](../administrator_manual/17-Wizard_connector-2.jpg)

Les informations suivantes liées aux sources de données sont requises:

- "Type" de la base de données source (les bases de données PostgreSQL, MySQL, Oracle et SQLServer sont prises en charge, si les pilotes d'accès correspondant sont installés)
- "Adresse" de la base de données source
- "Port" de la base de données source
- "Nom" de la base de données source 
- "Nom d'utilisateur" pour l'accès
- "Mot de passe" pour l'accès
- "Filtre d'entité", vous pouvez choisir les entités que vous voulez afficher dans la définition des critères de mappage (ex: certaines vues avec le même préfixe, pré-arrangées pour la requête du connecteur)

### Page 4 de l'assistant

![](../administrator_manual/17-Wizard_connector-3.jpg)

Pour chaque entité de la source de données externe impliquée dans les activités de synchronisation, les informations suivantes sont requises:

- "Entité externe": nom de la table, vue ou autre entité
- "Classe CMDBuild": classe CMDBuild pour la synchronisation des données
- "Création": si activé, crée dans CMDBuild toutes les fiches de données disponibles dans le système externe et pas dans CMDBuild.
- "Modification": si activé, crée dans CMDBuild toutes les fiches de données disponibles dans le système externe et pas dans CMDBuild
- "Annulation": si activé, crée dans CMDBuild toutes les fiches de données disponibles dans le système externe et pas dans CMDBuild
- "Mode d'annulation": montre si l'annulation doit supprimer la fiche de données dans CMDBuild ou modifier l'état de la fiche actuelle

### Page 5 de l'assistant

![](../administrator_manual/17-Wizard_connector-4.jpg)

Pour tous les attributs de chacune des entités à synchroniser, les informations suivantes sont requises:

- "Entité externe": nom de la table, vue ou autre entité
- "Attribut externe": le nom de la colonne / information de l'entité externe
- "Classe CMDBuild": classe CMDBuild à synchroniser
- "Attribut CMDBuild": attribut CMDBuild à synchroniser
- "Clef": identifie l'attribut clef de chaque entité (nécessaire pour comparer les données et décider le type de synchronisation
