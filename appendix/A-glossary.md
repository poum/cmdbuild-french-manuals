# ANNEXE: Glossaire

## Pièce jointe

Une pièce jointe est un fichier associé à une fiche.

Les pièces jointes contenant du texte (PDF, Open Office, Microsoft Word, etc.) sont indexés en mode
texte complet (_full text_) de telle sorte qu'elles peuvent apparaître dans des résultats de recherche.

## Étape de workflow

"Activité" fait référence à l'une des étapes qui constituent le processus.

Une activité possède un nom, un exécuteur, un type, d'éventuels attributs ou méthodes avec des
instructions (API CMDBuild) à exécuter.

Une instance de processus est un processus particulier qui a été activé automatiquement par une application 
ou manuellement par un opérateur.

Voir également: processus

## Attribut

Le terme fait référence à un attribut d'une classe CMDBuild.

CMDBuild vous permet de créer de nouveaux attributs (dans les classes et les domaines) ou de modifier
des attributs existants.

Par exemple, dans la classe "fournisseur", les attributs sont: nom, adresse, numéro de téléphone, etc.

Chaque attribut correspond, dans le module de gestion, à un champ de formulaire et à une colonne dans 
la base de données.

Voir également: Classe, Domaine, Rapport, Superclasse, Type d'attribut

## BIM

Méthode ayant pour objet de gérer le cycle de vie complet d'un bâtiment: depuis sa construction,
son utilisation et sa maintenance, jusqu'à sa démolition s'il y a lieu.

La méthode BIM (Building Information Modeling pour modélisation des informations des bâtiments) est gérée 
par plusieurs programmes IT qui peuvent interagir via un format ouvert d'échange de données, appelé IFC  
(Industry Foundation Classes).

Voir également: GIS

## CI ou EC

Nous définissons comme CI (_Configuration Item_ ou Élément de configuration ou EC) chaque élément qui fournit des services IT à l'utilisateur
et qui possède un niveau de détail suffisant pour sa gestion technique.

Des exemples de CI comprennent: des serveurs, des stations de travail, des logiciels, des systèmes d'exploitation, des imprimantes, etc.

Voir également: Configuration

## CLASSE

Une classe est un type de données complexe possédant un ensemble d'attributs qui décrivent ce type de données.

Une classe modèlise un objet qui doit être géré dans la CMDB, tels qu'un ordinateur, un logiciel, un fournisseur de service, etc.

CMDBuild permet à l'administrateur - via le module d'administration - de définir de nouvelles classes ou de supprimer / modifier celles qui existents.

Les classes sont représentées par des fiches, et, dans la base de données, par des tables créées automatiquement au moment de leur définition.

Voir également: Fiche, Attribut

## CONFIGURATION

Le processus de gestion de la configuration est conçu pour maintenir à jour et disponibles aux autres processus les éléments d'information (CI), leur relationss et leur historique.

C'est l'un des processus majeurs ITIL gérés par l'application.

Voir également: CI, ITIL

## TABLEAU DE BORD

Dans CMDBuild, un tableau de bord correspond à une collection de différents graphiques, de façon à ce que puissiez immédiatement identifier certains paramètres clefs (KPI pour Key Parameter Indicator) liés à un aspect particulier de la gestion d'un service IT.

Voir également: Rapport

## BASE DE DONNÉES

Le terme fait référence à une collection structurée d'information, hébergée sur un serveur, ainsi que le logiciel utilitaire qui gère cette information pour des tâches telles que l'initialisation, l'allocation, l'optimisation, la sauvegarde, etc.

CMDBuild s'appuie sur PostgreSQL, la base de données open source la plus puissante, la plus fiable, la plus professionnelle, et fait usage de ses fonctionnalités avancées et sa structure orientée objets.

## DOMAINE

Un domaine est une relation entre deux classes.

Un domaine a un nom, deux descriptions (directe et inverse), des codes de classe, une cardinalité et des attributs.

L'administrateur système, en utilisant le module d'admjnistration, peut définir de nouveaux domaines ou supprimer / modifier ceux qui existent.

Il est possible de définir des attributs personnalisés pour chaque domaine.

Voir également: Classe, Relation

## Filtre de données

Un filtre de données est une restriction sur la liste des éléments contenus dans une classe, obtenue en indiquant des conditions booléennes (égal, différent, contient, commence par, etc.) sur les valeurs possibles qui sont acceptées par chacun des attributs de la classe.

Les filtres de données peuvent être définis et utilisés immédiatement, ou bien ils peuvent être enregistrés par l'opérateur puis appelés de nouveau (par le même opérateur ou par des opérateurs d'autres groupes utilisateurs qui ont obtenu de l'administrateur système la permission de les utiliser)

Voir également: Classe, Vue

## GIS

Un GIS est un système capable de produire, de gérer et d'analuser des données spatiales en associant des éléments géographiques à une ou plusieurs descriptions alphanumériques.

Les fonctionnalités GIS dans CMDBuild vous permettent de créer des attributs géographiques (en plus des attributs standards) qui représentent sur des plans / des cartes, des positions de marqueurs (éléments), des lignes brisées (lignes de cables) et des polygones (étages, pièces, etc.).

Voir également: BIM

## FRAMEWORK IHM ou GUI FRAMEWORK

C'est une interface utilisateur que vous pouvez totalement personnaliser. Il est conseillé de fournir un accès simplifié à l'application. Il peut être fourni dans n'importe quel portail web et peut être utilisé avec CMDBuild via les webservices REST standard.

Voir également: Mobile, Webservices

## ITIL

Système de "meilleures pratiques" qui a établi un "standard de facto"; c'est un système non propriétaire pour la gestion des services IT, suivant un schéma orienté processus (Information Technology Infrastructure Library).

Les processus ITIL comprennent: support des services, gestion des incidents, gestion des problèmes, gestion des changements, gestion des configurations et gestion des livraisons.

Pour chaque processus, ITIL fournit une description, des composants de bae, des critères et des outils pour la gestion de la qualité, des rôles et des responsabilités pour les ressources impliquées, des points d'intégration avec d'autres processus (pour éviter les duplications et le manque d'efficacité).

Voir également: Configuration

## LOOKUP ou LISTE DE VALEURS

Le terme "Lookup" ou "Liste de valeurs" fait référence à une paire de valeurs (Code, Libellé) fixée par l'administrateur dans le module d'administration.

Ces valeurs sont utilisées pour contraindre les choix de l'utilisateur (au moment de remplir un formulaire) à l'une des valeurs prédéfinies.

Grâce au module d'administration, il est possible de définir de nouvelles tables de listes de valeur conformément aux besoins de l'organisation.

## Mobile

C'est une interface utilisateur pour les outils mobiles (smartphones et tablettes). Elle est implémentée comme une application multi-plateforme (iOS, Android) et peut être utilisée avec CMDB via les ebservices REST.

Voir également: GUI, IHM, Framework, Webservices

## PROCESSUS 

Le terme "processus" (ou workflow) fait référence à une séquence d'étapes qui réalisent une action.

Chaque procesus se situera sur des éléments spécifiques et sera exécuté par des utilisateurs particuliers.

Un processus est activé en démarrant un nouveau processus (en remplissant le formulaire associé) et se termine quand la dernière étape du workflow est exécutée.

Voir également: étape de workflow

## RELATION ou LIEN

Une relation est un lien entre deux fiches CMDBuild ou, en d'autres mots, une instance d'un domaine donné.

Une relation est définie pr une paire d'identifiants uniques de fiches, un domaine et d'éventuels attributs.

CMDBuild permet aux utilisateurs, via le module de gestion, de définir de nouvelles relations entre les fiches enregistrées dans la base de données.

Voir également: Classe, Domaine

## RAPPORT 

Le terme fait référence à un document (PDF ou CSV) contenant des informations extraites d'une ou plusieurs classes et domaines liés.

Les utilisateurs de CMDBuild exécutent des rapports en utilisant le module de gestion; les définitions des rapports sont enregistrées dans la base de données.

Voir également: Classe, Domaine, Base de données

## FICHE

Le terme "Fiche" fait référence à un élement enregistré dans une classe.

Une fiche est définie par un ensemble de valeurs, i.e. les attributs définis pour sa classe.

Les utilisateurs de CMDBuild, via le module de gestion, peuvent enregistrer de nouvelles fiches et mettre à jour / supprimer celles qui existent.

Les informations de la fiche sont enregistrées dans la base de données et, plus précisément, danes les tables/colonnes créées pour cette classe (Module d'administration).

Voir également: Classe, Attribut

## SUPERCLASSE

Une superclasses est une classe abstraite utilisée pour définir des attributs partagés entre des classes. À partir de la classe abstraite, vous pouvez dériver des classes réelles qui contiennent des données et incluent à la fois des attributs partagés (définis dans la superclasse) et des attributs spécifique à la classe elle-même.

Par exemple, vous pouvez définir la superclasse "Ordinateur" avec quelques attributs génériques (RAM, HD, etc.) puis définir les classes dérivées "Ordinateur de bureau", "Portable", "Serveur", chacun possédant des attributs spécifiques.

Voir également: Classe, Attribut

## TYPE D'ATTRIBUT

Chaque attribut possède un type de données qui représente les informations et la gestion de l'attribut.

Le type d'attribut est défini en utilisant le module d'administration et peut être modifié avec certaines limitations, selon que des données sont déjà enregistrées dans le système ou pas.

CMDBuild gère les types d'attributs suivants: "Booléen", "Date", "Décimal", "Double", "Inet" (adresse IP), "Entier", "Liste de valeurs" ou "Lookup" (listes définies dans "Configuration / Listes de valeurs"), "Références" (clef étrangère), "Chaîne de caractères", "Texte", "Estampille".

Voir également: Attribut

## VUE

Une vue n'inclut pas seulement tout le contenu d'une classe CMDB, c'est un groupe de fiches définies d'une manière logique.

En particulier, une vue peut être définie dans CMDBuild en appliquant un filtre à une classe (si bien qu'elle ne contiendra qu'un ensemble réduit de lignes du même type) ou en indiquant une fonction SQL qui extraiera des attributs d'une ou de plusieurs classes liées.

Le premier type de vue conserve toutes les fonctionnalités disponibles pour une classe, tandis que le second ne permet que l'affichage et la rechecher à l'aide de filtres rapides.

Voir également: Classe, Filtre

## WEBSERVICE ou SERVICE WEB

Un service web est une interface qui décrit une série de méthodes, disponibles via le réseau et fonctionnant en utilisant des messages XML.

Avec les services web, une application permet à d'autres applications d'interagir avec ses méthodes.

CMDBuild comprend des webservices SOAP et REST.

## WIDGET

Un Widget (pour Window Gadget) est un composant d'une interface graphique qui améliore les interactions de l'utilisateur avec l'application.

CMDBuild utilise des widgets (présentés sous forme de boutons) qui peuvent être placés sur des fiches ou des processus. Les boutons ouvre des fenêtres surgissantes qui vous permettent d'ajouter des informations complémentaires puis d'afficher le résultat de la fonction choisie.
