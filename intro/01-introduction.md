#Introduction

CMDBuild est une application web Open Source conçue pour modéliser et gérer les actifs et les
services contrôlé par les départements en charge des technologies de l'information, c'est pourquoi
elle gère les opérations de processus associées si nécessaire, conformément aux meilleures pratiques ITIL.

La gestion d'une base de données de configurations (CMDB: Configuration Management Database Build)
implique de maintenir à jour et accessibles pour les autres processus la base de données des composants utilisés, de leurs relations et de leurs changements au cours du temps.

Avec CMDBuild, l'administrateur système peut construire et étendre sa propre CMDB (d'où le nom du projet), modéliser la CMDB selon les besoins de sa compagnie; le module d'administration vous permet d'ajouter progressivement de nouvelles classes d'éléments, de nouveaux attributs et de nouvelles relations. Vous pouvez également définir des filtres, des vues et des droits d'accès limités aux lignes et aux colonnes de chaque classe.

CMDBuild fournit une prise en charge complète des meilleures pratiques ITIL qui est devenu désormais un "standard de facto", c'est un système non propriétaire pour la gestion des services avec des critères orientés processus.

Grâce à l'intégration d'un moteur de workflow, vous pouvez créer de nouveaux processus de workflow à l'aide d'éditeurs externes puis les importer / exécuter au sein de l'application CMDBuild conformément aux automatismes configurés. 

Un gestionnaire de tâches intégré à l'interface utilisateur du module d'administration est
également disponible. Il permet de gérer différentes opérations (lancement de processus,
envoi et réception d'email, exécution de connecteurs) et le contrôle des données de la CMDB
(événements synchrones et asynchrones). Selon ce qu'il détecte, il envoie des notifications,
lance des processus et exécute des scripts.

CMDBuild inclut également JasperReports, un moteur de rapports open source qui vous permet 
de créer des rapports; vous pouvez concevoir (avec un éditeur externe), importer et exécuter 
des rapports personnalisés dans CMDBuild.

Il est alors possible de définir des tableaux de bord constitués de graphiques qui montre immédiatement la situation de certains indicateurs du système actuel (KPI).

CMDBuild intègre Alfresco, le système populaire open source de gestion documentaire. Vous
pouvez joindre des documents, des images et d'autres fichiers.

Mieux encore, vous pouvez utiliser les fonctions géographiques pour géoréférencer et afficher
des actifs sur une carte géographique (services cartographiques externes) et / ou des plans
de bureau (GeoServer local) et des fonctionnalités BIM pour voir des modèles 3D (format IFC).

Le système inclut également des webservices SOAP et REST pour implémenter des solutions
d'interopérabilité avec SOA.

CMDBuild comprend deux frameworks appelés Basic Connector (Connecteur de base) et Advanced Connector (Connecteur avancé) qui sont capables - via les webservices SOAP - de synchroniser les informations enregistrées dans la CMDB avec des sources de données externes, par exemple via des systèmes
d'inventaires automatisés (tels que le logiciel open source OCS Inventory) ou via des systèmes
de virtualisation ou de supervision.

Via les webservices REST, le framework d'IHM CMDBuild permet de produire des pages web personnalisées sur des portails externes capable d'interagir avec la CMDB.

Une interface utilisateur pour les outils mobiles (smartphones et tablettes) est également disponible.  Elle est implémentées comme une application multi plate-forme (iOS, Android) et est liée à la CMDB via les webservices REST.

##Modules de CMDBuild

L'application CMDBuild comporte deux modules principaux:

- le module d'administration pour la définition initiale et les modifications ultérieures du modèle 
de données et la configuration de base (typologie et relation des classes, utilisateurs et autorisations,
tableaux de bord, téléchargement de rapports et de workflows, options et paramètres)
- le module de gestion, utilisé pour gérer les fiches et les relations, ajouter des pièces jointes, exécuter des processus 
de processus, consulter des tableaux de bord et exécuter des rapports;

Le module d'administration n'est disponible qu'aux utilisateurs ayant le rôle "administrateur"; le module de gestion
est utilisé par tous les utilisateurs qui consultent et modifient les données.

##Documentation disponible

Ce manuel est dédié au module de gestion à l'aide duquel les opérateurs de service IT pourront
mettre à jour et consulter des fiches, lancer des processus, exécuter des rapports, géoréférencer
des éléments et exécuter d'autres fonctions utilitaires.

Vous trouverez tous les manuels sur le site web officiel [http://www.cmdbuild.org](http://www.cmdbuild.org):

- vue d'ensemble ("Manuel d'aperçu général")
- administration système ("Manuel administrateur")
- installation et gestion du système ("Manuel technique")
- configuration des workflow ("Manuel des workflow")
- détails et configuration des webservices ("Manuel Webservices")
- connecteurs pour synchroniser les données via des systèmes externes ("Manuel Connecteurs")
