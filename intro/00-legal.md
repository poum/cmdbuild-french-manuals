Aucune partie de ce document ne peut être reproduite, en totalité ou partiellement, sans la permission
expresse écrite de Tecnoteca s.r.l.

CMDBuild ® utilise beaucoup de formidables technologies issues de la communauté open source:
PostgreSQL, Apache, Tomcat, Eclipse, Ext JS, JasperReports, IReport, Enhydra Shark, TWE, OCS
Inventory, Liferay, Alfresco, GeoServer, OpenLayers, Prefuse, Quartz, BiMserver.
Nous sommes reconnaissants pour les formidables contributions qui ont conduit à la création de ces produits.

CMDBuild ® est un projet de Tecnoteca Srl. 

![](../intro/tecnoteca.png)

Tecnoteca est responsable de la conception et du développement du logiciel, c'est le mainteneur
officiel et a déposé le logo CMDBuild.

Dans le projet, la municipalité d'Udine était également impliquée en tant que client initial.

![](../intro/udine.png)

CMDBuild ® est livré sous licence open source [AGPL](http://www.gnu.org/licenses/agpl-3.0.html).

CMDBuild ® est une marque déposée de Tecnoteca Srl.
Chaque fois que le logo CMDBuild® est utilisé, le mainteneur officiel "Tecnoteca srl" doit être mentionné; de plus, 
il doit y avoir un lien vers le site web officiel [http://www.cmdbuild.org](http://www.cmdbuild.org).

Le logo CMDBuild ® :

- ne peut pas être modifié (couleurs, proportions, forme, police de caractères) en aucune façon, et ne peut pas être intégré dans d'autres logos
- ne peut pas être utilisé comme logo d'une organisation, pas plus que la compagnie qui l'utilise ne peut apparaître comme auteur / propriétaire / mainteneur du projet
- ne peut pas être retiré de l'application, et en particulier de l'en-tête en haut de chaque page

Le site web officiel est [http://www.cmdbuild.org](http://www.cmdbuild.org).

La [traduction française](https://poum@bitbucket.org/poum/cmdbuild-french-manuals.git) a été réalisée par ![Philippe Poumaroux](http://philippe.poumaroux.free.fr) à partir de ce manuel. Cette traduction réalisée en accord avec Tecnoteca est également placée sous licence [AGPL](http://www.gnu.org/licenses/agpl-3.0.html). Pour ce faire, les logiciels libres suivant ont principalement été utilisés:

- vim
- pandoc
- pdftotext
- pdfimages
- convert
- git
- Firefox
- Evince
- LibreOffice
- LanguageTool
