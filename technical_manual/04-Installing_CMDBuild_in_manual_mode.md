#Installer CMDBuild manuellement

##Démarrage

Avant de commencer à installer CMDBuild, vous devez désarchiver le répertoire cmdbuild.war.

Pour faire ça, vous devez le copier dans le répertoire webapps de Tomcat et attendez que le serveur d'application crée le répertoire cmdbuild.

Vérifiez que le pilote JDBC PostgreSQL est présent dans le répertoire lib de Tomcat![1][note]

Note: Dans ce qui suit, nous noterons:

- {CMDBUILD}, le répertoire CMDBuild dans webapps
- {ALFRESCO}, le répertoire alfresco dans webapps
- {SHARK}, le serveur Together Workflow Server dans webapps

Attention: l'installation suivante implique la création d'une nouvelle base de données vide. Si vous avez une base de données pré-existante, passez directement au paragraphe "Configurer l'application". Si vous avez déjà une base de données CMDBuild, passez à la section "Configuration de l'application".

##Installation de la base de données

Pour installer manuellement, vous devez faire ce qui suit:

- utiliser un outil avec une interface graphique (par exemple pgAdmin3, dédié à PostgreSQL) ou en igne de commande pour créer la base de données en précisant son nom, par exemple 'cmdbuild':

```sql
  CREATE DATABASE cmdbuild
        WITH OWNER cmdbuilduser
        ENCODING = 'UTF8';
```

- accéder à la nouvelle base de données et créer le langage plpgsql:

```sql
  CREATE LANGUAGE plpgsql;
```

- pour créer une base de données avec des données de démonstration, lancez dans l'ordre alphabétique les scripts qui se trouvent dans le répertoire {CMDBUILD}/WEB-INF/sql/base_schema, ou, alternativement, le fichier {CMBUILD}/WEB-INF/sql/sample_schemas/demo_schema.sql
- si vous décidez d'utiliser une base de données vide, vous devez trouver le nom du dernier patch disponible en regardant dans le répertoire {CMDBUILD}/WEB-INF/patches (selon l'ordre alphabétique sans l'extension, par exemple "1.2.3") puis exécuter ces commandes:

```sql
  SELECT cm_create_class('Patch', 'Class', 'DESCR: |MODE: reserved|STATUS: active|SUPERCLASS:
false|TYPE: class');

  INSERT INTO "Patch" ("Code") VALUES ('1.2.3');

```

- exécuter les commandes SQL suivantes pour créer l'utilisateur "super utilisateur" (dans l'exemple, il aura "admin" comme nom et comme mot de passe):


```sql
 INSERT INTO "User" ("Status", "Username", "IdClass", "Password", "Description")
       VALUES ('A', 'admin', '"User"', 'DQdKW32Mlms=', 'Administrator');
 INSERT INTO "Role" ("Status", "IdClass", "Administrator", "Code", "Description")
       VALUES ('A', '"Role"', true, 'SuperUser', 'SuperUser');
 INSERT INTO "Map_UserRole" ("Status", "IdClass1", "IdClass2", "IdObj1", "IdObj2", "IdDomain")
       VALUES ('A', '"User"'::regclass,'"Role"'::regclass, currval('class_seq')-2, currval('class_seq'), '"Map_UserRole"'::regclass);
```

A ce stade, vous avez une base de données vide utilisable avec le système CMDBuild.

##Configuration de l'application

Vous pouvez configurer manuellement l'application en modifiant certains fichiers. Si Tomcat a déjà été démarré, vous devez l'arrêter et aller dans le répertoire {CMDBUILD}/WEB-INF/conf.

Vous devrez alors faire ce qui suit:

- fichier cmdbuild.conf
    - l'ouvrir avec un éditeur de texte
    - choisir la langue par défaut du système en modifiant le "language" (les valeurs sont IT et EN IL DOIT Y EN AVOIR D AUTRES DONT FR; en donnant à "languageprompt" la valeur "true", vous pourrez choisir la langue dans l'interface de connexion)
    - enregistrer et fermer le fichier
- fichier database.conf
    - décommenter les trois lignes
    - indiquer le nom de la base de données à utiliser dans "db.url"
    - sous "db.username" et "db.password", indiquez les paramètres d'accès à la base de données
    - enregistrer et fermer le fichier

L'installation est maintenant terminée, relancez Tomcat et connectez-vous dans CMDBuild

##Configuration de l'interface entre CMDBuild et Together Workflow Server

Vous pouvez configurer Together Workflow Server en modifiant certains fichiers de configuration. Si Tomcat a déjà été lancé, vous devez l'arrêter et exécuter les opérations suivantes:

###1) Adresse de la base de données

Dans le fichier de la webapp shark {SHARK}/META-INF/context.xml, vous devez configurer l'adresse de la base de données en modifiant la ligne suivante:

```
  url="jdbc:postgresql://localhost/${cmdbuild}"
```

###2) URL et utilisateur du service

Dans le fichier {SHARK}/conf/Shark.conf, définissez correctement les paramètres suivants:

``̀ 
  # CMDBUild connection settings

  org.cmdbuild.ws.url=http://localhost:8080/cmdbuild/

  org.cmdbuild.ws.username=workflow

  org.cmdbuild.ws.password=changeme

``̀ 
en insérant l'url correspondant à CMDBuild, le nom et le mot de passe du compte technique utilisé par Together Workflow Server pour accéder aux services de CMDBuild.

###3) Configuration du compte technique pour Together Workflow Server

Avec un éditeur de texte, ouvrez le fichier {CMDBUILD}/WEB-INF/conf/auth.conf et décommentez la ligne 

```
  serviceusers.prigileged=workflow

```
Si nécessaire, remplacez 'workflow' par la valeur insérée dans le paramètre org.cmdbuild.ws.username du point précédant.

###4) Démarrage des services

Le service Tomcat Shark doit déjà être actif quand vous commencez à travailler avec les processus en utilisant l'interface utilisateur de CMDBuild.

Sous Windows, le chemin doit être exprimée en inversant les barres:

```
  DatabaseManager.ConfigurationDir=C:/srv/tomcat/webapps/shark/conf/dods
```

ou en les doublant:

```
  DatabaseManager.ConfigurationDir=C:\\srv\\tomcat\\webapps\\shark\\conf\\dods
```

Une manière de vérifier le bon fonctionnement de l'instance Shark consiste à consulter son fichier journal.

Avant de poursuivre la configuration, redémarrez l'instance Tomcat de CMDBuild.

###5) Droits d'accès

En utilisant le module d'administration de CMDBuild, vous devez accéder au menu de configuration et:

- activer les processus (via la case à cocher appropriée)
- indiquer l'URL à laquelle le service Shark répond

###6) Créer l'utilisateur dans CMDBuild et les accorder des droits

En utilisant le module d'administration de CMDBuild, vous devez accéder à "Utilisateurs et groupes" et :

- dans le menu "Utilisateurs", créer un nouvel utilisateur dont l'identifiant et le mot de passe dorrespondent aux valeurs définies dans les paramètres de org.cmdbuild.ws.username et org.cmdbuild.ws.password à l'étape 2
- ajouter cet utilisateur nouvellement créé dans un groupe disposant des droits d'administration (par exemple, SuperUsers ou un groupe dont l'indicateur administrateur est coché)

{note] Le pilote JDBC peut être téléchargé sur http://jdbc.postgresql.org/download.html
