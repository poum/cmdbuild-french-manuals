# Installer CMDBuild en utilisant l'interface graphique

## Pour démarrer

L'installation de CMDBuild nécessite que vous ayez déjà installé les produits de base nécessaires pour son fonctionnement, à savoir:

- la base de données PostgreSQL (elle doit être lancée et accessible)
- le serveur d'application Tomcat (non démarré)
- le DMS Alfresco (si vous avez l'intention de l'utiliser pour gérer les documents joints)
- l'environnement Java

Comme première étape, il est nécessaire de s'assurer que ces produits ont été téléchargés et installés, via les liens mentionnés dans le chapitre précédent.

**Attention :** vous devez faire attention à utiliser des répertoires qui ne contiennent pas d'espace où que ce soit dans leur chemin d'accès complet.

Puis vous devez lancer le service PostgreSQL et éventuellement (mais ce n'est pas obligatoire), le service Alfresco.

## Installation de CMDBuild

Une fois les opérations mentionnées précédemment réalisées, l'installation et la configuration standard de CMDBuild est très simple.

Pour installer CMDBuild, il suffit:

- de télécharger à partir du site du projet (http://www.cmdbuild.org/download) le fichier compressé (fichier ZIP) correspondant à la dernière version livrée
- de copier le répertoire CMDBuild-{version}.war (qui se trouve à la racine du fichier zip) dans le répertoire 'webapps' de Tomcat en le renommant cmdbuild.war
- de copier le répertoire CMDBuild-shark (trouvé dans le fichier zip dans le répertoire 'extras') dans le répertoire 'webapps' de Tomcat
- de copier les bibliothèques additionnelles pour la version choisie de Tomcat (situées dans le répertoire 'extras/tomcat-libs') dans le répertoire 'libs' de Tomcat

Une fois que vous avez fait ça, et seulement à ce moment, il va être nécessaire de lancer le serveur d'application Tomcat.

## Configuration de CMDBuild

La configuration de base est faite en utilisant quelques pages de configuration que CMDBuild affiche automatiquement à la première utilisation.

Pour accéder à ces pages de configuration, vous devez simplement vous connecter en utilisant votre navigateur à http://localhost:8080/cmdbuild (l'adresse peut varier selon la configuration de Tomcat).

1. Configuration de la langue

Si les opérations décrites précédemment ont été réalisées correctement, l'écran suivant va apparaître:

![](../technical_manual/ecran-conf-langue.png)

Depuis cet écran, vous pouvez configurer la langue du système.

Cochez "Afficher le choix de langue dans la fenêtre de connexion" pour qu'un menu de choix de la langue soit présenté dans l'interface de connexion de CMDBuild.

Une fois que vous avez fait votre choix, cliquez sur Suivant.

1. Configuration de la base de données

Dans la section "Connexion à la base de données", vou devez indiquer:

- l'hôte (nom d'hôte ou adresse IP) qui héberge la base de données PostgreSQL
- le port de la base de données PostgreSQL (le port par défaut est le port 5432)
- le nom d'utilisateur pour accéder à la base de données PostgreSQL (pour les activités d'administrateur de base de données)
- le mot de passe pour accéder à la base de données PostgreSQL (pour les activités d'adminitrateurs de base de données)

Dans la section base de données CMDBuild, vous devez indiquer:

- le type de base de données à configurer pour CMDBuild, parmi:
  - créer une bae de données vide
  - choisir une base de données existante compatible avec CMDBuild 1.0
  - créer une base de données avec des données de test
- le nom de la base de données

Cocher la case "Créer un utilisateur de base de données avec privilèges limités" va créer un nouvel utilisateur PostgreSQL avec tous les pivilèges d'administrateur de base de données, mais seulement pour la base de données qui sera utilisée / créée dans l'instance CMDBuild que vous êtes en train de configurer.

1. Configurer les autorisations d'accès

À partir du formulaire précédent, vous pouvez indiquer les identifiant et mot de passe que l'utilisateur administrateur (super utilisateur) utilisera pour accéder à CMDBuild (au module de gestion et au module d'administration).

En cliquant sur "Terminer", vous serez redirigé vers l'interface de connexion au système.
