# Configuration système

De façon à installer le système CMDBuild, vous pouvez utiliser soit un serveur, soit davantage. Sur ces serveurs, vous installez les composants du système:

- le serveur web
- les composants de calcul
- la base de données
- les web services
- l'archivage des documents

Dans les paragraphes suivants, nous présentons les pré-requis logiciels nécessaires au système CMDBuild et la façon d'installer et de configurer ses composants.

Quand on prépare la configuration du système, les problèmes de sécurité des informations doient être pris en compte.

L'activation d'une application web telle que CMDBuild requiert la disponibilité du matériel et des composants réseau avec les niveaux de sécurité appropriés. Ceci pour éviter des accès externes non souhaités (firewall, DMZ) et pour fournir une bonne disponibilité en ligne et des temps de réponses satisfaisants.

## Pré-requis matériels

Pour l'installation de CMDBuild, il est nécessaire de disposer d'un serveur physique ou virtuel doté des caractéristiques suivantes:

- processeur de génération récente
- 4 GB de RAM minimum; 6 GB si le même serveur héberge à la fois les instances de test et de production.
- une capacité de stockage disque minimum de 60 Go, à moins que vous n'ayez besoin de gérer d'importantes archives documentaires (alimentées par la gestion des pièces jointes)

Nous recommandant également que:

- le stockage sur disque soit en configuration RAID
- les données du système CMDBuild soient sauvegardées quotidiennement
- un onduleur soit utilisé de façon à éviter des pannes d'alimentation électrique soudaines

## Pré-requis logiciels

L'installation et l'utilisation de CMDBuild nécessite les composants logiciels suivants.

1. Système d'exploitation

Vous pouvez utiliser n'importe lequel des systèmes d'exploitation capable de faire fonctionne les logiciels listés ci-dessous (le système d'exploitation Linux est préférable car CMDBuild est testé plus intensivement sur lui).

2. Base de données

PostgreSQL 9.0 ou plus récent (PostgreSQL 9.3 est préférable).

Vous devriez vérifier que "plpgsql" est actif et que la base de données est configurée avec l'encodage UTF-8.

CMDBuild utilise la bibliothèque "tomcat-dbcp" pour se connecter à la base de données, cette bibliothèque est distribuée avec Tomcat mais n'est pas incluse dans certaines distributions Linux. Dans un tel cas, la bibliothèque peut être trouvée dans la distribution officielle de Tomcat ou dans le répertoire extras/tomcat-libs/5.5 dans le fichier zip de CMDBuild; la bibliothèque doit être placée dans /usr/share/tomcat6/lib.

CMDBuild ne prend en charge que la base de données PostgreSQL car c'est la seule qui implémente la fonctionnalité d'"héritage" des tables de manière "orientée objets". Cette fonctionnalité est utilisée pour gérer les sous-classes et l'historisation des fiches.

Si vou utilisez les extensions GIS de CMDBuild, vous devez également installer l'extension spatiale PostGIS en version 1.5.2 ou 2.0. Si vous créez une nouvelle base de données, vous devez suivre ces opérations (en utilisant par exemple l'outil "psql"):
 
``` 
  $ psql ... cmdbuild

  cmdbuild=# CREATE SCHEMA gis;

  cmdbuild=# SET SEARCH_PATH TO gis, public;

  cmdbuild=# \i ${POSTGIS_DIR}/postgis.sql

  cmdbuild=# \i ${POSTGIS_DIR}/spatial_ref_sys.sql

  cmdbuild=# \i ${POSTGIS_DIR}/legacy.sql (if you use PostGIS 2.0)

  cmdbuild=# ALTER DATABASE your_database_name_here SET search_path="$user", public, gis;
``` 

Si vous avez à restaurer une base de données existante (en utilisant par exemple l'outil "psql"):

``` 
  $ psql ... cmdbuild

  cmdbuild=# CREATE SCHEMA gis;

  cmdbuild=# SET SEARCH_PATH TO gis, public;

  cmdbuild=# \i ${POSTGIS_DIR}/postgis.sql

  cmdbuild=# \i ${POSTGIS_DIR}/legacy.sql (if you use PostGIS 2.0)

  cmdbuild=# ALTER DATABASE ${DB_NAME} SET search_path="$user", public, gis;

  cmdbuild=# DROP TABLE gis.geometry_columns;

  cmdbuild=# DROP TABLE gis.spatial_ref_sys;
``` 

Site web de référence: [http://www.postgresql.org](http://www.postgresql.org)

3. Containeur de servlets / serveur web

CMDBuild a besoin d'Apache Tomcat version 6.0 ou supérieure. Actuellement, du fait de problèmes provenant de l'utilisation de Shark, vous devez vérifier que Tomcat est exécuté avec l'utilisateur "root" et pas avec un utilisateur privilégié (par exemple "tomat6").

De façon à pouvoir gérer les caractères UTF-8 lors de l'utilisation des pièces jointes (voir l'nstallation du système DMS), modifiez le fichier de configuration 'server.xml' et indiquez l'attribut URIEncoding="UTF-8" pour l'élément "Connector" principal.

Vous pouvez utiliser le serveur web Apache 2.2 de façon à accéder à plusieurs instances de CMDBuild via des hôtes virtuels gérant différents domaines.

Site web de référence pour les deux: [hhtp://www.apache.org](http://www.apache.org)

4. Système de gestion des documents (DMS pour Document Management System) Alfresco

De façon à utiliser la fonction de gestion des documents joints aux fiches dans CMDBuild, vous devez installer le système de gestion de documentation Alfresco.

L'utilisation d'Alfresco (géré de manière encapsulée) est optionnelle et nécessite la version Community 3.4.

Site web de référence: [http://www.alfresco.com](http://www.alfresco.com)

5. Bibliothèques Java

Les bibliothèques Java sont requises par apache Tomcat.

CMDBuild nécessite le JDK 1.6 Sun / Oracle.

Site web de référence: [http://www.oracle.com](http://www.oracle.com)

6. Bibliothèques incluses dans la version

Le fichier CMDBuild téléchargeable à partir du site du projet contient déjà certaines bibliothèques dans le paquet d'installation, à savoir:

- la bibliothèque pour la connection JDBC à la base de données PostgreSQL
- les bibliothèques JasperReports version 3.7.1 pour la production des rapports (http://www.jasperforge.org)
- les bibliothèques du serveur TWS Together Workflow Server 4.4 qui implémentent le moteur de workflow utilisé par CMDBuild (http://www.together.at/prod/workflow/tws)
- les services web disponibles grâce au système de gestion documentaire Alfresco de façon à utiliser son dépôt (http://www.alfresco.com)
- les bibliothèques ExtJS version 4.1 pour la génération de l'interface utilisateur Ajax (http://extjs.com/)
- les bibliothèques Prefuse pour la génération des graphiques de relations en technologie Flash (http://prefuse.org)
- les composants serveur et client pour la publication des cartographies géoréférencées (http://geoserver.org et http://openlayers.org)

Pour la conception des rapports personnalisés, vous pouvez utiliser l'éditeur graphique iReport; il produit ses descripteurs dans un format compatible avec le moteur JasperReports (http://jasperforge.org/projects/ireport).

Pour la conception des workflows personnalisés, nous suggérons l'utilisation de l'éditeur de workflow TWE Together Editor version 4.4 (http://www.together.at/prod/workflow/twe). L'éditeur produit un fichier de sortie XPDL 2.0 compatibe avec le moteur du serveur de workflow Together 4.4.

Pour l'intégration de systèmes d'inventaire automatisé, nous suggérons d'utiliser OCS Inventory version 1.3.3 (http://www.ocsinventory-ng.org).

Certaines fonctionnalités de CMDBuild peuvent être intégrées en tant que portlts dans des systèmes compatibles avec la JSR Portal, parmi lesquels Liferay version 6.0.6 ou supérieure (http://liferay.com).

Tous les logiciels listés précédemment sont livrés sous licence Open source (hormis le système d'exploitation si vous choisissez de ne pas utiliser le système d'exploitation Linux).

## Pré-requis du poste client

CMDBuild est une application web, si bien que les deux modules sont disponibles en utilisant un navigateur web standard.

L'utilisateur du système doit s'assurer qu'il utilise une version récente de navigateur web (Mozilla Firefox, à partir de la version 3.6, Chrome à partir de la version 9, Microsoft Explorer versions 7 à 9).

L'architecture web assure une complète utilisabilité pour toute organisation IT qui opère depuis de multiples localisations (i.e. workflow collaboratif); tout client autorisé peut se connecter et interagir avec le système en utilisant un navigateur web standard.

