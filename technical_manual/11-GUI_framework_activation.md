# Activation du framework d'interface graphique

## Introduction

Le framework d'interface graphique (ou GUI Framework) rend disponiblle une interface simplifiée à destination des équipes non techniques.

Le framework d'interface apporte les fonctionnalités principales suivantes:

- il peut être activé dans des portails basés sur des technologies différentes étant donné qu'il est développé dans un environnement JavaScript / JQuery
- il permet une liberté (presque) totale d'agencement graphique, défini via un descripteur XML et avec la possibilité d'intervenir sur le CSS
- il permet une configuration rapide grâce aux fonctions prédéfinies (communication, logiques d'authentification, etc.) et aux solutions graphiques natives (formulaires, grilles, boutons de téléchargement et autres widgets)
- il interagit avec CMDBuild via les services web REST
- il est capable de récupérer des données depuis la base de données ou d'autres applications, permettant la gestion de solutions mixtes

## Configuration

Le framework définit des pages HTML en commençant par leur définition en XML.

De telles pages peuvent être insérées dans un fichier hTML, permettant au framework de d'insérer dans des portails existants des points d'accès aux données de CMDBuild.

La configurabilité du système peut être atteinte via la définition de CSS et de JavaScript personnalisé.

L'élément qui doit être inséré dans le portail html est un conteneur html (DIV, IFRAME, etc.) dans le format suivant:

```html
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script type="text/javascript" src="http://10.0.0.107:8080/cmdbuild-gui-framework/api/cmdbuildAP.js"></script>
  <script type="text/javascript">
  $( document ).ready(function(){
       $("#cmdbuilForm").cmdbuildAP({
         apiUrl : 'http://10.0.0.107:8080/cmdbuild/services/rest/',
         appRootUrl : 'http://10.0.0.107:8080/cmdbuild-gui-framework/api/',
         appConfigUrl : 'http://10.0.0.107:8080/cbap/cbap/config/bologna/',
         cqlUrl : 'http://10.0.0.107:8080/cbap/cbap/cql/',
         customjs : [
                'backend/GUIForm.js',
                'backend/Process.js'
         ],
         start: "home.xml",
         theme: [
                 "jquery-ui.theme-crt-toscana.min.css",
                 "custom.css"
            ],
          });
    });
  </script>
  <div id="cmdbuilForm"></div>
```

La configuration du style est conçue pour CSS, qui peut directement être téléchargée à partir de balises HTML. Ceci rend l'interface graphique flexible, permettant au programmeur de l'inclure dans le portail de façon à ce qu'il ne semble pas être un élément dépareillé.

Vous pouvez également définir les serveurs qui contiennent le système CMDBuild, les fichiers de configuration  des formulaires et le moteur Cql, le langage de requêtage natif de CMDBuild.

Dans la balise customjs, certains fichiers de configuration de l'interface graphique sont insérés.

Les balises téléchargent le moteur d'interface graphique en le configurant avec les fichiers mentionnés précédemment.

Les formulaires sont définis en XML via un langage semblable à HTML avec la possibilité de définir des formulaires de gestion des données directement liés aux méta données définies dans CMDBuild.

Toutes les bases de l'interface graphique, les commandes et les comportements peuvent être à nouveau configurés par le programmeur selon les critères du système.

Puisque les bibliothèques font référence ) JQuery, avec lequel le framework d'IHM a été développé, le programmeur peut nativement utiliser tous les plugins compatibles avec JQuery pour développer les fonctionnalités disponibles.

Un autre élément qui distingue le fonctionnement de l'IHM est le mécanisme de processus d'arrière plan, i.e. les classes JavaScript qui lient l'IHM avec les serveurs. De cette façon, l'application offre une plus grande liberté en fournissant la possibilité de définir des formats de données que le serveur attend et le nom du serveur sur lequel vous voulez vous connecter.

