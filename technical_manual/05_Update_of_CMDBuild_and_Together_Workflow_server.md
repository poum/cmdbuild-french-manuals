#Mise à jour de CMDBuild et de Together Workflow Server

##Mise à jour de CMDBuild

Pour mettre à jour CMDBuild, suivez les étapes suivantes:

1. éteignez tomcat et sauvegardez l'application et sa base de données
2. enregistrez les configurations présentes dans ${tomcat_home_cmdbuild}/webapps/${cmdbuild_instance}/WEB-INF/conf
3. si vous utilisez gis, vous devez enregistrer les icones gis existant dans ${tomcat_home_cmdbuild}/webapps/cmdbuild_instance}/upload/images/gis
4. supprimer le répertoire ${tomcat_home_cmduild}/webapps/${cmdbuild_instance}/
5. supprimer le contenu du répertoire work dans tomcat
6. placez le war fourni dans le répertoire webapps de tomcat et lancez tomcat
7. connectez-vous à cmdbuild et lancez la configuration en indiquant que la base de données existe déjà ainsi que son nom
8. 
