﻿#Rapports

CMDBuild inclut et utilise un moteur de génération de rapports très puissant (JasperReport) capable de créer des rapports dynamiques qui sont conçus en utilisant un éditeur graphique externe (iReport) puis importés en utilisant le module d'administration.

Le moteur de rapports gère ces formats:

- PDF
- CSV, utilisé pour importer des données dans des feuilles de calcul
- ODT, utilisé pour importer des données dans un traitement de texte

##Types de rapports et fonctionnalités

L'éditeur iReport possède les fonctionnalités suivantes:

- options de mise en forme du texte (polices, alignement du texte, espaces, couleurs, etc.)
- définition des éléments standards (en-têtes, pieds de page, en-têtes de colonnes, sommaires, etc.)
- regroupement des données
- évaluation des expressions
- champs calculés
- gestion avancée des sous-rapports
- prise en charge des codes barre
- éléments géométriques (lignes, rectangles)
- images et graphiques (camembert, histogrammes, Gantt, etc.)

En utilisant ces fonctionnalités, il est possible de créer et d'importer dans CMDBuild:

- des rapports sous forme de tableau avec des groupes et des totaux (options multi niveaux)
- des rapports avec des images et des logos
- des rapports avec des statistiques et des graphiques
- des étiquettes avec des codes barre

Des paramètres personnalisés des rapports peuvent être définis puis demandés au moment de la génération. Avant le rendu, CMDBuild va afficher une fenêtre surgissante avec les paramètres requis pour le rapport (i.e. le nom de l'ordinateur, la plage de dates, etc.)

Les paramètres listes de valeur et références seront automatiquement affichés sous forme de liste déroulante contenant des valeurs prédéfinies.

Les rapports importés dans CMDBuild peuvent être exécutés:

- en sélectionnant le rapport depuis le menu de navigation (s'il est disponible) dans la colonne en haut à gauche
- en accédant à la liste des rapports disponibles (pour l'utilisateur courant) en choisissant le menu "Rapports" dans la colonne de gauche.

Capture d'écran du choix et de la génération d'un rapport
