﻿#Fiches

En utilisant le module de gestion, il est possible d'accéder aux informations enregistrées dans le système, réaliser des recherches, mettre à jour des fiches, créer des relations, voir l'historique des fiches.

##Liste des fiches

Pour toutes les entrées du menu "Fiches" (ou pour les classes disponibles dans le menu de navigation), le module de gestion offre, dans le coin supérieur droit, la liste ("grille" ou "grid") des fiches de la classe choisie. Les détails d'une fiche sélectionnée particulière sont affichés dans le coin inférieur droit.

![](../user_manual/06-Card_list.png)

La gestion des fiches permet de:

- trier les fiches en cliquant sur le titre d'une colonne (le premier clic effectuera un tri ascendant, le second un tri descendant) ![](../user_manual/06_Card_list-2.png)
- ajouter des colonnes à la grille (les colonnes par défaut sont définies dans le module d'administration). La liste des colonnes d'attributs disponibles est affichée quand on clique sur la flèche de colonne (capture d'écran) ![](../user_manual/06_Card_list-3.png)
- utiliser les contrôles de la page pour avancer ou reculer d'une page, passer à la première / dernière page, aller à un numéro de page particulier ![](../user_manual/06_Card_list-4.png)
- recharger les données de la grille (rafraîchir) ![](../user_manual/06-Card_List-5.gif)
- accéder au système de recherche "avancé" ![](../user_manual/06_Card_list-6.png)
- définir un nouveau filtre de recherche en choisissant des valeurs d'attribut pour la fiche ou pour les fiches en relation (voir le prochain paragraphe). ![](../user_manual/06_Card_list-7.png)
- enregistrer le nouveau filtre de recherche
- modifier un filtre existant
- cloner un filtre existant
- supprimer un filtre existant ![](../user_manual/06_Card_list-8.png)
- réaliser une recherche rapide de fiches (recherche sur tous les attributs de la classe, y compris ceux qui ne sont pas affichés) ![](../user_manual/06_Card_list-9.png)
- imprimer (PDF ou CSV) les données (lignes et colonnes) actuellement affichées dans la grille ![](../user_manual/06_Card_list-10.png)
- modifier l'agencement de la page en cliquant et en tirant sur les bords du cadre ![](../user_manual/btn_full_height_detail.png) ![](../user_manual/btn_full_height_grid.png) ![](../user_manual/btn_standard_layout.png)
- basculer sur la vue cartographique pour afficher la position des fiches sur une carte / un plan. ![](../user_manual/06-Card_map.png)

##Définition d'un nouveau filtre de recherche avancé

Un filtre vous permet de chercher des fiches en utilisant de multiples critères de recherche:

- chercher sur tous les attributs de la classe actuelle (les conditions doivent être remplies simultanément, ce qui correspond à un "ET") à l'aide de différents opérateurs ainsi qu'appliquer plusieurs conditions sur un même attribut (dans ce cas, ce sera considéré comme un "OU")
- chercher sur tous les attributs des classes en relation avec la classe actuelle, soit en sélectionnant des lignes individuelles dans l'onglet "Liste" ou en appliquant des filtres sur les attributs de la classe en relation
- chercher sur toutes les pièces jointes (pièces jointes au format texte)

Dans le prochain exemple, nous extrayons tous les ordinateurs de marque "Epson" ou "Canon" et contenant la chaîne "Pentium" dans leur description.

La recherche peut être sauvegardée en tant que filtre puis réutilisée par le même utilisateur ou d'autres utilisateurs qui ont partagé ce filtre avec l'administrateur.

![](../user_manual/06_Search-1.png)

Dans l'exemple suivant, nous extrayons tous les ordinateurs affectés à "Robert Brown" ou "Michael Davis" (le domaine choisi est "Dépositaire actif").

![](../user_manual/06_Search-2.png)

Ici encore, vous pouvez sauvegarder et réutiliser la recherche.

L'exemple suivant extrait tous les ordinateurs de l'une des deux pièces du data center (le domaine est défini à "Pièce Actif").

![](../user_manual/06_Search-3.png)

Les deux types de sélection (onglets "Liste" et "Filtre") peuvent être utilisés simultanément, i.e. il est toujours possible de réduire la liste extraite en cliquant sur les cases à cocher de la ligne affichée.

Quand l'utilisateur valide le filtre de recherche, l'application cherche les fiches qui correspondent; les colonnes affichées sont celles définies dans le module d'administration.

##Onglet Fiche

En plus de la liste des fiches (commune à toutes les sous-pages de cette section du menu), l'onglet Fiche affiche la liste des attributs de la fiche sélectionnée.

Il est alors possible de réaliser les opérations suivantes:

- créer une nouvelle fiche en cliquant sur le bouton "Ajouter une fiche" ![](../user_manual/06-Add_card_btn.png)
- modifier une fiche existante en choisissant une ligne et en cliquant sur le bouton "Modifier la fiche" ![](../user_manual/06-Modify_card_btn.png)
- supprimer une fiche existante en choisissant la ligne et en cliquant sur le bouton "Supprimer la fiche" (suppression logique) ![](../user_manual/06-Delete_card_btn.png)
- cloner la fiche actuelle ![](../user_manual/06-Clone_card_btn.png)
- ouvrir le graphique des relations pour la fiche sélectionnée ![](../user_manual/06-Relation_card_btn.png)
- imprimer la fiche actuelle ![](../user_manual/06-Print_card_btn.png)

![](../user_manual/module_gestion.png)

L'agencement des fiches contenant beaucoup d'attributs peut être réarrangé en répartissant les informations dans des groupes de données (dans la capture d'écran, les groupes sont "General", "Administrative data" et "Technical data"); en utilisant ensuite les onglets (situés en haut ou en bas de la zone - cela dépend des réglages faits dans le module d'administration), il est possible de n'ouvrir que le groupe choisi d'attributs.

Les champs affichés lors d'une opération d'ajout ou de mise à jour sont configurés en utilisant le module d'administration. Il existe deux "modes de modification" que vous pouvez choisir dans le module d'administration: "Modifiable" pour les attributs modifiables et "Lecture seule" pour les attributs en lecture seule.

Selon le type d'attribut, le système utilise dans le formulaire:

- des champs de saisie standard
- des listes de choix (type de champ "Liste de valeurs" ou "Lookup")
- des champs de référence avec plusieurs options (voir l'exemple suivant):
  - choix d'une valeur directement dans la liste correspondante ou ouverture d'une fenêtre surgissante pour utiliser des fonctionnalités avancées (les onglets "Liste" et "Filtre" décrits dans le paragraphe précédent)
  - définition de valeurs pour les attributs des domaines (ceux définis dans le module d'administration en tant qu' "affichage de base")
- des champs dates avec un widget calendrier

Quand la liste des attributs dépasse la hauteur de la zone visible, une barre de défilement sur la droite vous aide à faire défiler toute la liste.

En utilisant les boutons du formulaire, vous pouvez confirmer ou annuler l'opération en cours. 

Toutes les erreurs sont surlignées et indiquées dans la langue de l'utilisateur.

Dans l'exemple suivant, il y a une fiche avec un champ HTML et un champ référence ("Fournisseur"); sur la droite, il y a quatre icones:

- la première ouvre la liste avec les noms des fournisseurs et vous permet d'en choisir un
- la deuxième vous permet de réinitialiser la valeur
- la troisième ouvre la fenêtre surgissante avancée (voir ci-dessous)
- la quatrième donne accès aux attributs de relation (voir ci-dessous)

![](../user_manual/06_Card_layout-2.png)

Ci-après, vous trouverez la fenêtre surgissante pour la valeur du champ référence "Dépositaire" avec des fonctions de filtre avancées:

![](../user_manual/06_Card_layout-3.png)

Ci-dessous, vous voyez un exemple de gestion des attributs définis sur le "domaine" pour lesquels le champ référence "Référence technique" est configuré (dans l'exemple, le rôle du référent de l'élément):

![](../user_manual/06_Card_layout-4.png)

###Widget

Des widgets - qui peuvent être configurés en utilisant le module d'administration - peuvent être utilisés pour exécuter des fonctions spécifiques utiles pour la fiche en cours.

Sur une fiche standard, les widgets suivants peuvent être configurés:

- créer un rapport: il permet d'imprimer un rapport
- calendrier: il affiche les échéances définies sur un calendrier
- arbre de navigation: il permet de choisir une ou plusieurs fiches de données via une interface utilisant un arbre de navigation pré-défini (sous-ensemble du graphe de domaine)
- démarrer un processus: il permet de démarrer le processus indiqué en utilisant une fenêtre surgissante (le processus utilisera ensuite les fonctions standards de CMDBuild)
- ping: il réalise un ping sur les machines indiquées
- créer ou modifier une fiche: il permet d'ajouter / de modifier une fiche de données dans une classe qui est différente de celle actuellement en cours

D'autres widgets peuvent être utilisés seulement dans les processus (voir le manuel dédié aux processus).

Ci-après, vous pouvez voir des exemples de fenêtres surgissantes que CMDBuild génère dans le cas des widgets "Ping" et "Calendrier".

####Ping

En utilisant le widget "Ping", vous pouvez exécuter une commande "ping" sur l'adresse IP correspondant à l'ordinateur objet de la fiche en cours et vérifier ainsi son accessibilité.

![](../user_manual/06-Ping.png)

####Calendrier

En utilisant le widget "Calendrier", vous pouvez contrôler les garanties en ouvrant le calendrier à la fin de la date de garantie de l'ordinateur objet de la fiche en cours.

![](../user_manual/06-Calendar.png)

####Créer un rapport

Il permet d'imprimer un rapport parmi ceux conçus avec IReport et importés dans CMDBuild. Si le rapport comporte certains paramètres, ils sont demandés au moment de la génération.

![](../user_manual/06-Report.png)

####Créer ou modifier une fiche

Il vous permet d'accéder à une fiche de données (en lecture seule ou en modification) qui vous intéresse au lieu de celle en cours. Dans l'exemple suivant, en partant d'une fiche d'un PC, vous pouvez accéder aux données de toutes les personnes à qui il a été affecté.

### Verrouillage d'une fiche pour modification

Si l'instance de CMDBuild est paramétrée avec la fonction de verrouillage actif et qu'un utilisateur essaie de modifier une fiche (bouton "Modifier la fiche"), le système réserve la fiche à cet utilisateur pendant la période définie dans les paramètres de configuration.

Si un autre utilisateur essaie de modifier la même fiche durant cette période, le message d'erreur suivant apparaît:

"L'utilisateur [nom de l'utilisateur] modifie cette fiche depuis [n] secondes".

La fiche sera automatiquement déverrouillée dès que le premier utilisateur quittera le mode édition en utilisant le bouton "Valider" ou "Annuler".

##Onglet détails

Pour certaines classes, il peut être utile de gérer des fiches en utilisant un fonctionnement "maître-esclave", où les fiches "esclaves" sont hiérarchiquement liées aux fiches "maîtres".

Cette option peut être activée en utilisant la case à cocher "Détails maîtres" du domaine correspondant et en définissant dans la classe détail (esclave) un champ référence qui pointe vers la classe principale (maître).

Dans l'exemple suivant, nous avons configuré un domaine de telle sorte que les éléments situés dans une pièce donnée soient connectés, via un domaine maître détail, à la pièce elle-même.

En conséquence, la fiche maître affiche une liste de sélection supplémentaire (sur la droite) appelée "Détails" et qui peut inclure le nom d'une ou plusieurs classes liées à la classe "Maître".

Quand on accède à la liste des fiches, une entrée appelée "élément" (la description du domaine) va apparaître et, quand on sélectionne cette entrée, on obtient, dans l'onglet "Détails", la liste des éléments situés dans la pièce courante.

On a alors la possibilité de réaliser les opérations suivantes:

- créer une nouvelle fiche en utilisant le bouton en haut de l'écran
- modifier la fiche
- supprimer la fiche (suppression logique)
- ouvrir le graphique des relations pour la fiche en cours
- afficher et autoriser la modification du champs "Notes"
- afficher et autoriser la gestion des pièces jointes à la fiche

##Onglet Notes

En utilisant l'onglet "Notes", vous pouvez parcourir et mettre à jour le champ "Notes", un champ contenant une description de la fiche sélectionnée.

Le champ "Notes" est géré en utilisant un éditeur HTML qui autorise diverses options de format (type, taille et couleur du texte, alignement du texte, listes).

##Onglet relations

L'onglet "Relations" vous permet de gérer les relations de la fiche en cours.

Il y est possible de réaliser les opérations suivantes:

- créer une nouvelle relation pour la fiche en cours
- ouvrir (accéder à) une fiche liée (via un double clic sur la ligne de la fiche)
- modifier la relation la reliant à une autre fiche
- supprimer la relation (suppression logique)
- ouvrir ou modifier une fiche en relation
- ouvrir les pièces jointes de la fiche liée
- ouvrir le graphique des relations pour voir les relations actives

Les relations sont groupées par domaine et on a la possibilité de replier ou de déplier chaque groupe.

Par défaut, les groupes contenant un nombre de relations inférieur au seuil défini dans le module d'administration sont automatiquement dépliés.

L'application affiche également les attributs définis pour le domaine (s'il y en a).

L'onglet relations est désactivé si aucun domaine ne concerne la classe en cours (directement ou via une superclasse).

###Créer une relation

Pour créer une nouvelle relation, vous pouvez cliquer sur le bouton "Ajouter un domaine" qui affiche la liste des domaines (type de relations) disponibles pour la classe en cours.

L'application ouvre une fenêtre surgissante avec une liste de fiches afin que vous puissiez choisir une fiche à connecter, soit directement, soit en appliquant certains filtres de l'onglet "Filtres". 

Selon la cardinalité du domaine, vous pourriez pouvoir choisir plusieurs éléments en utilisant des cases à cocher.

Dans l'exemple suivant, nous choisissons une fiche de la classe "Asset" puis nous choisissons le domaine "fourni par le fournisseur" et enfin nous choisissons l'entrée "Misco" dans la liste des fournisseurs disponibles.

En confirmant cette opération, nous aurons une relation entre l'actif et le fournisseur.

###Accéder à la fiche liée

En cliquant sur la flèche verte sur la droite ou en double cliquant sur la ligne, l'application va sur l'onglet "Relations" de la fiche liée.

###Modifier la relation

La fonction de modification utilise le même outil que celui décri pour la création d'une nouvelle fiche. Dans ce cas, cependant, le domaine conserve sa valeur d'origine.

###Supprimer une relation

La relation est supprimée, mais ce n'est qu'une suppression logique (de telle sorte que l'historique reste consultable).

###Afficher la fiche liée

L'application affiche dans une fenêtre surgissante la fiche liée avec tous ses attributs.

Par exemple, voici la visualisation de la fiche fournisseur liée au PC.

###Ouvrir le graphique des relations

Merci de vous référer au chapitre dédié au début de ce manuel.

##Onglet historique

Grâce à la fonctionnalité de "gestion des versions" incluses dans CMDBuild, l'onglet "historique" vous permet de naviguer dans l'historique de la fiche sélectionnée.

Les "versions" de la fiche sont affichées, à raison d'une par ligne, et vous pouvez déplier / masquer les détails de la fiche

Pour chaque "version" de la fiche, le système affiche:

- la date de début de cette version
- la date de fin de cette version
- l'utilisateur qui a effectué la modification
- un icone indiquant s'il s'agit d'une modification d'attribut ou de relation
- dans le premier cas, les attributs de la fiche pour cette "version" spécifique dont les modifications sont mises en évidence (en vert)
- dans le second cas, le code et la description des fiches liées à cet instant ("version")

##Onglet des pièces jointes

L'onglet "Pièces jointes" vous permet de voir les documents joints à la fiche en cours.

L'application utilise le DMS Alfresco comme système de stockage; cependant, les opérations standards (telles que joindre, ouvrir, supprimer, etc.) sont réalisées en utilisant l'interface standard de CMDBuild.

Quand on enregistre une nouvelle pièce jointe, l'utilisateur doit choisir un type de document à partir d'une liste déroulante; cette liste doit être définie en utilisant le module d'administration (l'administrateur doit créer une liste de valeurs - Lookup - spécifique à Alfresco et définir tous les paramètres dans la page de configuration d'Alfresco).

On peut alors réaliser les opérations suivantes:

- enregistrer une nouvelle pièce jointe
- télécharger une pièce jointe (ouvrir)
- modifier la description d'une pièce jointe
- supprimer une pièce jointe

L'intégration du DMS Alfresco nécessite la création d'une zone spécifique de CMDBuild dans le dépôt Alfresco, en créant un répertoire dédié au premier niveau, un sous-répertoire pour chaque classe et, à l'intérieur, un sous-répertoire pour chaque fiche.

La fonctionnalité des pièces jointes est totalement intégrée dans CMDBuild si bien que l'utilisation de l'interface d'Alfresco n'est pas nécessaire. Cependant, ceux qui voudraient utiliser l'interface d'Alfresco pourront trouver et accéder aux mêmes documents que ceux disponibles dans CMDBuild.

###Enregistrement de pièces jointes

L'enregistrement s'effectue en utilisant un formulaire d'enregistrement dont les champs sont:

- la catégorie de pièce jointe (en relation avec la liste de valeurs spécifique - voir les paramètres de configuration dans la section Alfresco du module d'administration)
- le fichier
- la description de la pièce jointe

###Affichage des pièces jointes

Les fichiers joints à la fiche en cours sont présentés de cette façon:

##Gestion des cartes

En cliquant sur le bouton "Carte", vous pouvez consulter ou modifier la position d'un objet sur la carte (ou sur le plan); voyons comment configurer la fonctionnalité GIS dans CMDBuild.

Les pré-requis sont:

- l'installation de PostGIS (merci de vous reporter à la documentation technique pour le numéro de version à choisir)
- l'activation des fonctions PostGIS pour votre base de données (les scripts SQL sont fournis dans la documentation de PostGIS)

La configuration de CMDBuild nécessite:

- configurer - dans le module d'administration - au moins un attribut géographique
- activer un service cartographique (Open Street Map ou Google Maps ou Yahoo! Maps) et / ou activer un serveur GIS geoserver pour les fichiers raster / vectoriels (plans, etc.)

Les fonctionnalités GIS disponibles dans CMDBuild, une fois que vous activez le mode cartographique (bouton "Carte" dans la grille) comprennent:

- le basculement en mode cartographique
- le retour en mode "Liste"
- l'augmentation / la diminution du facteur de zoom en utilisant la molette de la souris ou en utilisant les contrôles de la carte
- le "panoramique" en déplaçant la souris sur la carte
- le "contrôle des calques" pour activer un service cartographique pré-configuré et afficher un ou plusieurs calques pour la classe en cours
- l'arbre de navigation GIS pour accéder aux détails d'un élément et afficher ceux des éléments contenus à un niveau plus détaillé; vous pouvez également accéder à la fiche liée en cliquant sur la flèche verte à droite
- la liste des éléments de la classe en cours, pour afficher ceux des éléments contenus et accéder à la fiche liée en cliquant sur la flèche verte à droite
- "info", i.e. l'affichage de la liste des éléments définis à un certain endroit de la carte et qui peuvent être atteints en appuyant sur le bouton gauche plus de deux secondes; dans ce cas également, vous pouvez placer la fiche liée en cliquant sur la flèche verte à droite
- niveau de zoom et coordonnées actuelles du curseur de la souris

Les fonctionnalités GIS des fiches en cours comprennent:

- l'affichage des fiches sur la carte; la fiche sélectionnée apparaît avec un rond jaune tandis que les autres fiches apparaissent avec l'icone prédéfinie en attribut
- l'affichage de la barre d'outils de modification en entrant dans le mode de modification de la fiche
- la sélection de l'attribut géographique (si la classe en cours en possède plusieurs)
- l'ajout d'un nouvel élément géographique ou le déplacement d'un élément existant (l'ajout d'un nouvel élément supprime automatiquement l'élément précédemment défini, provoquant son remplacement)
- la suppression d'un élément géographique existant pour la fiche en cours

Grâce aux fonctionnalités décrites, vous pouvez obtenir une gestion avancée du géoréférencement des éléments.

Exemple d'éléments situés sur le plan:

##Afficheur BIM

Les modèles IFC visibles doivent être préalablement importés dans CMDBuild (ou dans son dépôt BIM dédié) via ses propres fonctions disponibles dans le module d'administration.

Les fichiers IFC comportent les entités et les relations du modèle représenté, en partant d'une classe racine (par exemple, bâtiment).

Une telle classe racine est signalée dans l'IHM de CMDBuild. Dans la liste des fiches, elle est matérialisée par un icone spécial qui lance l'afficheur.

Une fois démarré, l'afficheur 3D va afficher une fenêtre surgissante telle que celle qui suit.

Les fonctions disponibles dans l'afficheur 3D du BIM sont réparties dans 3 en-têtes du menu accordéon de gauche:

- divers contrôles décrits dans la table suivante
- liste des calques qui comprennent les éléments du modèle, avec la possibilité d'afficher / masquer chacun d'entre eux
- arbre hiérarchique de chaque élément du modèle, avec la possibilité d'afficher / masquer chacun d'entre eux et de déplacer dans la fiche de l'élément
- fermer la fenêtre de l'afficheur

Le détail des fonctions disponible dans le sous-menu "Contrôles" (premier sous-menu de la liste précédente) comprend:

- caméra
  - réinitialisation: restaure le point de contrôle initial
  - devant: affiche le modèle depuis le point de contrôle de devant
  - côté: affiche le modèle depuis le point de contrôle de côté
  - top: affiche le modèle depuis le point de contrôle du dessus
- mode
  - panoramique: il permet de bouger le modèle
  - rotation: il permet de faire tourner le modèle
- zoom: pour zoomer le modèle
- exposition: en sélectionnant un étage du bâtiment, vous pouvez l'extraire horizontalement du corps du bâtiment et voir efficacement ses fonctionnalités et ses éléments


