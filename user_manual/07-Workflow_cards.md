﻿# Fiches de processus

L'interface utilisateur des processus de CMDBuild fournit, pour tous les types de processus:

- la liste des instances de processus en cours / terminés (selon la valeur choisie dans la combo)
- les détails du processus, à la fois en lecture et en écriture
- des boutons pour ouvrir les sous-fiches de l'étape actuelle du processus pour exécuter des opérations dépendant du contexte (voir, créer, modifier des fiches de données, créer des relations, choisir des fiches liées, télécharger des pièces jointes, etc.)
- un panneau pour afficher des instructions relatives à l'étape actuelle du processus (bouton à droite)

Nous fournissons un manuel spécifique (Manuel des processus) dédié à la configuration et à l'utilisation du système de processus. Dans ce chapitre ne figurent que quelques informations générales et des captures d'écrans liées aux façons d'utiliser le module de gestion.

Voici un exemple de formulaire pour une étape qui fait partie d'un processus de gestion d'une demande de changement.

De la même façon que les fiches standards, les fiches de processus fournissent des attributs, des notes, des relations et un historique.

Il est possible d'interagir avec le processus de CMDBuild en utilisant des interfaces simplifiées qui peuvent plus facilement être utilisées par des utilisateurs non techniques. Elles peuvent être utilisées pour ouvrir un nouveau ticket auprès du support ou pour souscrire des services IT ou encore pour valider toutes les activités dans des processus de demande d'autorisation.

CMDBuild fournit une solution standard pour offrir certaines fonctionnalités sous la forme de Portlet (au standard JSR 168) dans le portail open source Liferay.

La portlet est capable d'accéder à CMDBuild via les services web SOAP en s'adaptant à la configuration de l'instance concernée (menu, autorisations, structure des fiches de données, flux de processus)

Cette solution est très avantageuse car elle peut s'adapter à l'instance CMDBuild liée sans développer de code pour la gestion de l'interaction entre les deux environnements.

Sa limite est son "auto adaptabilité" elle-même qui n'autorise pas tous les types de personnalisations.

Une solution plus récente et plus complète est fournie par l'interface appelée framework d'IHM de CMDBuild. Contrairement aux portlets Liferay, le framework d'IHM de CMDBuild peut être activé dans n'importe quel portail ou site web et permet une personnalisation complète de l'interface, à la fois fonctionnelle et graphique.

D'un autre côté, la configuration du framework d'IHM de CMDBuild requiert une personnalisation du code JavaScript qui est simplifiée si des templates et des bibliothèques sont disponibles.

Le framework d'IHM offre notamment les fonctionnalités suivantes:

- il peut être activé dans des portails basés sur différentes technologies étant donné qu'il est développé dans un environnement JavaScript / JQuery.
- il permet une liberté (presque) illimitée dans l'agencement graphique, défini via un descripteur XML tout en donnant la possibilité d'intervenir sur le CSS
- il fournit une configuration rapide grâce aux fonctions prédéfinies (communication, logiques d'authentification, etc.) et aux solutions graphiques natives (formulaires, grilles, boutons d'enregistrement de documents et autres widgets)
- il interagit avec CMDBuild via des services web REST

##Onglet Fiche

En choisissant la fonction de gestion du processus de demande de changement, le système affiche la demande de changement comme ouverte (ou dans l'état choisi dans la liste supérieure: ouverte, suspendue, terminée, annulée, tous).

Via le bouton "Démarrer une demande de modification", le service utilisateur peut enregistrer une nouvelle demande.

Avant de renseigner le formulaire, l'opérateur peut se référer aux instructions opératives en lien avec l'activité de l'utilisateur (définies dans le fichier XPDL qui décrit le flux du processus).

##Widgets

Les Widgets définis en utilisant le module d'administration peuvent être utilisés pour réaliser des fonctions spécifiques utiles pour l'activité en cours de l'utilisateur.

Ces widgets peuvent être classés comme suit;

- créer ou modifier une fiche: créer ou modifier une fiche de données dans une classe particulière indiquée
- gérer les relations: créer, modifier ou lier des fiches (créer des relations) avec la fiche principale
- lier une fiche: choisir des fiches dans une liste filtrée (le filtre est indiqué en utilisant le langage CQL), avec une extension géographique pour choisir des points et des polygones sur la carte 
- Webservice: choisir un enregistrement dans une liste en requêtant à l'aide d'un service web externe (par exemple des bases de données utilisées dans le processus)
- gestion des mails (avec des paramètres à remplacer, également obtenus à l'aide d'une requête CQL)
- pièce jointe: gestion des pièces jointes
- note: gestion des notes avec un éditeur WYSIWYG
- créer un rapport: générer des rapports (le rapport peut être attaché au processus et envoyé par mail)
- modifier la grille: générer une table avec plusieurs lignes (fiches de données) et colonnes (attributs des fiches de données) avec la possibilité d'ajouter ou de supprimer des lignes ou de les importer depuis un fichier CSV
- lancer un processus: démarrer un autre processus statique choisi (selon la définition du processus) ou un processus choisi dynamiquement (lors du démarrage du processus)
- arbre de navigation: il permet de choisir une ou plusieurs fiches de données via une interface basée sur un arbre de navigation prédéfini (sous-ensemble du graphique des relations)

##Exemple 1: lier une fiche

Le widget permet dans cet exemple de choisir une ou plusieurs fiches depuis une liste filtrée d'éléments (situés dans un bureau prédéfini).

Dans cet exemple, le processus donne accès à l'espace de travail en utilisant le bouton:

La zone de travail dans l'onglet Options possède l'interface suivante (le mode de sélection, unique ou multiple) peut être modifié:

##Exemple 2: gestion des mails

Dans cet exemple, le processus donne accès à l'espace de travail en utilisant le bouton:

La zone de travail dans l'onglet Option a l'interface suivante:

Il est possible de créer automatiquement des mails en utilisant un modèle prédéfini, complété manuellement ou d'insérer du texte libre dans un mail généré automatiquement.

Quand vous écrivez un mail, vous pouvez joindre des documents en les téléchargeant soit depuis votre propre système de fichiers, soit depuis ceux renseignés dans le système de documentation intégré à CMDBuild.

Les champs "To" ("Destinataire") et "CC" ("Copie") peuvent inclure une ou plusieurs adresses mails; dans le cas où il y en a plusieurs, le séparateur doit être ",".

##Exemple 3: gestion des pièces jointes

Dans cet exemple, le processus donne accès à l'espace de travail en utilisant le bouton:

La zone de travail de l'onglet Option possède l'interface suivante:

##Onglet relations

Concernant les fiches de processus, vous pouvez vous référer aux relations créées lors de la progression de l'activité.

Les relations peuvent être manuellement créées via l'interface utilisateur ou directement par le système via des automatismes utilisant les APIs configurées dans le processus.

##Onglet historique

En vous référant à l'onglet historique, vous pouvez connaître la séquence complète de chacune des activités de la séquence, leurs utilisateurs, les dates de début et de fin.

De telles informations peuvent être utilisées dans des rapports et des tableaux de bord pour calculer et fournir des indicateurs clefs de performance (KPI) de l'efficacité du système (contrôle des SLA).

