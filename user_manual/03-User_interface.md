﻿#Interface utilisateur

L'interface utilisateur utilise la technologie Ajax.

Cette solution, partie intégrante du paradigme Web 2.0, offre une application plus intuitive, améliore les interactions et obtient des réponses plus rapides du système.

##Critères généraux de conception

L'interface utilisateur comporte les éléments principaux suivants:

- menu côté gauche - en style _accordéon_ pour accéder aux menus des modules de gestion et d'administration
- la zone des donnée dans le coin haut droit, contenant:
    - un bouton d'ajout d'une nouvelle fiche
    - des options de tri et une liste de colonnes (limitées au module de gestion)
    - des fonctions de pagination, des filtres basiques et avancés, d'impression et d'export des grilles de données (seulement pour le module de gestion)
- une zone de données en bas, au milieu, contenant:
    - des onglets dédiés pour accéder aux sections spécifiques des fiches
    - des boutons pour gérer l'élément sélectionné dans la grille
    - la fiche complète - informations et libellés
    - liens vers les fiches associées avec la possibilité d'insérer, de modifier et de supprimer (suppression logique) des données
    - des boutons sur le côté droit correspondant aux widgets configurés pour la fiche

En haut de la page, dans l'en-tête:

- le logo CMDBuild (marque déposée de Tecnoteca Ltd) à gauche
- un panneau d'information central indiquant l'utilisateur actuel et son groupe; le panneau contient le lien vers le module d'administration (seulement pour les administrateurs système) et le module de gestion
- un bouton de déconnexion (panneau central)
- le nom de l'application sur la droite

Un pied de page contenant:

- URL du site officiel
- remerciements concernant l'application
- la note d'information du copyright

Les détails précédents sont présents dans la configuration standard mais peuvent différer dans le cas d'une installation spécifiques.

Nous présentons maintenant les détails du module de gestion puisque le module d'administration possède un manuel dédié.

Voici deux captures d'écran du module de gestion (tons bleus) et du module d'administration (tons gris).

###Module de gestion

![](../user_manual/module_gestion.png)

###Module d'administration

![](../user_manual/module_administration.png)

##Éléments de contrôle

###Redimensionner les zones principales

Les trois zones principales décrites précédemment peuvent être redimensionnées en cliquant et en tirant les bords du cadre.

Sur les pages qui ont un agencement standard (divisé en zones supérieure et inférieure), vous pouvez (seulement dans le module de gestion) agir plus rapidement en utilisant les boutons localisés dans le coin supérieur droit pour:

- étendre sur toute la hauteur la zone de détail de la fiche ![](../user_manual/btn_full_height_detail.png)
- étendre sur toute la hauteur la grille de données ![](../user_manual/btn_full_height_grid.png)
- restaurer l'agencement standard ![](../user_manual/btn_standard_layout.png)

###Menu "accordéon"

Le menu sur le côté gauche est en style accordéon et permettre à l'utilisateur pour ouvrir / fermer chaque élément du premier niveau d'un simple clic.

Ouvrir / fermer une entrée de menu fait apparaître les sous-éléments de l'entrée sélectionnée.

###Gestion de la grille

La gestion standard de la grille de données (module de gestion uniquement) permet de:

- trier selon une colonne donnée par un clic de souris
- afficher des colonnes supplémentaires dans la grille (parmi celles qui sont disponibles)

Dans certains cas, vous pouvez faire appel à des fonctionnalités spéciales associées à une ligne de la grille simplement en double-cliquant sur l'élément (ouvrir des relations, ouvrir un document dans l'onglet des pièces jointes, etc.)

Il y a également un bouton pour imprimer (en PDF ou CSV) les données (lignes et colonnes) qui sont actuellement affichées dans la grille.

Enfin, en utilisant le bouton "Carte", il est possible de basculer entre les modes texte et géographique.

###Actions rapides sur les éléments de la grille

Dans certains cas, vous pouvez faire appel à des fonctionnalités spéciales associées à une ligne de la grille simplement en double-cliquant sur l'élément (ouvrir des relations, ouvrir un document dans l'onglet des pièces jointes, etc.)

###Filtre de sélection

Le filtre de sélection, s'il est défini, est disponible en deux modes:

- recherche rapide sur tous les attributs des fiches (pas seulement ceux affichés dans la grille) ![](../user_manual/search.png)
- filtre de recherche avancée qui fournit un filtrage avancé (incluant l'enregistrement et la réutilisation du filtre) et qui sera décrit dans les cas d'utilisation individuels ![](../user_manual/advanced_filter.png)

###Aide interactive

Dans certaines fiches (en particulier dans la gestion des processus) il y a un bouton dans le coin inférieur droit qui affiche un panneau avec un recueil de certaines astuces.

##Renseignement de formulaires pour la modification de données

L'élaboration des fiches de données fait appel à différents champs:

- les types numérique et chaîne de caractères avec un contenu simple ![](../user_manual/champ_simple.png)
- du texte sur plusieurs lignes ![](../user_manual/multiligne_texte.png)
- type date avec calendrier interactif ![](../user_manual/champ_date.png)
- sélection unique dans une liste ![](../user_manual/champ_select.png)
- sélection avancée dans une liste (avec des options de filtrage) ![](../user_manual/champ_select_avance.png)
- texte mis en forme (à l'aide d'un éditeur de texte) ![](../user_manual/champ_editeur.png)

Vous pouvez ensuite utiliser les widgets (boutons) configurés pour la fiche.

Chaque opération d'insertion fournit des boutons de confirmation et d'annulation.

##Menu

Les deux modules de gestion et d'administration fonctionnent avec les mêmes objets, le premier pour définir les options de configuration (structures de données), le second pour gérer les informations enregistrées dans ces objets (fiches).

Les menus des deux applications, tous les deux en style "accordéon", comportent des entrées pour les classes sélectionnées, les processus, les rapports, mais diffèrent pour les fonctions spécifiques (i.e. tables des listes de valeurs, utilisateurs et groupes, menus, GIS) qui se trouvent dans le menu d'administration, tandis que les vues et les utilitaires se trouvent dans le module de gestion.

Le module de gestion fournit un menu complémentaire - le menu de navigation - qui comporte une liste d'éléments disponibles pour l'utilisateur connecté; il est possible de définir un menu de navigation personnalisé en utilisant le module d'administration.

##Interface utilisateur simplifiée

A l'aide des paramètres de configuration appropriés définis dans le module d'administration (gestion des groupes - configuration de l'interface utilisateur), vous pouvez définir une interface simplifiée pour les groupes d'utilisateurs qui le demandent.

En particulier, vous pouvez:

- masquer un à un les en-têtes du menu en accordéon situé sur le côté gauche de la page
- masquer un à un tous les onglets des fiches et des processus
- masquer le menu accordéon lui-même à l'ouverture de la page
- définir un mode d'affichage alternatif des listes de fiches et de processus ainsi que des formulaires d'ajout ou de modification des fiches et des processus

![](../user_manual/grid_nav_only.png)

![](../user_manual/fiche_nav_only.png)
