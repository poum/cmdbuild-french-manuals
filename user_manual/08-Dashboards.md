﻿#Tableaux de bord

CMDBuild offre la possibilité de configurer une ou plusieurs pages "tableau de bord"; chacune peut être dédiée à différentes sortes d'aspects à contrôler: état des éléments, performance des services utilisateurs, allocation des coûts, etc.

Chaque tableau de bord est constitué d'un certain nombre de graphiques de différents types : camembert, histogrammes, courbes, jauges.

Comme tous les composants CMDBuild, les tableaux de bord sont également configurés via le module d'administration et peuvent être
référencés dans le module de gestion, à la fois via le menu spécifique aux tableaux de bord du menu en accordéon et via le menu de navigation.

Pour chaque graphique du tableau de bord, vous pouvez réaliser les opérations suivantes;

- afficher ou masquer les contrôles pour modifier les potentiels paramètres fournis
- afficher la liste complète des valeurs pour la génération du graphique
- mettre à jour l'affichage des graphiques
- si nécessaire, modifier les paramètres d'analyse et mettre à jour l'affichage des graphiques

Dans l'exemple suivant, vous pouvez voir comment vous pouvez modifier la valorisation des paramètres fournis dans l'histogramme, dans ce cas la classe d'analyse.

Dans l'exemple, vous pouvez voir comment la liste des valeurs pour la génération de l'histogramme peut être demandée et affichée.

Les tableaux de bord sont exécutés via les composants propres de la bibliothèque Ext JS avec laquelle l'interface utilisateur de CMDBuild est implémentée et comprennent certains mécanismes d'interaction pour le référencement des données.

Dans l'exemple suivant, si vous placez la souris sur la zone contenant un camembert, la zone sera mise en valeur et la valeur numérique correspondante sera affichée.
