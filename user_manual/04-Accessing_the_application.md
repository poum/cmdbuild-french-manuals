﻿#Accès à l'application

Le module de gestion est utilisé par les opérateurs des services IT et peut être utilisé pour voir et mettre à jour les fiches, exécuter des processus, créer des rapports et réaliser d'autres opérations utilitaires.

Évidemment, les activités précédentes ne sont disponibles que si le système a été correctement configuré en utilisant le module d'administration.

##Pré-requis pour le poste de travail

CMDBuild est une application web, si bien que les deux modules sont disponibles en utilisant un navigateur web standard.

L'utilisateur doit disposer d'un navigateur web à jour (Firefox jusqu'à la version 34, Chrome jusqu'à la version 39, Microsoft Explorer 8 ou supérieur, jusqu'à la version 10).

L'architecture web assure une utilisabilité complète pour toute organisation IT qui opère dans plusieurs endroits (i.e. processus collaboratif); tout client digne de confiance peut se connecter et interagir avec le système en utilisant un navigateur web standard.

##Authentification

Vous devez vous connecter pour utiliser l'application.

Le formulaire de connexion nécessite un identifiant, un mot de passe et éventuellement le choix d'une langue (si l'application a été configurée pour gérer plusieurs langues).

![](../user_manual/04-login.jpg)

Une fois que l'utilisateur s'est connecté, le système démarre avec le module de gestion et seuls les utilisateurs qui ont le rôle requis peuvent basculer vers le module d'administration en cliquant sur le lien en haut de la page (visible en cliquant sur le panneau d'information situé au centre).

![](../user_manual/04-admin_link.jpg)
