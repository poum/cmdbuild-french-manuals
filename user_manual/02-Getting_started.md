﻿#Démarrage

##Philosophie de CMDBuild

Une CMDB est un système de stockage et de consultation qui gère les actifs d'information d'une compagnie.

C'est le dépôt central officiel qui fournit une vue cohérente des services technologiques.

C'est un système dynamique qui représente la situation courante et la connaissances des actifs des technologies de l'information et des entités liées: matériels (ordinateurs, périphériques, éléments réseau, équipements téléphoniques), logiciels (logiciels de base, d'environnement, applicatifs), documents (projets, contrats, manuels) et autres ressources, internes et externes.

C'est un système de surveillance pour les processus exécutés, décrits et gérés via les fonctions de workflow.

CMDBuild est une solution de CMDB robuste, personnalisable et extensible.

Fournir une solution évolutive signifie apporter un système ouvert et dynamique qui peut facilement être conçu, configuré et étendu par l'administrateur système en différentes phases, en terme de types d'objet à gérer, d'attributs et de relations.

Puisqu'il n'existe pas deux organisations qui travaillent exactement avec le même ensemble d'objets (actifs) et, pour chaque objet, avec les mêmes informations, nous avons décidé de placer, comme fonctionnalité centrale de CMDBuild,la flexibilité système en développant des fonctionnalités pour configurer la totalité du système: modèles de données, processus, rapports, connecteurs de systèmes externes, etc.

Plus précisément, les fonctionnalités de gestion disponibles dans CMDBuild vous permettent de:

- afficher les fiches de données d'une classe ou d'une vue selon les droits fournis (sur la classe entière ou sur un sous-ensemble de lignes et de colonnes)
- effectuer des recherches via des mécanismes de filtre disponible dans le système avec la possibilité d'enregistrer un filtre de recherche pour le réutiliser ultérieurement
- utiliser des widgets configurés pour exécuter des fonctions particulières aux fiches
- consulter les relations entre les fiches à l'aide d'un outil graphique ou simplement en navigant dans le système
- ajouter ou mettre à jour des fiches de données, y compris des champs de commentaire dotés d'un éditeur HTML
- créer ou modifier des relations entre des fiches, y compris des attributs de relation
- enregistrer des pièces jointes dans une fiche
- géoréférencer des objets, des cartes ou des plans 2D via des fonctions GIS dédiées
- voir la liste des processus en attente et terminés (workflow)
- consulter les tableaux de bord définis dans le système
- exécuter des rapports prédéfinis et personnalisés (conçus avec IReport)
- changer de mot de passe
- importer ou exporter des données au format CSV
- modifier plusieurs fiches en même temps
- accéder au module d'administration (rôle administrateur)

Voici un schéma qui explique les termes et les concepts introduits précédemment en relation à la configuration du modèle de données.

![](../user_manual/diagramme.png)

##Critères généraux 

L'utilisation du module de gestion suppose que l'administrateur système a défini, via le module d'administration, un modèle de donnée initial pour le système.

Il n'est pas nécessaire que le modèle initial décrive toutes les propriétés gérées par l'organisation, il est en effet important d'adopter une politique de développement graduel du système en phases successives en terme de fiches et de relations entre elles.

Nous vous recommandons de débuter en gérant un ensemble d'objets et de relations petit mais complet puis d'étendre le système une fois que la connaissance et les exigences sont devenues plus claires.

CMDBuild attend que les opérateurs utilisent le module de gestion pour gérer les fiches selon les règles définies par l'administrateur système via le module d'administration.

Nous vous recommandons d'utiliser CMDBuild:

- en respectant les procédures et les droits
- en enregistrant de manière appropriée les fiches de données: une base de données avec des informations manquantes est inutile
- en enregistrant les nouvelles informations dès qu'elles sont disponibles et en mettant à jour les informations existantes: un système avec des informations obsolètes n'est utile à personne.

##Comment utiliser CMDBuild

Les objectifs principaux de CMDBuild sont de:

- obtenir des informations à jour sur chacun des actifs et des relations avec les autres actifs et autres éléments du système
- trouver l'état de chaque actif et de chaque relation à tout moment précédent
- mettre à jour les informations enregistrées dans le système - fiches et documents - soit un par un ou avec les fonctions avancés pour des modifications multiples
- aider l'opérateur en définissant des processus et en implémentant des assistants
- consulter des tableaux de bord pour contrôler les paramètres de gestion de base
- créer des rapports utiles pour l'analyse des opérations quotidiennes ou des tendances historiques et des statistiques
- mener des activités automatiques (e-mails de notification, lancement de workflow, implémentation de scripts qui font suite à des opérations telles que des réceptions d'emails, des démarrages de workflow, des contrôles sur des événements synchrones et asynchrones) configurés via le gestionnaire de tâches disponible dans le module d'administration.

Les fonctionnalités les plus largement utilisées, regroupées dans la section 'fiches de données', sont:

- consultation des fiches de données d'une classe ou d'une vue, selon les droits disponibles (sur la classe complète ou sur des sous-ensembles de lignes et de colonnes)
- recherche de fiches en indiquant des filtres à la fois les fiches de données de la classe courante et de celles des classes en relation, avec la possibilité de les enregistrer et de les réutiliser
- aperçu des données de la fiche, des relations des fiches et de l'historique
- exécuter des fonctions spécifiques (widget) configurés pour cette fiche
- impression d'une fiche particulière ou de plusieurs
- navigation graphique des relations entre fiches
- mise à jour des données d'une fiche
- définition de relations incluant les attributs des relations
- téléchargement des pièces jointes des fiches
- géoréférencement des fiches (GIS)
- géoréférencement de modèles 3D

L'onglet "Processus" possède un moteur de workflow puissant pour:

- avoir un aperçu du processus
- modifier les processus en attente

La section "Tableaux de bord" permet de:

- afficher la liste des tableaux de bord configurés dans le système
- afficher les tableaux trouvé dans chaque tableau de bord avec la possibilité de consulter les nombres générés

L'onglet "Rapport" fournit également un moteur d'impression puissant pour:

- les rapports personnalisés (iReport)
- les exports de données au format CSV ou en tant que requête SQL

L'onglet "Utilitaires" comprend des assistants pour gérer des opérations telles que:

- modifier tous les attributs d'une classe
- importer des données à partir de fichiers CSV externes
- exporter des données en CSV
- modification du mot de passe

