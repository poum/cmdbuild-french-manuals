﻿#Fonctions utilitaires

Certaines des fonctions utilitaires sont:

- modification du mot de passe
- modification des données des fiches en masse
- export CSV
- import CSV

##Modification du mot de passe

Cette opération permet à l'utilisateur de définir un nouveau mot de passe; l'utilisateur doit saisir l'ancien mot de passe pour confirmer la modification.

L'utilisateur doit saisir le nouveau mot de passe deux fois (pour éviter les erreurs de saisie).

##Modification des données des fiches en masse

Cette fonctionnalité vous permet de modifier la valeur d'un ou plusieurs attributs d'un ensemble choisi de fiches.

D'abord, vous devez choisir une classe parmi celles disponibles dans le menu en haut à gauche.

Ensuite, vous pouvez créer des filtres pour choisir les fiches que vous voulez modifier en utilisant à la fois le filtre rapide ou le filtre avancé (décrit dans les pages précédentes).

Ensuite, vous pouvez choisir manuellement les fiches particulières sur lesquelles appliquer la modification, ou vous pouvez choisir toutes les fiches dans la liste en cochant la case en haut de la dernière colonne.

Une fois les fiches sélectionnées, vous devez choisir (case à cocher) les attributs que vous voulez modifier puis insérer la nouvelle valeur.

Enfin, vous devez confirmer l'opération en cliquant sur le bouton "Confirmer".

Avant d'exécuter l'opération de modification, le système affiche le nombre de fiches qui seront modifiées et demande que l'opération soit confirmée ou annulée.

Dans l'exemple suivant, nous modifions toutes les fiches de la marque "HP" en fixant la "date de réception" au 06/09/2011.

##Export de données CSV

Cette fonctionnalité vous permet de créer un fichier CSV (comma separated value i.e. valeurs séparés par des virgules) qui contient les données des classes en utilisant un séparateur de champ choisi (les séparateurs possibles sont ",", ";" et "|").

Le navigateur vous demandera d'enregistrer ou d'ouvrir le fichier en utilisant l'application associée à l'extension de fichier "csv".

Capture d'écran de l'interface utilisateur fournie par le système.

##Import de données CSV

L'import de fichier CSV (Comma Separated Value i.e. valeurs séparées par des virgules) est une fonctionnalité très utile qui vous permet de charger des valeurs dans CMDBuild en utilisant un assistant; c'est un moyen rapide de charger plein de fiches dans l'application en seulement quelques étapes.

Cette opération est réalisée via l'interface utilisateur, comme montré ci-dessous, et sans interroger la base de données car nous voulons que l'application contrôle et valide les données avant leur insertion.

Il est important de noter que l'opération d'import ajoute toujours des données, donc les fiches importées sont toujours ajoutées aux fiches existantes.

L'opération d'import comporte les étapes suivantes:

###Étape 1

La première étape comprend:

- le choix de la classe (l'opération d'import ne fonctionne que sur une seule classe)
- choix du fichier CSV
- choix du séparateur de champs: "," (virgule) ou ";" (point-virgule) ou "|" ("pipe" ou tube)

En confirmant l'opération avec le bouton "Charger", l'application importera les données du fichier indiqué, affichant les fiches importées dans la grille standard.

Capture d'écran de l'interface utilisateur fournie par le système.

La première ligne du fichier CSV contiendra les en-têtes des colonnes.

Le système ne fera correspondre que les colonnes du fichiers CSV avec les noms d'attributs (note: le nom, PAS la description) définis en utilisant le module d'administration (i.e. le nom de colonne dans la table en base de données).

La fonction d'import proposera le contenu du fichier CSV comme table sur la page de CMDBuild, en écrivant en rouge les éléments pouvant être incorrectes (noms de colonnes non identifiées, colonnes obligatoires absentes, types de données invalides ou ne correspondant pas à ceux de la base de données, etc.).

Les attributs listes de valeurs doivent correspondre (majuscule/minuscule, espaces, etc.) à la description dans la liste des valeurs comme indiqué, au moment de leur définition, dans le module d'administration.

Les attributs de référence doivent correspondre au champ "Code" (note: "Code", PAS "Libellé") de la fiche liée (majuscules/minuscules, espaces, etc.).

Les dates doivent être écrites en utilisant le format jj/mm/aa.

Dans le fichier CSV, seules les lignes ayant un numéro de colonne fourni dans la ligne d'en-tête sont considérés valides: vous devez faire attention à ne pas insérer de lignes ou de colonnes supplémentaires dans le fichier CSV.

L'exemple montre certaines erreurs sur les champs de référence.

##Étape 2

L'étape suivante est la correction des éventuelles erreurs mises en évidence, ce qui peut être fait en double-cliquant sur le champ et en modifiant la valeur (comme montré dans la capture d'écran).

Alternativement, il est possible de modifier le fichier CSV et de retenter l'opération d'import.

Une fois résolues les erreurs, vous pouvez appuyer sur le bouton "Mise à jour" pour relancer la validation des données.

Enfin, s'il n'y a pas d'erreurs, vous terminerez l'opération en remplissant les fiches en base de données.

Merci de vous rappeler que l'opération d'import de données ajoute toujours des données, donc les nouvelles fiches sont ajoutées aux existantes (éventuellement, une erreur est signalée dans le cas où une clef est dupliquée).

Ci-dessous, vous pouvez trouver une capture d'écran de l'interface utilisateur fournie par le système pour le chargement final.
