﻿# Graphiques des relations

Puisque cette fonctionnalité est disponible dans de nombreuses situations, nous décrivons maintenant les détails du graphique des relations.

L'idée est de fournir une représentation graphique interactive des relations enregistrées dans CMDBuild.

Le système ouvre une fenêtre surgissante affichant la fiche principale et ses relations avec des boîtes de colorées et des lignes de connexion entre les éléments; la distance entre la fiche principale et les fiches liées peut être définie dans le module d'administration en tant que paramètre de configuration.

La fiche actuelle est affichée en jaune foncé alors que les fiches liées sont affichées dans un jaune plus clair. Plus grande est la distance (en terme de niveaux de relation), plus claire est la couleur.


![](../user_manual/graphe_relation.png)

Il est possible d'exécuter les opérations suivantes:

- cliquer sur une fiche (i.e. une des boîtes affichées) pour voir dans le coin supérieur droit tous les détails de cette fiche
- double cliquer sur une fiche pour réouvrir le graphe en utilisant la fiche sélectionnée comme fiche principale
- déplacer la souris sur une ligne de relation pour lire la description du type de connexion
- augmenter le nombre de niveaux de relations affichés dans le graphe (le niveau maximum est défini dans le module d'administration)
- modifier le type de graphe: "Arbre radial" ("Radial Tree", valeur par défaut) ou "Cercle" (capture d'écran suivante)
- cliquer sur les cases à cocher en bas à droite pour séparer les fiches affichées en forme groupée. Le système regroupe les fiches ayant une cardinalité supérieure à celle du seuil défini dans le module d'administration et permet à l'utilisateur de les séparer si la cardinalité est inférieure au paramètre prédéfini (toujours défini dans le module d'administration)

![](../user_manual/graphe_cercle.png)

**Note :**
Le graphe des relations est le seul outil provenant des versions 1.x qui n'a pas subi de rénovation. C'est pourquoi il possède certaines limitations qui seront résolues dans une prochaine version;

**NdT :** Quand la langue choisie est le français, ce graphique s'affiche en italien (bug signalé)

Cette fonctionnalité peut être désactivée dans le module d'administration.
