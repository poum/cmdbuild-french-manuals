﻿#Standards d'interopérabilité

##Architecture orientée services (Service-Oriented Architecture ou SOA)

De façon à rendre différentes applications interopérables, elles doivent
être créées sous forme de composants qui coopèrent avec l'implémentation
de services et ces services doivent être mis en place via des interfaces
de haut niveau définies à l'aide de protocoles standards.

CMDBuild est conçu avec une architecture orientée services (SOA):

- découplage des différents niveaux de logique (voir le schéma)

![Schéma SOA](../webservices_manual/soa.png)

- implémentation et configuration dans chaque interface des spécifications
externes comme mode unique d'accès aux données et méthodes concernées
- utilisation des interfaces à la fois pour les accès interactifs au client
web et pour les accès par programmation pour les applications externes

D'un point de vue technique, nous avons choisi d'utiliser les technologies
de web services suivantes:

- protocole SOAP
- protocole REST

Via les webservices, et les permissions de la politique de sécurité, CMDBuild fournit les données
saisies dans CMDBuild et ses méthodes de gestion pour permettre l'utilisation depuis d'autres applications
impliquées avec ces informations, à la fois pour la gestion technique et pour l'administration.
