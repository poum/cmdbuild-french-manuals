﻿#Services Web et protocole SOAP

Un service web est une interface qui décrit une série de méthodes disponibles via le réseau et 
fonctionne en utilisant des messages XML.

Grâce aux services web, une application permet à d'autres applications d'interagir avec ses méthodes.
![SOAP](../webservices_manual/soap.png)

SOAP est un protocole standard reposant sur XML pour accéder à un service web.

Les spécifications de SOAP sont normalisées par le W3C.
                                                                  
La solution des services web offre d'importants avantages architecturaux:

- elle permet de réduire la dépendance entre les applications client et serveur ("couplage faible")
- elle offre un système interopérable indépendant de la plate-forme et des technologies
- elle gère l'interopérabilité du web, puisqu'elle est basé sur le protocole SOAP qui utilise habituellement HTTP comme protocole de base (que tous les firewalls autorisent)
- elle est basée sur des descripteurs XML (WSDL)

##Exemples d'utilisation

Le mécanisme d'interopérabilité de l'application fourni par CMDBuild via le service web SOAP
peut être utilisé pour initier le dialogue avec chacun des autres systèmes d'information
pré-existant dans la même organisation et prenant en charge ce protocole standard:

Voici quelques exemples d'utilisation:

- activation dans des portails intranets non conformes à la JSR 168 de mécanismes d'interactions simples avec
CMDBuild pour des non techniciens (impression des rapports, lancement de processus ou développement, etc.)
- synchronisation avec d'autres outils de CMDB
- intégration avec des applications de gestion qui nécessitent de récupérer depuis CMDBuild 
des inventaires à jour d'éléments déterminés ou qui doivent prendre des données d'administration d'un élément donné.
- intégration avec des outils techniques de surveillance qui ont besoin de connaître des informations sur des éléments soumis à leur contrôle

##Services fournis

###Catégories de méthodes

Le service web CMDBuild fournit des méthodes pour l'exécution externe de fonctions de base gérées dans le système, dédiées en particulier à gérer et afficher des fiches et exécuter des processus.

En particulier, les catégories de méthodes disponibles sont les suivantes :

- gestion des fiches: création, modification, suppression, recherche, historisation
- gestion des listes utilisées pour lier des champs d'information à des groupes de valeurs prédéfinies: 
création, modification, suppression, recherche sur les listes et leurs valeurs
- gestion des relations entre les fiches: création, modification, suppression, recherche, historisation
- gestion des pièces jointes des fiches (renseignés dans le dépôt du système de documentation Alfresco
utilisé par CMDBuild): téléchargement, enregistrement, modification, suppression
- gestion des processus configurés dans le système (et interprété par le moteur de processus Enhydra Shark intégré dans CMDBuild): démarrage, enregistrement de données, passage à l'étape suivante

Toutes les méthodes fournies dans le service web peuvent être utilisées via l'authentification dans le système CMDBuild.

L'authentification est réalisée selon la spécification 1 du profil de jeton de nom d'utilisateur WSS avec mot de passe haché.

Vous trouverez la description détaillée de chaque méthode disponible dans le chapitre suivant.

##Structures de données support

Les méthodes de services web exposées par CMDBuild utilisent des structures de données support correspondant aux
typologies d'objet suivantes:

- fiches (fiches de données)
- attributs (chacun des attributs personnalisés de la fiche)
- liste de valeurs (valeurs dans la liste prédéfinie utilisée pour optimiser un attribut d'information)
- requête (requête de filtre pour choisir la liste de fiches)
- opérateur de filtre (concaténation des conditions de filtrage)
- tri (organisation entre les fiches récupérées par la requête de filtrage)
- relation (corrélation entre les fiches)
- pièces jointes (documents encapsulés dans une fiche)

Vous pouvez trouver la description détaillées de chacun des objets employés dans le chapitre suivant.

###Système d'authentification

De façon à disposer de tous les services offerts par les web services de CMDBuild, l'accès d'un utilisateur au système doit être authentifié.

Ceci est possible grâce à l'authentification reposant sur UsernameToken.

CMDBuild utilise Axis2 pour fournir ses services à l'extérieur etle module Apache Rampart pour gérer
l'authentification des utilisateurs via un service web, en utilisant le standard WSS Username Token profile 1.0 [1]
avec mot de passe haché.

1 Le document de référence peut être trouvé [ici][http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0.pdf]

Ce standard fournit la distribution des éléments d'authentiication, à savoir un mot de passe, un nom d'utilisateur et un hachage. Il
est calculé à l'aide de l'algorithme défini dans la spécification, dans l'en-tête du message SOAP.

Une fois le message reçu, le module Rampart vérifie si CMDBuild connaît un utilisateur qui correspond
aux éléments d'authentification fournis 

Quand c'est le cas et que l'utilisateur est autorisé à accéder au service demandé, le service web renvoie le résultat. Dans le cas contraire, il y a un message d'erreur.

###Système de gestion des erreurs

La gestion des erreurs dans le système CMDBuild fournit l'utilisation de codes d'erreur personnalisés définis expressément.

En utilisant les services web, en plus des erreurs d'Axis (et du module Ramport pour l'authentification), 
des erreurs CMDBuild personnalisées peuvent être renvoyées.

Dans le tableau suivant, vous trouverez la liste des codes d'erreur personnalisés de CMDBuild; ils peuvent
éventuellement être retournés par le service web avec leur signification.

----------------------------------------------------------------------------------------------
 Code                        Signification
--------------------------- ------------------------------------------------------------------
NOTFOUND_ERROR               Élement non trouvé
AUTH_MULTIPLE_GROUPS         L'utilisateur est connecté avec plusieurs groupes
AUTH_UNKNOWN_GROUP           Groupe inconnu 
AUTH_NOT_AUTHORIZED          Les autorisations ne sont pas suffisantes pour réaliser l'opération
ORM_GENERIC_ERROR            Une erreur s'est produite lors de la lecture/enregistrement des données
ORM_DUPLICATE_TABLE          Il existe déjà une classe ayant ce nom
ORM_CAST_ERROR               Erreur dans la conversion de type
ORM_UNIQUE_VIOLATION         Contrainte non nulle violée
ORM_CONTAINS_DATA            Vous ne pouvez pas supprimer des classes ou des attributs de tables ou de domaines contenant des données
ORM_TYPE_ERROR               Type non correspondant
ORM_ERROR_GETTING_PK         La clef principale ne peut pas être déterminée
ORM_ERROR_LOOKUP_CREATION    La liste de valeurs ne peut pas être créée
ORM_ERROR_LOOKUP_MODIFY      La liste de valeurs ne peut pas être modifiée
ORM_ERROR_LOOKUP_DELETE      La liste de valeurs ne peut pas être supprimée
ORM_ERROR_RELATION_CREATE    La relation ne peut pas être créée
ORM_ERROR_RELATION_MODIFY    La relation ne peut pas être modifiée
ORM_CHANGE_LOOKUPTYPE_ERROR  Le type liste de valeurs ne peut pas être modifié
ORM_READ_ONLY_TABLE          Table en lecture seule
ORM_READ_ONLY_RELATION       Relation en lecture seule
ORM_DUPLICATE_ATTRIBUTE      Il existe déjà un attribut de ce nom
ORM_DOMAIN_HAS_REFERENCE     Les domaines avec des attributs référence ne peuvent être supprimé
ORM_FILTER_CONFLICT          Conflit en définissant le filtre 
ORM_AMBIGUOUS_DIRECTION      Le sens de la relation ne peut pas être déterminé automatiquement
---------------------------------------------------------------------------------------------

##Description des structures de données support 

###Objet "Fiche"

Il représente un type général des fiches configurés dans le système.

----------------------------------------------------------------------------------------------------------------
Nom             Type                   Obligatoire   Valeur par défaut   Description
-------------- ---------------------- ------------- ------------------- ---------------------------------------------------
className       chaîne de caractères                                     Nom de la classe qui contient la fiche. Il correspond au nom de la table dans la base de données
id              entier                                                   Identification de la fiche, automatiquement affecté par la base de données
attributeList   Attribute[]                                              Tableau d'objets "Attributs" contenant les valeurs des atttributs personnalisés complémentaires de la classe. Ils correspondent aux attributs complémentaires définis dans le module d'administration de CMDBuild et disponibles dans la gestion de la fiche. La liste inclut également le ClassId (pas le ClassName).
beginDate       date                                                     Il donne la date de création de la version actuelle de la fiche (la date de création ou de dernière modification. C'est un objet date dans le format de définition TimeZone du standard XML Schema (AAAA-MM-JJThh:mm:ssZ).
user            chaîne de caractères                                     Nom d'utilisateur de la personne qui a réalisé la dernière opération sur la fiche.
----------------------------------------------------------------------------------------------------------------

###Objet "Attribut"

Il représente un unique attribut "personnalisé" (qui est facultatif, comparativement aux attributs "Code" et "Description") appartenant à une catégorie de fiches configurée dans le système.

Name            Type          Mandatory    Default      Description
name            string                                  Attribute name, as created with the CMDBuild
                                                        Administration Module.
                                                        It corresponds to the table name in the
                                                        database table.
value           string                                  It corresponds to the attribute value.





code              string                                   It is developed only for "Reference" attributes
                                                           with the id of the card.



###Objet "Liste de valeurs"

Il représente la valeur d'une liste ayant des valeurs prédéfinies (liste de valeyrs) utilisée pour le développement d'un attribut "Liste de valeurs".

Name              Type             Mandatory    Default    Description
id                integer                                  Lookup identification, it is automatically
                                                           assigned by the database
type              string                                   Name of the Lookup list which includes the
                                                           current heading.
description       string                                   Description of the Lookup heading (one single
                                                           heading of a Lookup list).
code              string                                   Code of the Lookup heading (one single
                                                           heading of a Lookup list).
parent            Lookup                                   Lookup object corresponding to the parent
                                                           heading of the current heading.
parentid          integer                                  Identification of the parent Lookup in the
                                                           current heading (if applicable)
position          integer                                  Location of the Lookup heading in the related
                                                           Lookup list
notes             string                                   Notes connected with the Lookup heading



###Objet "Requête"

Il représente un filtre sur les valeurs à chercher. Le filtre peut être unique (filtre) ou un ensemble de conditions avec un opérateur de comparaison classique (opérateur de filtre).

Name              Type             Mandatory    Default    Description
filter            Filter                                   Atomic filter condition
     or
filterOperator    FilterOperator                           Concatenation of filter conditions


###Objet "Filtre"

Il représente une condition de filtre atomique pour choisir une liste de fiches.

Name              Type             Mandatory    Default    Description
name              string                                   Attribute which the filter condition is applied to.
value             string                                   Value for the comparison with the attribute's
                                                           content.


operator          string                                Comparison operator (values such as
                                                        EQUALS, LIKE are admitted).
###Objet "Opérateur de filtre"

Il représente la concaténation de conditions atomiques de filtre connectées avec un opérateur.

Name              Type           Mandatory   Default    Description
operator          string                                Concatenation operator to join filter conditions
                                                        (values such as AND, OR are admitted)
subquery          Filter                                “Filter” object containing the filter condition
                                                        applied to the query.

**Exemple d'opération à réaliser :** création d'un filtre "Fournisseur = Quasartek s.r.l. OR Fournisseur = IBM Italia s.p.a.”

####Filtre 1:

name: Supplier
value: Quasartek s.r.l.
operator: EQUALS

####Filtre 2:

name: Supplier
value: IBM Italia s.p.a.
operator: EQUALS

####Opérateur de filtre:
subquery: [Filter1, Filter2]
operator: OR

####Requête:
filterOperator: FilterOperator

###Objet "Tri"

It represents the ordering standard among the cards drawn from the filter query.
Name              Type           Mandatory   Default    Description
columnName        string                                Attribute which the ordering is performed on.
type              string                                Ordering typology applied (only ASC and
                                                        DESC values are admitted).

###Objet "Relation"

Il représente une corrélation entre des paires de fiches renseignées dans le système.

Name              Type           Mandatory   Default    Description
domainName        string                                Domain used for the relation.






class1Name      string                                ClassName of the first card taking part in the
                                                      relation.
card1Id         integer                               Identifier of the first card which takes part in
                                                      the relation
class2Name      string                                ClassName of the second card which takes
                                                      part in the relation.
card2Id         integer                               Identifier of the second card which takes part in
                                                      the relation.
status          string                      A         Relation status ('A' = active, 'N' = removed)
beginDate       date                                  Date when the relation was created (format
                                                      YYYY-MM-DDThh:mm:ssZ)
endDate         date                                  Date when the relation was created (format
                                                      YYYY-MM-DDThh:mm:ssZ)

###Objet "Pièce jointe"

Il représente un document encapsulé dans une fiche renseignée dans le système.

Name            Type           Mandatory    Default   Description
category        string                                Category which the attachment belongs to
                                                      (from proper Lookup list).
description     string                                Description related to the attachment.
filename        string                                Attachment name with extension
version         string                                Document version in the DMS Alfresco
author          string                                User that performs the file upload
created         date                                  Date when the document was inserted in the
                                                      DMS
modified        date                                  Date of the last change to the document in the
                                                      DMS



## Description des méthodes des web services

###Zone de gestion des fiches

Voici la liste des méthodes dédiées dans la gestion des fiches.

Name            Input                 Output           Description
createCard      Card card             integer id       It creates in the database a new card,
                                                       containing the information inserted in the
                                                       "Card" object.
                                                       It returns the "id" identification attribute.
deleteCard     string className         boolean return     It deletes logically - in the identified class -
               integer cardId                              the pre-existing card with the identified "id".
                                                           It returns “true” if the operation went
                                                           through.
updateCard     string className          boolean return    It updates a pre-existing card.
               integer cardId                              It returns “true” if the operation went
               Attribute[] attributeList                   through.
getCard        string className          Card card         It returns the required card with all
               integer cardId                              attributes specified in “attributeList” (all
               Attribute[] attributeList                   card attributes if “attributeList” is null).
getCardList    string className          Card[] cardList   It returns the card list resulting from the
               Attribute[] attributeList                   specified query, completed with all
               Query queryType                             attributes specified in “attributeList” (all
               Order[] orderType                           card attributes if “attributeList” is null).
                                                           If the query is made on a superclass, the
                                                           "className" attribute of the returned Card
                                                           objects contains the name of the specific
                                                           subclass the card belongs to, while in the
                                                           attributeList it appears the ClassId of the
                                                           same subclass.
getCardHistory string className         Card[] cardList    It returns the list of the historicized
               integer cardId                              versions of the specified card.


###Zone de gestion des titres des listes de valeurs

Voici la liste des méthodes dédiées à la gestion des titres des listes de valeurs (listes de valeurs prédéfinies auxquelles un attribut de fiche est lié).

Name           Input                    Output             Description
createLookup Lookup lookup              integer id         It creates in the database a new heading of a
                                                           data Lookup list containing information
                                                           inserted in the “Lookup” object.
                                                           It returns the "id" identification attribute.
deleteLookup integer lookupId           boolean return     It deletes logically the Lookup heading which
                                                           shows the "id".
                                                           It returns “true” if the operation went through.
updateLookup Lookup lookup              boolean return     It updates the pre-existing Lookup heading.
                                                           It returns “true” if the operation went through.
getLookupById integer id                Lookup lookup      It returns the Lookup heading which shows
                                                           the specified "Id" identification.
getLookupList string type               Lookup[]           It returns a complete list of Lookup values
              string value              lookupList         corresponding to the specified "type".
              boolean parentList                           If the "value" parameter is specified, only the
                                                           related heading is returned.
                                                           If “parentList” takes the “True” value, it returns
                                                           the complete hierarchy available for the
                                                           multilevel Lookup lists.

##Zone de gestion des relations

Voici la liste des méthodes dédiée à la gestion des relations entre fiches.

Name            Input                   Output           Description
createRelation Relation relation        boolean return   It creates in the database a new relation
                                                         between the pair of cards specified in the
                                                         "Relation" object.
                                                         It returns “true” if the operation went through.
deleteRelation Relation relation        boolean return   It deletes the existing relation between the
                                                         pair of cards specified in the "Relation" object.
                                                         It returns “true” if the operation went through.
getRelationList string domain           Relation[]       It returns the complete list of relations of the
                string className        relationList     card specified for the specified domain.
                integer cardId
getRelationHi Relation relation         Relation[]       It returns the relation history of a card starting
story                                   relationList     from a "Relation" object in which only
                                                         "Class1Name" and "Card1Id" were defined.

##Zone de gestion des processus 

Voici la liste des méthodes dédiées à la gestion des processus configurables dans CMDBuild.

Name            Input                   Output           Description
startWorkflow Card card            integer id            It starts a new instance of the workflow
              boolean CompleteTask                       described in the specified "Card".
                                                         If the “CompleteTask” parameter takes the
                                                         “true” value, the process is advanced to the
                                                         following step.
                                                         It returns the "id" identification attribute.
updateWorkflow string processId          boolean ret     It updates the information of the card in the
               Attribute[] attributeList                 specified process instance.
               boolean CompleteTask                      If the “CompleteTask” parameter takes the
                                                         “true” value, the process is advanced to the
                                                         following step.
                                                         It returns “true” if the operation went through.

##Zone de gestion des pièces jointes

Here's the list of methods dedicated to the management of documents enclosed in a card.
Name            Input                   Output           Description
uploadAttach    string className        boolean return   It uploads the specified file in the DMS
ment            integer cardId                           Alfresco and the relating connection to the
                Base64Binary file                        CMDBuild card belonging to the “className”
                string fileName                          class and having the “id” identification.
                string category                          It returns “true” if the operation went through.
                string description


downloadAtta string className       Base64Binary file It returns the file enclosed in the specified
chment       integer cardId                           card, which has the specified name.
             string fileName
deleteAttachm string className      boolean return    It removes from the DMS Alfresco the file
ent           integer cardId                          enclosed in the specified card, which has the
              string fileName                         specified name.
                                                      It returns “true” if the operation went through.
updateAttach   string className     boolean return    It updates the description of the file enclosed
ment           integer cardId                         in the specified card, which has the specified
               string fileName                        name.
               string description                     It returns “true” if the operation went through.

## Exemple de création d'un "client"

Dans ce paragraphe, nous proposons deux exemples de création de "clients" SOAP capable de s'interfacer avec CMDBuild.

###Langage Java

**ATTENTION:** nous vous conseillons d'utiliser l'outil wsdl2java [2]. Ce tutoriel implique la génération de classes support avec l'outil qui vient d'être mentionné.

2 http://ws.apache.org/axis/java/user-guide.html#WSDL2JavaBuildingStubsSkeletonsAndDataTypesFromWSDL

**Exemple d'opération suggérée: ** obtenir une liste de toutes les fiches actives de la classe "Ordinateur".

Une fois les classes générées, créez le client comme suit:

1. Créez une instance dans la classe ConfigurationContext et indiquez-y où se trouve le répertoire dépôt. Dans ce répertoire dépôt se trouvent deux répertoires: le répertoires modules qui contient le fichier rampart.mar et le répertoire conf qui contient le fichier définissant la politique de sécurité qui doit être adoptée.

```java
      ConfigurationContext configContext = ConfigurationContextFactory 
              .createConfigurationContextFromFileSystem( 

                       "/path/to/repository", 

                       null 

              );
```

2. Instanciez la classe WebservicesStub et placez-y le ConfigurationContextFactory que vous venez juste de créer

```java
      WebservicesStub stub = new WebservicesStub(configContext);
```

3. Configurez les éléments d'authentification

```java
      StAXOMBuilder builder = new StAXOMBuilder( 

                       "/path/to/repository/conf/policy.xml" 

              );

      Options options = stub._getServiceClient().getOptions();

      options.setUserName("username");

      options.setPassword("password");

      options.setProperty( 

              RampartMessageData.KEY_RAMPART_POLICY, 

              PolicyEngine.getPolicy(builder.getDocumentElement() 

      );StAXOMBuilder builder = new StAXOMBuilder( 

                       "/path/to/repository/conf/policy.xml" 

              );

       Options options = stub._getServiceClient().getOptions();

       options.setUserName("username");

       options.setPassword("password");

       options.setProperty( //

               RampartMessageData.KEY_RAMPART_POLICY, //

               PolicyEngine.getPolicy(builder.getDocumentElement() //

       );
```

4. Instanciez l'objet GetCardList et appelez le serveur 

```java
       GetCardList list = new GetCardList();

       list.setClassName("Computer");

       GetCardListResponse response = stub.getCardList(list);

       Card[] card = response.get_return();
```

5. À ce stade, vous pouvez parcourir le contenu du tableau des fiches et extraire les valeurs les plus intéressantes. Par exemple, si vous voulez récupérer la description de chaque Ordinateur, la méthode suivante suffira:

```java
       System.out.println(card[i].getDescription());
```

###Langage PHP

**ATTENTION :** puisque PHP ne prend pas en charge de manière native le standard WS-Security et les spécifications MTOM pour envoyer des messages SOAP avec des pièces jointes, nous vous recommandons d'utiliser un framework qui peut ajouter ces fonctionnalités. Pour ce tutoriel, nous avons utilisé le framework WSO2/PHP3 livré avec la licence Apache [2]. 

2 Vous pouvez trouver le site officiel à l'adresse suivante: http://wso2.org/projects/wsf/php

**Exemple d'opération suggérée :** télécharger dans CMDBuild un fichier associé à la fiche ayant l'Id = 13 de la classe "Ordinateur" (remplissant dans le système Alfresco)

1. Définition de la requête

```php
       $requestPayloadString = <<<XML

               <ns1:upload xmlns:ns1="http://soap.services.cmdbuild.org">

                        <className>Computer</className>

                        <objectid>13</objectid>

                        <file><xop:Include xmlns:xop="http://www.w3.org/2004/08/xop/include"
       href="cid:myid1"></xop:Include></file>

                        <filename>$userfile_name</filename>

                        <category>$category</category>

                       <description>$description</description>

              </ns1:upload>

      XML;
```

2. Définir une sécurité de jeton nécessaire à l'authentification

```php
      $security_options = array("useUsernameToken" => TRUE );

      $policy = new WSPolicy(array("security" => $security_options));
      $security_token = new WSSecurityToken(array("user" => "username", "password" => "password",
      "passwordType" => "Digest"));
```

3. Instancier un objet WSclient pour exécuter la requête. Vous avez à indiquer le MTOM à utiliser pour le transfert de fichier

```php
      $client = new WSClient(array("useMTOM" => TRUE, "useWSA" => TRUE, "policy" => $policy,
      "securityToken" => $security_token));
```

4. Enregistrer le contenu du fichier dans une unique chaîne de caractères en utilisant la fonction  file_get_contents()

```php
      $file = file_get_contents($_FILES['userfile']['tmp_name']);
```

5. Instancier un objet WSMessage et exécuter la requête

```php
      $url = "http://localhost:8080/cmdbuild/services/soap/FileTransfer";

      $requestMessage = new WSMessage($requestPayloadString, array("to"=> $url, "attachments" =>
      array("myid1" => $file)));
      $response = $client->request($requestMessage);
```

## Définition d'un fichier de politique d'authentification

###Structure de fichier

Le fichier de politique - qui doit être passé au module Rampart côté client - doit être défini en utilisant le langage de sécurité des services web (WS-SecurityPolicy).

Ce langage, dérivé de XML, permet de définir les règles de sécurité respectées par l'application.

Par exemple, de façon à définir l'authentification à utiliser via le jeton UserName avec le hachage de mot de passe, vous devez définir la politique comme ceci:

```xml
      <?xml version="1.0" encoding="UTF-8"?>

      <wsp:Policy wsu:Id="UTOverTransport"

       xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-ssecurity-utility-1.0.xsd"
      xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">

        <wsp:ExactlyOne>

           <sp:SignedSupportingTokens

                xmlns:sp="http://docs.oasis-open.org/ws-sx/ws-securitypolicy/200702">

               <wsp:Policy>

                 <sp:UsernameToken sp:IncludeToken="http://docs.oasis-open.org/ws-sx/ws-
      securitypolicy/200702/IncludeToken/AlwaysToRecipient">

                       <wsp:Policy>

                        <sp:HashPassword/>

                       </wsp:Policy>

                 </sp:UsernameToken>

              </wsp:Policy>

          </sp:SignedSupportingTokens>

        </wsp:ExactlyOne>

      </wsp:Policy>
```

Vous pouvez également indiquer dans le fichier de politique, certains paramètres de configuration du module Rampart.

Par exemple, si vous ne voulez pas transférer directement le mot de passe dans le code, mais le récupérer dans une base de données, vous pouvez indiquer à Rampart d'utiliser une classe personnalisée qui implémente une fonction CallbackHandler dans laquelle vous pouvez définir comment trouver le mot de passe.

Dans ce cas, vous devez modifier le fichier de politique de la façon suivante.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<wsp:Policy wsu:Id="UTOverTransport"
            xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-ssecurity-utility-1.0.xsd"
            xmlns:wsp="http://schemas.xmlsoap.org/ws/2004/09/policy">
  <wsp:ExactlyOne>
    <sp:SignedSupportingTokens xmlns:sp="http://docs.oasis-open.org/ws-sx/ws-securitypolicy/200702">
      <wsp:Policy>
        <sp:UsernameToken sp:IncludeToken="http://docs.oasis-open.org/ws-sx/ws-securitypolicy/200702/IncludeToken/AlwaysToRecipient">
          <wsp:Policy>
            <sp:HashPassword/>
          </wsp:Policy>
        </sp:UsernameToken>
      </wsp:Policy>
    </sp:SignedSupportingTokens>
    <ramp:RampartConfigxmlns:ramp="http://ws.apache.org/rampart/policy">
      <ramp:passwordCallbackClass>
        org.cmdbuild.services.soap.secure.MyPasswordHandler
      </ramp:passwordCallbackClass>
    </ramp:RampartConfig>
  </wsp:ExactlyOne>
</wsp:Policy>
```

Pour des informations plus détaillées sur WS-SecurityPolicy, merci de vous référer 
à la [documentation officielle](http://specs.xmlsoap.org/ws/2005/07/securitypolicy/ws-securitypolicy.pdf)
