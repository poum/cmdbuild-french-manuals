﻿#Web services REST

**Attention :** les informations de cette section peuvent ne pas être à jour ou ne pas être détaillées;
il est préférable de prendre l'habitude de toujours se référer au WADL, en cherchant le point d'accès
souhaité (par exemple http://example.com/cmdbuild/services/rest/v1?_wadl).

Sauf mention explicite contraire, le type de contenu (content type) des différentes méthodes HTTP sera “application/json”.

De façon à hiérarchiser la description des ressources:

- le JSON sera simplifier en omettant les caractères "
- les méthodes HTTP rapportées, les références aux points d'accès choisis seront omises

##Version 1

###Point d'accès

http://hostname:port/cmdbuild/services/rest/v1

###Format des dates 

Toutes les dates (attributs horaires, estampilles et dates) utilisent le format "yyyy-MM-dd'T'HH:mm:ss".

###Types de ressources

Les ressources qui ne nécessitent pas une session: ressources qui ne peuvent pas être atteintes sans
session active (par exemple: "/sessions").
Les ressources qui nécessitent au moins une session avec des autorisations partielles: ressources pour lesquelles
le rôle n'est pas obligatoire. Pour le moment, il n'en existe aucun exemple.
Ressources qui nécessitent une session avec des autorisations complètes: ressources pour lesquelles le rôle n'est **pas**
obligatoire.

###Gestion des sessions

L'accès aux différentes ressources est soumis à l'ouverture d'une session qui a été associée à certains droits.
Chaque session est identifiée par une chaîne alphanumérique ou "token".
Chaque session possède des permissions complètes ou partielles. Les droits sont complets quand, en plus du
"nom d'utilisateur" et du "mot de passe", un rôle valide est également fourni, tandis qu'ils sont partiels
quand le rôle n'est pas défini.

###Création d'une session

```
     POST .../sessions/ HTTP/1.1

     [différents autres en-têtes]

     {

            username : ...,

            password : ...

     }

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       _id : ${SESSION}

                       username : ...

                       password : null

                       role : ...

            }

     }
```

###Lecture d'une session

```
     GET .../sessions/${SESSION}/ HTTP/1.1

     [différents autres en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

             username : ...

             password : null

             role : ...

      }
```

Prêtez attention à l'attribut role, s'il a la valeur "null" ou s'il est absent, alors vous aurez un
accès restreint, autrement vous aurez un accès complet. Si l'utilisateur n'a qu'un seul rôle, alors
il sera déjà actif dès la création dans la session. Dans le cas contraire, vous devrez mettre à jour
les autorisations.

###Mise à jour d'une session

```
      PUT .../sessions/${SESSION}/ HTTP/1.1

     [différents autres en-têtes]

      {

             role : ...,

      }

      HTTP/1.1 204 No Content

     [différents en-têtes]
```

Vous pouvez modifier le rôle associé à une session (c'est-à-dire passer d'une session aux droits
restreints à une session complète et vice-versa).

###Suppression d'une session

```

      DELETE .../sessions/${SESSION}/ HTTP/1.1

     [différents autres en-têtes]

      HTTP/1.1 204 OK

     [différents en-têtes]
```

###Lecture de tous les rôles associés à une session

```
      GET /sessions/${SESSION}/roles/ HTTP/1.1


     [différents autres en-têtes]


     HTTP/1.1 200 OK

     [différents en-têtes]

     {

             data : [foo, bar]

     }
```

###Accès à toutes les ressources qui requièrent une session

Après qu'une session a été créée ou mise à jour, son identifiant doit être utilisée pour toutes les
ressources qui en ont besoin. L'identifiant doit être envoyé dans l'en-tête de la requête.

```
     GET .../classes/foo/cards/ HTTP/1.1

     [différents autres en-têtes]
     CMDBuild-Authorization: 0123456789ABCDF...
```

Si la ressource nécessite une session et que le token n'est pas envoyé ou n'est pas correct, alors
la réponse sera:

```
     HTTP/1.1 401 Unauthorized

     [différents en-têtes]
```

###Gestion des listes de valeurs 

###Lecture de tous les types

```
     GET .../lookup_types HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                    _id : ...,

                                    name : ...,

                                    parent : ...

                       },

                       ...

                       {

                                    ...

                       },

            ]

     }

```
###Lecture des détails d'un type

```
     GET .../lookup_types HTTP/1.1

     [différents en-têtes]


     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       _id : ...,

                       name : ...,

                       parent : ...

            }

     }
```

###Lecture de toutes les valeurs d'une liste

```
     GET .../lookup_types/AlfrescoCategory/values HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                 _type" : AlfrescoCategory,

                                 _id" : 703,

                                 code" : null,

                                 description" : Document,

                                 parent_type" : null,

                                 active" : true,

                                 number" : 1,

                                 default" : false,

                                 parent_id" : null

                       },

                       ...

                       {

                                ...

                       }

            ]

     }
```

###Lecture des détails d'une valeur

```
     GET .../lookup_types/AlfrescoCategory/values/703 HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       _type" : AlfrescoCategory,

                       _id" : 703,

                       code" : null,

                       description" : Document,

                       parent_type" : null,

                       active" : true,

                       number" : 1,

                       default" : false,

                       parent_id" : null

            }

     }
```

###Gestion des classes et des fiches


```
####Lecture de toutes les classes

```
     GET .../classes HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            }

            data : [

                       {

                                 _id : Asset,

                                 name : Asset,

                                 description : Asset,

                                 parent : Class,

                                 prototype : true

                       },

                       ...

                       {

                                 ...

                       }

            ]

     }

```

####Lecture des détails d'une classe


```
     GET .../classes/Asset HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       _id : Asset,

                       name : Asset,

                       description : Asset,

                       description_attribute_name : Description,

                       parent : Class,

                       prototype : true

            }

     }
```

####Lecture de tous les attributs d'une classe


```
     GET .../classes/Asset/attributes HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                             lookupType : null,

                             index : 1,

                             group : General data,

                             unique : false,

                             scale : null,

                             editorType : null,

                             targetClass : null,

                             active : true,

                             description : Code,

                             type : string,

                             displayableInList : true,

                             values : [],

                             length : 100,

                             name : Code,

                             _id : Code,

                             filter : null,

                             mandatory : false,

                             precision : null,

                             defaultValue" : null,

                             inherited" : true

                       },

                       ...

                       {

                             ...

                       }

            ]

     }
```

####Création d'une fiche

```
     POST .../classes/Building/cards HTTP/1.1

     [différents en-têtes]

     {

            Code = test,

            ...

     }

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : 3217

     }
```

####Lecture de toutes les fiches

```
     GET .../classes/Asset/cards HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                            Room : null,

                            Description : Acer - AL1716,

                            Notes : null,

                            Workplace : null,

                            ScreenSize : null,

                            Brand : 138,

                            PurchaseDate : null,

                            SerialNumber : null,

                            AcceptanceNotes : null,

                            Code : MON0001,

                            TechnicalReference" : null,

                            FinalCost : null,

                            AcceptanceDate : null,

                            Model : AL1716,

                            _id : 550,

                            _type : Asset,

                            Assignee : 134,

                            Type" : null,

                            Supplier" : null

                       },

                       {

                            ...

                       }

            ]

     }
```

####Lecture des détails d'une fiche 
```
     GET .../classes/Asset/cards/550 HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       Room : null,

                       Description : Acer - AL1716,

                       Notes : null,

                       Workplace : null,

                       ScreenSize : null,

                       Brand : 138,

                       PurchaseDate : null,

                       SerialNumber : null,

                       AcceptanceNotes : null,

                       Code : MON0001,

                       TechnicalReference" : null,

                       FinalCost : null,

                       AcceptanceDate : null,

                       Model : AL1716,

                       _id : 550,

                       _type : Asset,

                       Assignee : 134,

                       Type" : null,

                       Supplier" : null

            }

     }
```

####Mise à jour d'une fiche

```
     PUT .../classes/Building/cards/3217 HTTP/1.1

     [différents en-têtes]

     {

            Code = test,

            ...

     }

     HTTP/1.1 204 No Content
```

####Suppression d'une fiche

```
     DELETE .../classes/Building/cards/3217 HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 204 No Content
```

###Gestion des domaines et des relations

####Lecture de tous les domaines
```
     GET .../domains HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                        total : ...

            }

            data : [

                        {

                                  _id : WorkplaceComposition,

                                  name : WorkplaceComposition,

                                  description : Workplace composition

                        },

                        ...

                        {

                                  ...

                        }

            ]

     }
```

###Lecture des détails d'un domaine 
```
     GET .../domains/WorkplaceComposition HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data" : {

                        _id : WorkplaceComposition,

                       descriptionMasterDetail : Asset,

                       sourceProcess : false,

                       descriptionInverse : belongs to workplace,

                       descriptionDirect : includes assets,

                       destinationProcess : false,

                       name : WorkplaceComposition,

                       source : Workplace,

                       destination : Asset,

                       cardinality : 1:N,

                       description : Workplace composition

            }

     }
```

####Lecture de tous les attributs d'un domaine

```
     GET .../domains/UserRole/attributes HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : 1

            },

            data : [

                       {

                              targetClass : null,

                              editorType : null,

                              unique : false,

                              description : Default Group,

                              index : 1,

                              defaultValue : null,

                              lookupType : null,

                              active : true,

                              precision : null,

                              inherited : false,

                              displayableInList : true,

                              name : DefaultGroup,

                              mandatory : false,

                              type : boolean,

                              group : null,

                              scale : null,

                              values : [],

                              _id : DefaultGroup,

                              filter" : null,

                              length" : null

                       }

            ]

     }
```
####Création d'une relation

```
     POST .../domains/UserRole/relations/ HTTP/1.1

     [différents en-têtes]

     {

            data : {

                       _sourceType : User,

                       _sourceId : 123,

                       _destinationType : Role,

                       _destinationId : 456,

                       DefaultGroup : false

            }

     }

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : 789

     }
```

####Lecture d'une relation

```
     GET .../UserRole/relations HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                               _destinationDescription : Helpdesk,

                               _sourceDescription : Jones Patricia,

                               _sourceType : User,

                               _sourceId : 678,

                               _destinationType : Role,

                               _type : UserRole,

                               _destinationId : 677,

                               _id : 681

                       },

                       ...

                       {

                               ...

                       }

            ]

     }
```

####Lecture des détails d'une relation
```
     GET .../domains/UserRole/relations/681 HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       _id : 681,

                       _destinationId : 677,

                       _destinationType : Role,

                       _destinationDescription : Helpdesk,

                       _sourceId : 678,

                       _sourceDescription : Jones Patricia,

                       _type : UserRole,

                       DefaultGroup : null,

                       _sourceType : User

            }

     }
```

####Mise à jour d'une relation

```
     POST .../domains/UserRole/relations/123 HTTP/1.1

     [différents en-têtes]

     {

            data : {

                       DefaultGroup : true

            }

     }

     HTTP/1.1 204 No Content
```

####Suppression d'une relation

```
     DELETE .../domains/UserRole/relations/123 HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 204 No Content
```

###Gestion des processus, instances et activités

####Lecture de tous les processus
```
     GET .../processes HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            }

            data : [

                       {

                                 _id : RequestForChange,

                                 name : RequestForChange,

                                 prototype : false,

                                 parent : Activity,

                                 description : Request for change

                       }

                       ...

                       {

                                 ...

                       }

            ]

     }

```

####Lecture des détails d'un processus

```
     GET .../processes/RequestForChange HTTP/1.1

     [différents en-têtes]




     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       name : RequestForChange,

                       statuses : [

                               6,

                               7,

                               8,

                               10

                       ],

                       description : Request for change,

                       description_attribute_name : Description,

                       prototype : false,

                       _id : RequestForChange,

                       parent : Activity,

                       defaultStatus : 6

            }

     }
```

####Lecture de tous les attributs d'un processus

```
     GET .../processes/RequestForChange/attributes HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                 name : Dummy,

                                 scale : null,

                                 _id : Dummy,

                                 inherited : false,

                                 precision" : null,

                                 description : Dummy,

                                 editorType : null,

                                 type : string,

                                 unique : false,

                                 targetClass : null,

                                 defaultValue : null,

                                 values : [],

                                 filter : null,

                                 group : null,

                                 lookupType : null,

                                 length : 100,

                                 mandatory : false,

                                 index : 0,

                                 active : true,

                                 displayableInList : false

                       },

                       ...

                       {

                                   ...

                       }

            ]

     }
```

####Lecture de toutes les activités de démarrage d'un processus

```
     GET .../processes/RequestForChange/start_activities HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : 1

            }

            data : [

                       {

                                   _id : RegisterRFC,

                                   description : Register RFC,

                                   writable : true

                       }

            ]

     }
```

####Lecture des détails d'une activité de démarrage
```
     GET .../processes/RequestForChange/start_activities/RegisterRFC HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       attributes : [

                                {

                                             mandatory : true,

                                             _id : Requester,

                                             index : 0,

                                             writable : true

                                },

                                {

                                             writable : true,

                                             index : 1,

                                             _id : RFCDescription,

                                             mandatory : true

                                }

                       ],

                       instructions : ...,

                       description : Register RFC,

                       widgets : [],

                       _id : RegisterRFC

            }

     }
```

####Lecture de toutes les activités d'une instance

```
     GET .../processes/RequestForChange/instances/1460/activities HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                 description : Formal evaluation,

                                 _id : foo,

                                 writable : true

                       },

                       ...

                       {

                                 ...

                       }

            ]

     }
```

####Lecture des détails d'une activité
```
     GET .../processes/RequestForChange/instances/1460/activities/foo HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       widgets : [

                                {

                                        type : .OpenAttachment,

                                        active : true,

                                        _id : widget-57a9a9d9,

                                        required : false,

                                        data : {

                                                   ...

                                        },

                                        label : Attachments

                                },

                                ...

                                {

                                        ...

                                }

                       ],

                       _id : FormalEvaluation,

                       attributes : [

                                {

                                           mandatory : false,

                                           writable : false,

                                           _id : RFCStartDate,

                                           index : 0

                              },

                              ...

                              {

                                           ...

                              }

                     ],

                     instructions : ...,

                     description : Formal evaluation

            }

     }
```

####Création d'une instance de processus

```
     POST .../processes/RequestForChange/instances HTTP/1.1

     [différents en-têtes]

     {

            Requester : 123,

            RFCDescription : test,
            _advance = true

     }




     HTTP/1.1 200 OK

     [différents en-têtes]

     {
             data : 1460

     }
```

Selon la valeur de l'attribut "_advance", une instance sera soit avancée, soit enregistrée.

####Lecture de toutes les instances de processus

```
     GET .../processes/RequestForChange/instances HTTP/1.1

     [différents en-têtes]




     HTTP/1.1 200 OK

     [différents en-têtes]

     {

             meta : {

                        total : ...

             }

             data : [

                        {

                                  Category : null,

                                  FinalResult : null,

                                  RFCEndDate : null,

                                  ExecutionEndDate : null,

                                  RFCDescription : test,

                                  Requester : 123,

                                  ...

                                  _type : RequestForChange,

                                  _name : null,

                                  _id : 1460,

                                  _status : 6

                       },

                       ...

                       {

                               ...

                       }

            ]

     }
```

####Lecture des détails d'une instance

```
     GET .../processes/RequestForChange/instances/1460 HTTP/1.1

     [différents en-têtes]




     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       FinalResult : null,

                       RFCEndDate : null,

                       ExecutionEndDate : null,

                       RFCDescription : test,

                       Requester : 123,

                       ...

                       _type : RequestForChange,

                       _name : null,

                       _id : 1460,

                       _status : 6

            }

     }
```

####Mise à jour d'une instance

```
     PUT .../processes/RequestForChange/instances/1460 HTTP/1.1

     [différents en-têtes]

     {

             Requester : 123,

             RFCDescription : test,
             _advance = true

     }




     HTTP/1.1 204 No Content
```

Selon la valeur de l'attribut "_advance", une instance peut soit être avancée, ou simplement enregistrée.

####Suppression d'une instance

```
     DELETE .../processes/RequestForChange/instances/1460 HTTP/1.1

     [différents en-têtes]




     HTTP/1.1 204 No Content
```

###Gestion des menus

####Lecture du menu

```
     GET .../menu HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       menuType : root,

                       children : [

                                {

                                      menuType : folder,

                                      objectId : 0,

                                      children" : [

                                               ...

                                      ],

                                      ...

                                },

                                ...

                                {

                                      objectDescription : Employee,

                                      children : [],

                                      objectType : Employee,

                                      objectId : 0,

                                      index : 34,

                                      menuType : class

                                },

                       ]

            }

     }
```

###Gestion de configuration

####Lecture de toutes les catégories des pièces jointes

```
     GET .../configuration/attachments/categories HTTP/1.1

     [différents en-têtes]




     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                 _id : Document,

                                 description : Document"

                       },

                       ...

                       {

                                 description : Image,

                                 _id : Image

                       }

            ]

     }
```

####Lecture de tous les attributs d'une catégorie

```
     > GET .../configuration/attachments/categories/Document/attributes HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                 length : null,

                                 index : 0,

                                 editorType : null,

                                 targetClass : null,

                                 inherited : null,

                                 ...

                       },

                       ...

                       {

                                 ...

                       }

            ]

     }
```

####Lecture des états de tous les processus

```
     GET .../configuration/processes/statuses HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            },

            data : [

                       {

                                 _id : 6,

                                 description : Open,

                                 value : open

                       },

                       ...

                       {

                                 ...

                       }

            ]

     }
```

###Gestion des droits

####Lectures de tous les droits d'un rôle

```
     GET .../roles/Foo/classes_privileges HTTP/1.1

     [différents en-têtes]



     HTTP/1.1 200 OK

     [différents en-têtes]

     {
              meta : {

                         total : ...

              },

              data : [

                         {

                                   name : UPS,

                                   description : UPS,

                                   mode : w,

                                   _id : UPS

                         },

                         ...

                         {

                                   name : License,

                                   _id : License,

                                   description : License,

                                   mode" : r

                         }

              ]

       }
```

###Gestion des pièces jointes

S'applique de la même façon que celle indiquée pour les instances de processus. Donc en remplaçant:

       .../classes/${CLASS_ID}/cards/${CARD_ID}
par

       .../processes/${PROCESS_ID}/instances/${PROCESS_ID}

vous aurez les mêmes possibilités.

####Téléchargement d'une pièce jointe

```
       POST .../classes/Building/cards/64/attachments HTTP/1.1

       [différents en-têtes]
       Content-Type : multipart/form-data



       == Part, name : "attachment", content-type : "application/json" ==

       {

                _description : this is a test,

                _category" : Image,

       }

       ====



       == Part, name : "file", content-type : "*/*" ==

       binary

       ====




       HTTP/1.1 200 OK

       [différents en-têtes]

       {

                data : abc123

       }
```


Notes:
- la méthode utilise le content-type “multipart/form-data”
- télécharge des fichiers et indique les propriétés

####Lecture de toutes les pièces jointes

```
       GET .../classes/Building/cards/64/attachments HTTP/1.1

       [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            meta : {

                       total : ...

            }

            data : [

                       {

                                 _name : test.jpg,

                                 _id" : "abc123,

                                 _version : 1.3,

                                 _description : this is a test,

                                 _modified" : 2014-12-16T12:35:12,

                                 _category" : Image,

                                 _created" : 2014-11-20T14:20:04,

                                 _author" : admin

                       },

                       ...

                       {

                                 ...

                       }

            ]

     }
```

####Lecture des détails d'une pièce jointe

```
     GET .../classes/Building/cards/64/attachments/abc123 HTTP/1.1

     [différents en-têtes]

     HTTP/1.1 200 OK

     [différents en-têtes]

     {

            data : {

                       _category : Image,

                       _created : 2014-11-20T14:20:04,

                       _description : this is a test,

                       _author : admin,

                       _id : abc123,

                       _version : 1.3,

                       _modified : 2014-12-16T12:35:12,

                       _name : test.jpg

            }

     }
```

####Téléchargement d'une pièce jointe

```
     GET .../classes/Building/cards/64/attachments/abc123/download.jpg HTTP/1.1

     [différents en-têtes]




     HTTP/1.1 200 OK

     [différents en-têtes]

     Content-Type : application/octet-stream
```
####Mise à jour d'une pièce jointe

```
     PUT .../classes/Building/cards/64/attachments/abc123 HTTP/1.1

     [différents en-têtes]
     Content-Type : multipart/form-data

       == Part, name : "attachment", content-type : "application/json" ==

       {

                _description : this is a test,

                _category" : Image,

       }

       ====



       == Part, name : "file", content-type : "*/*" ==

       binary

       ====




       HTTP/1.1 204 No Content
```


Notes:
- la méthode utilise le content-type “multipart/form-data”
- télécharge des fichiers et indique les propriétés

####Suppression d'une pièce jointe

```
       DELETE .../classes/Building/cards/64/attachments/abc123 HTTP/1.1

       [différents en-têtes]




       HTTP/1.1 204 No Content
```


