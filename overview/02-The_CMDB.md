#La CMDB

##Qu'est-ce qy'une CMDB ?

Une CMDB est un système de stockage et de consultation qui gère les éléments d'information d'une compagnie. C'est le dépôt officiel central qui fournit une vue cohérente des services IT.
C'est un système dynamique qui représente la situation actuelle et la connaissance des éléments d'information technologiques et des entités qu'ils concernent.

C'est un système de supervision pour les processus exécutés, décrits et gérés via les fonctions de processus.

Quels éléments d'information gère la CMDB ?

- matériels: ordinateurs, périphériques, systèmes réseau, équipements téléphoniques
- logiciels: de base, d'environnement, applications
- services fournis à l'utilisateur
- autres ressources internes et externes

A quelles questions la CMDB répond-elle ?

- Où se trouve un CI (Configuration Item i.e. élément de configuration) ?
- Qui l'utilise ?
- A qui appartient-il ?
- De quoi est-il constitué ?
- Quels sont les autres CI similaires et où sont-ils ?
- Ai-je assez de licences pour utiliser le logiciel ?
- Que s'est-il passé dans le cycle de vie du CI ?
- Quels autres CI peuvent être impactés par un changement de ce CI ?
- Quelles sont les activités en attente d'une action de ma part ?

![](../overview/02-The_CMDB.jpg)

##Pourquoi utiliser une CMDB ?

L'utilisation d'une CMDB permet de contrôler la situation des éléments d'information gérés, en sachant à tout moment de quoi ils sont composés, où se trouvent leurs composants et leurs relations fonctionnelles.

Des informations manquantes ou périmées induisent des coûts inutiles, des opérations redondantes, des retards dans la résolution des problèmes, autant d'obstacles aux activités de la compagnie.

Les mots clefs d'une CMDB snt le temps de réponse et le contrôle du système.

Une CMDB permet de :

- réduire les problèmes de votre système d'information
- résoudre plus rapidement les problèmes restant
- résoudre immédiatement la majeure partie des problèmes sans impliquer trop fréquemment les équipes d'experts
- garder une trace de toutes les opérations de modification des données
- avoir un dépôts de données statistiques utiles pour contrôler les SLA

En d'autres mots, les coûts diminuent et la qualité de service s'améliore.

##Guide d'implémentation

Les "bonnes pratiques" ITIL auxquelles se réfère CMDBuild requièrent explicitement l'adoption d'un outil d'information pour gérer la CMDB.

L'introduction d'un tel outil doit être préparée de manière adaptée en terme d'organisation et de formation de manière à réduire les risques d'échec ou de rejet de l'outil.

En gérant le projet, il est important de:

- adopter une stratégie visant à une implémentation progressive et flexible
- choisir un niveau de détail proportionnel aux besoins réels de l'organisation et aux ressources humaines, financières, aux éléments d'information et technologiques disponibles (si vous avez un système vraiment extensible et modulaire, il sera préférable que vous utilisiez les extensions autonomes suivantes de gestion du modèle de données plutôt que de surcharger le schéma initial)
- introduire la nouvelle application dans un système organisationnel reposant sur des responsabilités, des procédures et des rôles bien définis et formalisés

Un projet réussi est celui qui a pris en compte les impacts et les changements induits par le système et qui a obtenu l'adhésion explicite des dirigeants de l'organisation.

Démarrer et gérer le projet en s'appuyant sur les conseils d'experts ITIL permet de bénéficier de l'expérience et des "meilleures pratiques" acquises et développées dans différentes situations et différents pays, d'accélérer leur mise en oeuvre et de réduire les risques d'échec.

##Open Source

Un solution Open Source permet de:

- éviter (grandement) les coûts de licences
- récupérer le code source pour éviter la dépendance à un fournisseur
- garantir une grande liberté d'utilisation, dans différentes situations et pour divers besoins
- réutiliser des fonctionnalités évolutives développées sur demande d'autres utilisateurs
- coopérer avec une communauté d'utilisateurs qui partagent leur expérience en terme d'organisation et d'implémentation (modèle de données, processus, etc.)

Une solution Open Source n'est pas exempte de coûts lors de sa mise en oeuvre, qu'il s'agisse de services externes ou de coûts internes, mais de nombreuses sources faisant autorité reconnaissent avoir abaissé leur coût total de possession (TCO ou Total Cost Ownership).

Un produit Open Source n'est pas nécessairement un bon produit maus l'évolution du marché fournit un tas de solutions Open Source technologiquement plus avancées et mieux supportées que les applications propriétaires concurrentes.

Les composants Open Source, intégrés ou interopérables avec CMDBuild sont choisis parmi les plus complets, les plus répandus et les plus aboutis.

![](../overview/02-Open_source.jpg)

