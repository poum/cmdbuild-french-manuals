#Bonnes pratiques ITIL

De façon à maintenir les niveaux de service requis, il est recommandés aux gestionnaires IT d'adopter un système de gouvernance IT organisant les processus de gestion en terme de flux, de rôles et de responsabilités.

ITIL (IT Infrastructure Library) est un modèle constitué de lignes de conduite et de bonnes pratiques pour la gestion des services IT, adopté avec succès dans divers contextes et décrits dans des publications dédiées.

![](../overview/B-ITIL.jpg) 

Développé pour la stratégie ICT du gouvernement du Royaume Uni à la fin des années 80, il s'est répandu dans le monde entier en comme "standard de facto", non propriétaire, et promouvant les standards récents ISO / IEC 20000 (précédemment BS 15000).

Les livres des fondamentaires d'ITIL v3 s'appliquent à différents domaines:

- Stratégie des services : alignement entre les affaires et les services IT
- Conception des services : planification de la gestion des services
- Transition des services : gestion des changements et démarrage de la production
- Mise en oeuvre des services : gestion des processus de production
- Amélioration continue des services

Parmi les principaux avantages d'ITIL, on trouve:

- facilité d'utilisation grâce à la philosophie "adopter et adapter" (la méthode doit être adaptée aux besoins spécifiques de la compagnie et activée d'une façon progressive et sélective)
- approche processus
- forte spécialisation et totalement orientée vers les services IT
- environnement IT selon un système et une vision intégrée
- approche proactive dans la gestion des services
- pour résumer, une meilleure qualité et des coûts moindres

Parmi les pprocessus ITIL les plus courants, on trouve ceux de planification et d'exécution, tels que:

- la gestion des changements
- la gestion des configurations
- la gestion des événements
- la gestion des incidents
- la gestion des problèmes
- la gestion du catalogue de services

Pour chaque processus, ITIL gère la description, les composants de base, les critères et les outils pour la gestion de la qualité, les rôles et responsabilités des ressources impliquées, les points d'intégration avec d'autres processus (pour éviter les duplications et les inefficacités).

CMDBuild fournit une prise en charge complète des bonnes pratiques ITIL qui sont conformes à la fois à l'implémentation d'une CMDB et à sa mise à jour, et aux mécanismes fournis pour la configuration et l'utilisation des processus de gestion IT. 
