#Fonctionnalités de CMDBuild

CMDBuild comprend deux modules principaux:

- le module d'administration, utilisé pour définir et modifier le modèle de données de CMDBuild, pour définir et importer des processus et des rapports, pour définir des utilisateurs et des groupes et d'autres fonctions de servitude 
- le module de gestion, utilisé pour gérer des fiches configurées dans le système, pour exécuter des processus et des rapports et d'autres fonctions utilitaires

##Module d'administration

Les principales fonctionnalités du module d'administration comprennent:

- création de nouvelles classes, i.e. nouvelles typologies d'objets
- création et modification d'attributs (communs et géographiques) d'une classe
- définition de widgets à placer dans le formulaire de gestion de la classe
- définition des informations de persistence dans les processus et importation des flux conçus de façon externe
- affichage du modèle de données (complet ou limité à la classe demandée)
- création de "typologies de relations" parmi les classes ("domaines") et des attributs distinctifs liés
- création de "vues", à la fois via des filtres et via des requêtes SQL, qui peuvent être utilisées d'une façon similaire aux classes
![](../overview/06-Administration)
- création de filtres prédéfinis utilisés lors de la référence à des données depuis diverses classes
- définition d'arbres de navigation (sous-ensembles du graphique "domaines")
- création de listes de valeurs (lookup) pour gérer des attributs avec des valeurs prédéfinies
- définition de tableaux de bord
- import d'agencement de rapports personnalisés conçus de façon externe
- définition de menus personnalisés pour différents groupes d'utilisateurs
- définition de rôles et de droits d'accès pour diverses classes, également limité à des groupes de lignes et de colonnes
- configuration du gestionnaire de tâches et de ses diverses opérations exécutées en arrière plan
- définition de modèles de notification par email
- configurations GIS
- configurations BIM
- définition de paramètres et d'options

##Module de gestion

Les principales fonctionnalités du module de gestion des données comprennent:

- gestion des fiches ou des "vues" sur les fiches de données
    - recherche sur des fiches en indiquant à la fois des filtres sur les données et les relations standards avec d'autres classes, incluant la possibilité d'enregistrer de nouveaux filtres ou d'utiliser des filtres prédéfinis
    - insertion, création et modification de fiches
![](../overview/06-Gestion)
    - utilisation de widgets mis à disposition
    - vue maître / détails
    - création et modification de relations entre les fiches
    - consultation de l'historique des modifications des données et des relations entre les objets gérés
    - import de pièces jointes liées aux fiches
    - affichage de fiches géoréférencées dans la cartographie
    - visionneuse 3D des modèles BIM
    - affichage de fiches choisies en définissant de filtres sur les colonnes et les lignes
    - affichage de la fiche actuelle avec les détails des relations
- gestion des processus
    - démarrage de processus
    - progression des processus en utilisant les widgets fournis
    - consultation des états intermédiaires des processus
- rapports
    - exécution de rapports complexes conçus à l'aide d'éditeurs externes et importés dans le système (comprenant également divers graphiques, codes barre, tableaux et autres éléments avancés)
- tableaux de bord
    - consultation de graphiques configurés dans le système et contrôles des indicateurs clefs de performance associés (KPI)
- graphe des relations
- fonctions utilitaires
    - modification de masse de fiches
    - import CSV
    - export CSV
    - modification du mot de passe
