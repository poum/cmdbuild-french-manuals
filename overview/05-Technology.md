#Technologie

CMDBuild a été conçu et développé avec les standards les plus avancés et les meilleures technologies disponibles.

CMDBuild est conçu avec une architecture orientée services (SOA), organisée avec des composants et des services qui coopèrent également avec des applications externes via des services web. Une architecture de services représente la meilleure solution pour créer des applications capable de monter en charge et maintenables (et interopérables).

L'interface utilisateur est créée avec Ajax. Cette solution, partie du nouveau paradigme web 2.0, fournit une application plus intuitive, améliore l'interaction et obtient des temps de réponses du système plus rapides.

Les composants du serveur sont développées en Java, indépendant de la plate-forme, orienté objet, utilisé depuis longtemps par de grandes organisations pour développer des applications web d'entreprise grâce à la validité des spécifications J2EE, les bibliothèques et d'autres produits pour créer eurs services.

Les fonctionnalités de l'application sont créées avec la prise en charge de certains des composants Open Sourceles plus matures et les plus populaires disponibles sur le marché. La CMDB s'appuie sur la base de données PostgreSQL, la plus avancée et la plus solide des bases de données Open Source.

Quans nous avons conçu CMDBuild, nous avons choisi de suivre les tendances technologiques les plus récentes, pour réutiliser les meilleurs modèles de conception, pour adhérer - si possible - aux standards normatifs les plus populaires.

##Architecture Orientée Services

De façon à rendre différentes applications interopérables, elles doivent être créées comme composants qui coopèrent avec l'implémentation des services et ces services doivent être configurés via des interfaces haut niveau définies en utilisant les protocoles standards.

Via les services web SOAP - et l'autorisation des politiques de sécurité - , CMDBuild fournit les données renseignées dans la CMDB et des méthodes de gestion pour autoriser l'utilisation depuis des
applications externes impliquées avec l'information elle-même, à la fois pour la gestion
technique et pour l'administration.

Les services web crée également l'interaction avec les composants graphiques intégrés dans CMDBuild:

- Enhydra Shark (moteur de processus)
- Alfresco (gestion des documents)

Les portlets au standard JRS 168 qui communiquent avec CMDBuild via les services web SOAP liés, crée l'interopérabilité avec les applications "portails", par exemple Liferay.

![](../overview/05-SOA.jpg)

##Composants Open Source

CMDBuild utilise seulement des composants Open Source choisis pour leur validité technologique et leur diffusion. En particulier, les composants logiciels intégrés ou ceux qui interagissent avec CMDBuild comprennent:

- bibliothèque Ext JS pour les composants clients basés sur la technologie Ajax
- base de données PostgreSQL utilisant le module "spatial" PostGIS
- conteneur de servlet Tomcat
- moteur JasperReports avec l'éditeur IReport
- moteur de processus (Together Workflow Server) en conjonction avec Together Workflow Editor
- planificateur Quartz
- Alfresco pour la gestion des documents document 
- Portail Liferay pour la publication de Portlet JSR
- OCS Inventory pour l'inventaire automatique des éléments
- OpenLDAP pour l'accès aux systèmes d'authentification externe 
- GeoServer et OpenLayers pour les fonctionnalités GIS
- BIMServer comme dépôt pour les modèles IFC 
- Apache AXIS pour l'implémentation des services web

![](../overview/05-Open_source.jpg)

##Historique des modifications

Conformément aux indications ITIL, pour assurer la traçabilité complète des opérations, CMDBuild historise les fiches modifiées (à la fois pour le suivi en version de chaque attribut et des éventuelles relations).

![](../overview/05-History.jpg)

De cette façon, vous pouvez à la fois voir la situation versionnée de chaqye fiche et afficher un rapport à toute date antérieure.

Le mécanisme est rendu possible par l'héritage fourni par la base de données PostgreSQL.

CMDBuild emploie de telles fonctionnalités pour créer des tables (en utilisant la syntaxe SQL "inherits") qui dérivent de la table principale et qui enregistre le versionnage complet de chacune des fiches et de leurs relations.

##Standard

L'utilisation de standards et de formats ouverts favorise l'interopérabilité de l'application et la réutilisation des composants.

CMDBuild utilise les standards suivants:

- SOAP pour l'implémentation des services web
- schéma XML pour la conception des rapports
- le format XPDL 2.0 pour décrire les processus standardisé par le WFMC (Workflow Management Coalition)
- XML et les schémas XSLT pour synchroniser les données avec les systèmes d'inventaires automatisés
- schémas XML pour la fusion des mails avec Open Office
- JSON comme format d'échange entre le client et le serveur
- protocole WS-Security pour l'authentification des services web, standardisé par OASIS
- standard JSR 168 pour publier les Portlets sur les portails
- protocole WMS pour gérer les cartes
- protocole WFS pour gérer les fonctionnalités vectorielles
- protocoles IMAP et POP3 pour l'accès aux serveurs de messagerie
