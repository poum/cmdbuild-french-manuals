#Les réponses apportées par CMDBuild

CMDBuild apporte des réponses au questions et aux besoins des départements IT, en simplifiant la gestion des activités de gestion et en garantissant la disponibilité d'informations précises et à jour.
Voici certaines questions récurrentes relatives à la gestion des infrastructures IT que CMDBuild se propose d'adresser:

##Inventaire des éléments IT
- combien de PC mon centre a-t'il à sa disposition ?
- lesquels ont été acheté auprès d'un fournisseur particulier ?
- combien de nouveaux PC ai-je installé chaque année ces trois dernières années ? Combien ai-je arrêté d'utiliser ?
- combien d'écrans ai-je en entrepôt ? De quel modèle ? Quand leur garantie expire-t'elle ?

##Maintenance des ressources matérielles

- attendu que je dois déplacer un bureau entier dans un autre bâtiment, sur combien d'objets IT dois-je travailler ? Quelle en est la liste mise à jour ?
- parmi les PC dont les utilisateurs veulent acheter un certain logiciel, combien y en a-t'il qui ont moins de 512 Mo de RAM et qui doivent être mis à jour ?
- combien d'entre eux ont plus de 4 ans et ont une garantie qui a expirée, ce qui suggère d'arrêter de les utiliser ?
- quels serveurs sont injoignables quand vous coupez un UPS particulier pour réaliser une maintenance ?
- combien d'utilisateurs partagent une imprimante donnée ?
- à quel point d'accès réseau un PC est-il connecté et à quel port du switch ce point d'accès réseau est connecté ?

##Gestion des licences logicielles

- ai-je assez de licences pour que tous les utilisateurs d'un bureau opérationnel puissent utiliser un logiciel particulier ?
- quelle est la liste des licences utilisées par chaque département ?
- quelles licences arrivent à expiration ?

##Catalogue de service
- quels sont les services d'information disponibles pour les utilisateurs et lesquels sont souscrits par chaque utilisateur ?
- quels utilisateurs dois-je informer si une application web n'est pas disponible ?
- de quelles licences dispose l'utilisateur que j'appelle au sujet d'une application gérée ?
- quel SLA dois-je garantir ?

##Traçabilité dans la durée
- quels sont les bureaux qui se plaignent de trop de problèmes ?
- quels utilisateurs disposent d'un compte pour accéder au réseau à une date donnée ?
- quand ai-je installé le dernier correctif d'un logiciel donné ? Quelle en était la version ?

##Gestion des équipements téléphoniques
- quels utilisateurs n'ont pas de téléphone ?
- y a-t'il des prises téléphoniques libres dans une pièce où il faut déplacer vos employés ?

##Gestion de la documentation
- les documents du projet liés à une application logicielle commandée sont-ils immédiatement récupérables ?
- tous les opérateurs concernés ont-ils les manuels techniques nécessaires à leur disposition pour configurer un ordinateur ou un périphérique ?

##Gestion des processus
- les activités en attente sont-elles signalées aux opérateurs IT dont on attend l'intervention ? Ces opérateurs sont-ils guidés par le système quand ils réalisent l'intervention ?
- des notifications automatiques sont-elles disponibles pour faciliter la prise en charge des activités ?
- quels sont les éléments ou les intervenants qui sont principalements concernés pour une assistance donnée ?
- les modifications de configuration sont-elles complètement et immédiatement enregistrées dans le système ?
- existe-t'il des rapports disponibles sur les activités de développement et sur les mesures de performance ?
- les SLA calculés pour chacun des services me sont-ils communiqués ?

![](../overview/03-CMDBuild_answers.jpg)
