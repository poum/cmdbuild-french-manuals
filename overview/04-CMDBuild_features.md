#Fonctionnalités de CMDBuild

CMDBuild est une solution technologiquement avancée, flexible et personnalisable pour la gestion des infrastructures IT:
- avancée car elle repose sur une architecture technologique correspondant à l'état de l'art, implémentée avec une interface utilisateur Ajax et developpées selon les préceptes SOA (Architecture Orientée Services - Service Oriented Architecture) de façon à faciliter l'interopérabilité avec d'autres systèmes utilisés par l'organisation
- flexible car- grâce à une application de configuration adaptée - l'administrateur peut progressivement intégrer et contrôler de nouvelles classes d'objets et de nouvelles typologies de relations en ayant en même temps un outil cohérent avec les structures de données modélisées
- personnalisable, car elle est Open Source, si bien que vous pouvez toujours intervenir sur le projet pour intégrer des fonctionnalités manquantes utiles à votre organisation (ou, mieux, selon la philosophie Open Source, chaque utilisateur peut améliorer le projet en rendant disponibles de nouvelles contributions)

Parmi les fonctionnalités particulières de CMDBuild, nous mettrons l'accent sur celles qui suivent:

- configurabilité du système complète et autonome
- mise en évidence des relations entre les fiches de données et avec les instances de processus
- configuration des processus, y compris les processus ITIL
- système pour consulter des tableaux de bord
- système de rapports pour la configuration autonome des impressions
- mécanismes avancés d'interopérabilité avec des applications externes
- système intégré pour la gestion de la documentation
- géoréférencement sur une carte des éléments (GIS)

##Configurabilité

En adoptant une politique permettant de construire graduellement la CMDB implique que le développement du système se fasse étape par étape, en terme de structure et de relation entre objets.

L'utilisateur du système doit gérer de manière autonome de nouveaux types d'objets, sans recourir à des modifications logicielles coûteuses.

La flexibilité de CMDBuild a été recherchée comme standard, permettant à l'administrateur système de définir les modèles de données optimum pour ses besoins opérationnels:

- fiches traitées dans le système ("classes")
- hiérarchie de superclasses et sous-classes
- informations descriptives ("attributs") de chaque classe
- types de relation ("domaines") avec les attributs distinctifs liés ![](../administrator_manual/02-mcd.png)
- liste de valeurs ("lookup") utilisés pour les attributs ayant des valeurs prédéfinis
- utilisateurs, groupes et droits d'accès
- standards de géoréférencement possibles
- menu pour organiser l'accès aux classes, vues et rapports

Le modèle logique défini via un éditeur de schéma Entité-Relations et ensuite configuré dans CMDBuild via le module d'administration.

Chaque modèle de données peut ajouter divers éléments dans les "classes", dont les logiciels, les services, les fournisseurs, les utilisateurs, les éléments réseau, des emplacements, les tickets, les demandes de changement, etc.

Vous pouvez également créer et enregistrer des filtres de recherche, définir des "vues", définir des droits d'accès différenciés selon les classes et limités aux lignes et colonnes d'une classe.

Plus encore, il existe un mécanisme additionnel pour étendre l'orientation initiale de CMDBuild en tant que "système généraliste" en permettant l'intégration de logiques applicatives spécifiques dans les différentes fiches.

De tels mécanismes permet d'utiliser certaines typologies de "Widget", en configurant leur positionnement directement dans les fiches de données communes gérées dans CMDBuild.

Les "Widgets" qui peuvent également être utilisés comprennent:

- impression d'un rapport concernant la fiche courante
- ouvrir le calendrier avec une date disponible dans la fiche courante
- arbre de navigation pour choisir une ou plusieurs fiches via une interface hiérarchique
- lancer un processus
- exécution d'une commande "ping" sur l'IP de la fiche (utile pour les diagnostics)
- affichage et modification d'une fiche d'une classe donnée

Pour les besoins de flexibilité évoqués jusqu'à maintenant, des outils complémentaires ont été implémentés dans CMDBuild de façon à:
- analyser les relations entre les informations
- configurer des processus de façon générique
- configurer divers rapports pour présenter des données renseignées dans la CMDB
- configurer des tableaux de bord pour représenter des indicateurs qui doivent être contrôlés

##Corrélation

Les "domaines" configurés dans le modèle de données de CMDBuild définissent et mettent à jour des relations entre fiches, telles que:

- des éléments avec d'autres éléments (liens hiérarchiques, dépendances fonctionnelles, etc)
- éléments avec les dépositaires, les fournisseurs et les contrats
- éléments avec leurs dépositaires et leur emplacement
- éléments avec les périphériques réseau, le câblage, les points du réseau et les VLAN
- éléments, dépositaires et opérateurs avec les processus d'ouverture de tickets ou les changements de configuration
- éléments, utilisateurs, fournisseurs externes et opérateurs avec les SLA et le catalogue de services

![](../overview/04-Correlation.jpg)

On peut naviguer dans ces corrélations à la fois dans le système de base via le mécanisme de liens entre les fiches et avec une interface de consultation spécifique créée avec la technologie "Flash" et intégrée dans l'application.

##Processus

La valeur ajoutée principale de CMDBuild est la possibilité de définir des processus pour exécuter les activités de gestion de l'infrastructure IT et de garantir en même temps:

- l'assurance d'une mise à jour correcte de la CMDB, avec laquelle il est complètement intégré et corrélé
- une interface standard pour les utilisateurs
- un système support pour un contrôle opératif précis du service
- un dépôt pour les données d'activité, utile pour contrôler les SLA

Comme pour le modèle de données, le système de définition des processus peuvent être configurés à la fois en terme d'informations et de digramme de flux de gestion.

C'est pourquoi CMDBuild ne fournit pas des processus standards implémentés de manière statique, mais un système de configuration générique utilisé selon les besoins spécifiques de chacun des utilisateurs.

En particulier, chaque processus est décrit dans le système en terme de:

- rôles autorisés pour exécuter chaque étape du processus
- séquences d'activités avec branchements conditionnels
- données visibles, demandées à l'utilisateur pour compléter chaque étape
- tâches réalisées (démarrer le processus, mise à jour de la base de données, envoyer des mails, etc.)

![](../overview/04-Workflow.jpg)

Les processus sont conçus en utilisant des éditeurs graphiques externes Open Source (Together Workflow Editor) puis sont importés dans CMDBuild et exécutés avec le moteur de Together Workflow Server.

Ces mécanismes de base permettent une configuration conforme à ITIL de tous les processus fournis par les "bonnes pratiques" ITIL, dont la gestion des incidents, la gestion des changements, la réalisation des demandes, le catalogue de services, etc.

D'autres processus, utiles pour la gestion des IT, peuvent concerner la gestion des éléments (gestion du cycle de vie des éléments via des opérations d'acquisition, de débogage, affectation, transfert, retrait, cessation, etc), la gestion des câbles réseau (connection des câbles / déconnexion aux ports de l'appareil), la gestion des adresses IP (ajout d'interface réseau / retrait), etc.

##Gestion de la documentation

La documentation est également pour ITIL un composant d'information important lié aux éléments traités dans la CMDB: manuels et documentation technique, contrats, ensemble de formulaires, captures d'écran des erreurs, schémas techniques, images des objets et emplacements, etc.

CMDBuild utilise un système de gestion Alfresco comme dépôt embarqué, marque leader parmi les solutions Open Source.

![](../overview/04-Document_management.jpg)

Alfresco permet d'ajouter des documents, les classer en catégories et les renvoyer à CMDBuild.

Ils peuvent être vues comme des pièces jointes à la fiche actuelle.

Cependant, si vous voulez utiliser l'interface d'Alfrescon vous y trouverez les mêmes documents que ceux disponibles dans CMDBuild, situés dans un "espace" spécifique du dépôt, mais avec des noms qui sont des identifiants univoques de la CMDB.

Les deux systèmes communiquent l'un avec l'autre en utilisant:

- le protocole SOAP, pour le peuplement des fichiers téléchargés dans Alfresco et pour les spécifications des standards de recherche pour consulter des fichiers
- le protocole FTP pour un transfert de fichiers plus efficace entre les systèmes

##Tableau de bord

Il est important que les personnes en charge de la gestion des services IT puissent immédiatement consulter la tendance des indicateurs principaux (KPI - Key Performance Indicator ou Indicateur Clef de Performance) que le système réalise en analysant les données de la CMDB (fiches et processus).

Pour ceci, nous fournissons la possibilité de définir diverses pages "tableau de bord", chacune dédiée à des aspects différents (situation des éléments, performance du service d'assistance, éléments de revenus, etc) et contenant divers graphiques configurés selon les services IT.

![](../overview/04-Dashboard.jpg)

L'administrateur système peut configurer chaque graphique en créant les fonctions de calcul dans la base de données et en définissant les aspects de présentation via le module d'administration.

Le système de tableaux de bord ne remplace pas celui des rapports, puisqu'il fournit des informations plus et détaillées ou des constructions statistiques relatives à des périodes de temps moyennnes ou longues.

##Rapports

Une gestion efficace de l'information nécessite la disponibilité de divers rapports sur les données insérées dans le système: synthèses, analyse, statistiques avec divers graphiques.

![](../overview/04-Reports.jpg)

Des rapports tabulaires simples peuvent être créés avec un assistant du système et renseignés dans CMDBuild, puis imprimés avec des données mises à jour et - si nécessaire - modifiés dans la définition.

Pour la conception de rapports avancés (mise en forme, lots, décompte, création des graphiques, etc.), vous pouvez utiliser l'éditeur graphique externe IReport faisant partie de la suite Open Source JasperReport. Les rapports configurés de cette façon sont ensuite importés dans CMDBuild et fournis aux différents utilisateurs.

IReport peut concevoir des rapports pour créer:

- impressions et listings
- lettres et documents, intégrant des informations enregistrées dans CMDBuild
- impression avec des statistiques et divers graphiques
- codes barre / qrcode

De plus, vous pouvez également créer des rapports pour toute période, grâce à la fonctionnalité de "versionnage" complète de CMDBuild.

##Gestionnaire de tâches

CMDBuild comporte un gestionnaire de tâches qui peut être configuré dans le module d'administration via une interface graphique. Il comporte également diverses opérations exécutées en tâche de fond. 
![](../overview/04-Task_manager.jpg)

En particulier, il permet de:

- recevoir des emails sur des comptes / avec des modèles prédéfinis et les enregistrer dans le système
- démarrer des processus
- exécuter un connecteur
- contrôler si des événements synchrones (opérations sur les fiches de données) se produisent dans la CMDB
- contrôler su des événements asynchrone se produisent dans la CMDB (valeurs de seuil prédéfinies, par exemple dépassement du SLA ou d'une échéance d'une licence)

Si une telle situation se produit, le gestionnaire de tâches peut effectuer des notifications par email, lancer des processus et exécuter des scripts.

##Outils de géoréférencement

Dans les activités de gestion IT, nous vous suggérons de connaître la localisation exacte des éléments et de pouvoir chercher ces informations également sur des cartes (localisation géographique).

CMDBuild fournit des fonctionnalités pour la gestion de la représentation GIS, à la fois locales (planimétries) et liée à des régions plus étendues (cartes externes).

![](../overview/04-Georef-1.jpg)

Dans le premier cas, vous pouvez modéliser le détail des pièces individuelles et - si nécessaire - l'emplacement des éléments dans les bureaux ou les matériels dans les pièces communes (dans l'exemple, des éléments d'information, mais également des équipes, de l'ameublement, des oeuvres artistiques, des équipements, etc.).

A la fois les périmètres des pièces et l'emplacement des éléments peuvent être mis à jour directement à partir du navigateur web, en accédant en modification aux fiches de données liées.

![](../overview/04-Georef-2.jpg)

Dans le second cas, vous pouvez afficher les informations réparties sur le territoire, par exemple les éléments positionnés dans les différentes compagnies (dans l'exemple, les feux rouges dans les zones de trafic contrôlées, les points d'information, etc.)

Vous pouvez utiliser divers services de carte (OpenStreetMap, GoogleMaps, etc.) et enregistrer tout fichier au format "shape".

CMDBuild est également une solution de gestion des éléments capable de fournir les services requis par une infrastructure BIM d'entreprise (Building Information Modeling ou Modélisation des informations des bâtiments).

Un élément inévitable d'un système BIM est la gestion complémentaire et intégrée des textes, des informations sur les documents et les graphiques, processus en synergie, des fonctions des rapports et des solutions d'interopérabilité - toutes ces fonctionnalités sont pleinement et extensivement gérées par CMDBuild.

![](../overview/04-Georef-3.jpg)

BIM est une solution conceptuelle et technologique ayant pour but de prendre en charge la procédure de conception des bâtiments, depuis la conception du bâtiment jusqu'à sa construction, son utilisation, sa maintenance, et enfin sa démolition, s'il y a lieu.

Les solutions BIM sont devenues très importantes tant pour leurs grands avantages économiques sur les processus de base (grosses économies pour le processus complet), que pour leurs très grandes capacités à gérer d'autres besoins essentiels tels que le contrôle de l'énergie, la sécurité, etc.

Grâce aux fonctions spécifiques du format standardisé ouvert IFC (Industry Fondation Classes), on peut automatiquement synchroniser dans les deux sens les informations gérées avec le logiciel de conception 3D des bâtiments utilisant la base de données CMDBuild:

- en important le fichier IFC (ISO 16739:2013) produit par l'outil de conception externe
- en mettant à jour dans la base de données de CMDBuild les éléments d'infrastructure (pièces, murs, portes et fenêtres, etc.) qui sont dans le modèle importé basé dans des règles de mappage configurables
- en exportant le fichier IFC enrichi avec les données traitées dans openMAINT (éléments mobiles et installations)

Un autre élément qui prend en charge BIM est la visionneuse interactive pour les modèles 3D, intégrée dans sa propre interface utilisateur standard. Elle vous permet de consulter de façon réaliste l'intérieur d'un bâtiment et ses éléments.

![](../overview/04-Georef-4.jpg)

Les fonctions BIM sont implémentées avec l'aide du produit Open Source BIMServer.

Le département de mathématiques et d'informatique de l'université d'Udine ont coopéré à sa création.

##Solutions d'interopérabilité

ITIL v.3 étend le concept d'une CMDB unique et monolithique en vue des situations avec différentes CMDB interopérantes.

Dans ce but, CMDBuild implémente diverses solutions d'interopérabilité utilisées avec une architecture orientée services:

- services web SOAP natifs qui expriment toutes les méthodes primitives pour la gestion des données, les pièces jointes et les processus
- services web REST natifs
- web services SOAP, compatibles avec les standards de CMDB

Les outils mentionnés ci-dessus sont disponibles si vous voulez créer des mécanismes de communication entre CMDBuild et d'autres applications externes.

Le même système CMDBuild utilise des propres services web SOAP et REST pour implémenter certaines solutions standard d'interopérabilité:

- connecteurs avec des systèmes externes (connecteurs de base et connecteur avancé)
- Portlet CMDBuild - Liferay
- Framework d'IHM
- Interface mobile

##Connecteurs avec des systèmes externes

La gestion des services IT par des institutions et des compagnies de moyenne et grande taille est nécessairement réalisée à l'aide de systèmes d'information plus spécialisés qui doivent coopérer pour la gestion de leurs activités.

En collectant et en contrôlant manuellement les informations gérées dans la CMDB peut conduire à des problèmes de retard ou créer des incohérences lors de la mise à jour des données. C'est pourquoi  il est mieux de les mettre à jour automatiquement.

C'est pour cette raison que la configuration des connecteurs ETL via des systèmes externes est importante de façon à synchroniser dans CMDBuild (le système central de CMDB) les données qui sont principalement gérées ("maître") par d'autres applications spécialisées, parmi lesquelles:

- le répertoire LDAP e, tant que dépôt pour lister les équipes
- les systèmes RH comme solution alternative pour la liste des équipes ou les systèmes ERP pour les données administratives (sources, fournisseurs, etc.)
- systèmes d'inventaire automatisé pour automatiquement comparer les données techniques des éléments et pour gérer les différences saillantes avec une possible activation du processus de gestion des changements (cela peut être OCS Inventory mais d'autres produits peuvent également être liés) ![](../overview/04-Connectors.jpg)
- systèmes de virtualisation pour extraire et comparer la configuration actuelle des serveurs virtuels (pae exemple VCenter pour VMware)
- services s'exécutant sur les serveurs (bases de données, serveurs d'application, etc.) pour l'extraction des informations concernant la configuration actuelle de l'infrastructure IT
- Métamodèles du contenu de l'architecture IT (tels que Archi avec TOGAF, grâce au plugin développé par des contributeurs externes au projet)

A côté des besoins de sycnhronisation des données, l'interaction avec des systèmes externes peut être utile pour obtenir des informations en temps réel et les gérer de la manière appropriée (par exemple en obtenant des alarmes des systèmes de supervision et en exécutant des analyse d'impact et/ou en activant le processus de gestion des incidents).

Dans ce but, CMDBuild fournit un connecteur de base qui peut être configuré via le langage de transformation XSLT. Il fonctionne à l'égard du système externe via un accès direct à la base de données et à l'égard se sa CMDB en utilisant les services web SOAP.

De plus, un connecteur avancé est disponible (avec le code source mais une licence différente qui n'autorise son utilisation que par un service activé de maintenance annuelle). Il est basé sur un framework qui peut être paramétré via le langage de script Groovy capable de fonctionner à la fois via les services web et via un accès direct à la base de données, la lecture de fichiers ou la réception d'email.

Dans le cas d'un besoin d'une synchronisation moins complexe (mappage direct entre les entités qui doivent être mises à jour), vous pouvez également utiliser l'assistant de connexion qui peut être configuré à partir de l'IHM du module d'administration de CMDBuild.

##Portlet CMDBuild - Liferay

CMDBuild fournit une solution standard pour déporter certaines fonctionnalités sous forme de Portlet (standard JSR 268) dans le portail Open Source Liferay.

La portlet est capable d'accéder à CMDBuild via les services web SOAP, en les adaptant à la configuration de l'instance liée (menu, droits d'accès, structure de la fiche de données, flux des processus, rapports).

![](../overview/04-Portlet.jpg)

La portlet comporte le démarrage et l'avancement d'un procesus, avec la consultation des instances actives ou des instances achevées, de la gestion des fiches de données, (création, modification, annulation) et exécution de rapport.

L'utilisation d'un portail tel que Liferay vous permet d'utiliser d'autres outils diponibles pour:

- collaborer (wiki, forum, blog, messagerie, etc.)
- gérer le contenu (news, événements, pages d'information)
- des interfaces avancées fourni par une tierce partie via le même standard JSR 168.

Cette solution est très avantageuse car elle peut s'adapter à l'instance liée de CMDBuild sans développer de code pour la gestion des interactions entre les deux environnements.
Sa limite est justement son "auto adaptabilité" qui ne lui permet pas tout type de personnalisation.

##Framework d'IHM

Le framework d'IHM rend disponible une interface simplifiée pour les équipes non techniques.
A la différence de la portlet CMDBuild - Liferay, le framework d'IHM peut être activé dans tout portail ou site web et permet une personnalisation d'interface complète, à la fois fonctionnelle et graphique.

D'un autre côté, la configuration du framework d'IHM nécessite une personnalisation du code JavaScript qui est simplifiée si les templates et les bibliothèques sont disponibles.

En particulier, l'outil offre les fonctionnalités suivantes:

- il peut être activé dans des portails basées sur des technologies différentes puisqu'il est développé dans un environnement JavaScript/JQuery
- il offre une liberté (presque) totale dans la conception de l'agencement graphique, défini via un descripteur XML et avec la possibilité d'intervenir sur le CSS
- il garantit une configuration rapide grâce aux fonctions prédéfinies (communication, logiques d'authentification, etc.) et aux solutions graphiques natives (formulaires, grilles, boutons d'import et autres widgets)
- il interagit avec CMDBuild via des services web REST

##Interface mobile

Il existe plein d'opérations qui peuvent être menées directement sur le terrain (livraison / retrait d'ordinateurs et d'appareils, inventaires, ect.). Vous pouvez également les enregistrer quand elles sont réalisées, évitant des délais et en faisant usage de fonctionnalités spécifiques, i.e. envoyer des images des éléments ou en scannant un QR code pour recevoir du serveur la fiche de l'élément.

![](../overview/04-Mobile.jpg)

Dans ce but, une nouvelle interface pour CMDBuild a été créée pour les smartphones et les tablettes. C'est une application pour les appareils mobiles créée avec Sencha Touch (un framework JavaScript développé par Sencha, le même qui a produit le framework Ext JS utilisé par l'application de bureaude CMDBuild) et capable d'interagir avec CMDBuild via les services web REST.

CMDBuild mobile implémente les principales fonctionnalités de l'interface du bureau: connexion en plusieurs langues et pour plusieurs groupes, menu de navigation, gestion des classes avec les relations et les pièces jointes, les recherches et les filtres, gestion des processus avec les widgets les plus utilisés, gestion des rapports, utilitaires (réglages de l'application et gestion des journaux).

La nouvelle interface mobile est rendu disponible, avec une licence non Open Source, à tout subscripteur des services de gestion (avec un surcoût additionnel limité).
