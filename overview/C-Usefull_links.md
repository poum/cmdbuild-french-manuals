#Liens utiles

##Réunions et ateliers de travail

###SALPA 2005

|[Présentation de CMDBuild à la SALPA - National Conference on Open Source in Public Administration) à Pise](www.salpa.pisa.it/salpa/22/02/36/SALPA_ATTACH_FILE220236.pdf)

###Conférence annuelle de l'itSMF Italie (2005)

![La municipalité d'Udine présente son projet de réorganisation de ses processus IT internes avec l'aide d'ITIL et de CMDBuild](www.itsmf.it/download/GRUPPO01~ATTI_CONFERENZA_MILANO_2005/itSMF_ITIL_e_PA_Scaramuzzi.pdf)

###COMPA 2006

![Présentation de CMDBuild à la COMPA 2006 (European Exhibition of Public Communication and Services) à Bologne](http://www.cmdbuild.org/file/cmdbuild_compa_2006.pdf)

###Conférence annuelle de l'itSMF Italie (2006)

![La municipalité d'Udine présente l'avancement de ses projets de réorganisation de ses processus internes IT à l'aide d'ITIL et de CMDBuild](http://www.cmdbuild.org/file/slide_itsmf_comuneudine.pdf)

###Think Open 2007

La municipalité d'Udine - avec les partenaires du projet - présente CMDBuild lors de la première édition de
Think Open, événement qui s'est tenu à Rovigo du 26 au 28 septembre 2007.
Cliquer sur les liens suivants pour voir les planches des présentations:
![](http://www.cmdbuild.org/filesystem/Slide_ThinkOpen2007_Master_R03.pdf)
![](http://www.cmdbuild.org/filesystem/Slide_ThinkOpen2007_Technical_R03.pdf)

###Journée Linux 2007

Tecnoteca présente CMDBuild à la journée Linux 2007 dans la programmation organisée par IGLU
(Linux Users' Group d'Udine) dans les locaux de l'université d'Udine.
Cliquer sur les liens suivants pour voir les planches des présentations:
![](http://www.cmdbuild.org/filesystem/Slide_LinuxDay2007_Presentazione.pdf)
![](http://www.cmdbuild.org/filesystem/Slide_LinuxDay2007_ApprofondimentoTecnico.pdf)

###PLIO 2008

CMDBuild était l'un des projets présentés lors de l'événement "Il software libero in Friuli Venezia
Giulia" (Logiciel libre à FVG), qui s'est tenu dans le Palazzo della Regione d'Udine et qui était organisé par 
l'Associazione PLIO (OpenOffice.org Italian National Language Project).
Cliquer sur les liens suivants pour voir les planches des présentations:
![](http://flossfvg.plio.it/atti-del-convegno/Slide_EventoPlioFVG_R02.pdf/)

###ITIL at Work 2008

Cogitek complètement dédié à CMDBuild lors de la réunion organisée à Milau à l'Atahotel Executive.
Cliquer sur les liens suivants pour voir les planches des présentations:
![](://www.cmdbuild.org/file/ITILatWORK_Tecnoteca.pdf)
![](://www.cmdbuild.org/file/ITILatWORK_ComuneUdine.pdf)

###itSMF, printemps 2008

Nous avons parlé de CMDBuild à Rome et Udine, lors de deux événements organisés en mail par l'itSMF.
Les planches de la présention du service de conseil juridique de l'État:
![](://www.itsmf.it/download/PERSONAL4~EVENTO_PRIMAVERA08/AVVOCATURA_SITO.pdf)
Les planches de la présentation de la municipalité d'Udine:
![](://www.cmdbuild.org/file/Antonio_Scaramuzzi_itSMF_Udine.pdf)

###itSMF 2008

Lors de la cinquème conférence annuelle de l'itSMF Italie, CMDBuild a été montré au stand de Cogitek.
Lors de la conférence, le conseil régional de Toscane à présenter les bonnes pratiques ITIL prises en charge par CMDBuild.
Cliquer sur le lien suivant pour voir les planches de la présentation:
![](://www.cmdbuild.org/file/03-_regione_toscana.pdf)

###CLUB TI TRIVENETO 2009

Lors d'un atelier de travail dédié à la gouvernance IT, CMDBuild a été présenté dans deux exposés par
Tecnoteca et la municipalité d'Udine.
Cliquez sur les liens suivants pour voir les planches des présentations:
![](://www.cmdbuild.org/file/Slide_ClubTI2009.pdf)
![](://www.cmdbuild.org/file/Slide_ClubTI_2009-03-11_Scaramuzzi.pdf)

###ITIL & PMBOK 2009

CMDBuild a été présenté lors de l'événement ""ITIL e PMBOK, Gestion des services et gestion des projets confrontés"
qui s'est tenu à Florence, le 1er juillet 2009 sous les auspices du conseil régional de Toscane.
Cliquer sur les liens suivants pour voir les planches des présentations:
![](://www.cmdbuild.org/file/itil-e-pmbok)
![](://www.cmdbuild.org/file/pmbok-iv-e-itil-v.3)

###CMDBUILDAY 2010

La première conventions des utilisateurs de CMDBuild s'est tenue à Udine le 15 avril 2010.
La rencontre était organisée dans le bit de diffuser et de partager des expériences autour du projet;
elle a été réalisée par les rapporteurs des principales compagnies utilisatrices.
Cliquer sur le lien suivant pour voir les planches et les vidéos de tous les exposés:
![](://www.cmdbuild.org/it/diffusione/cmdbuild-day/2010)

###IT CLUB 2011
IT Club FVG et le "Centro di Competenza Open Source" de Ditedi (Digital Technology District)
ont orgainsé une réunion sur les logiciels Open Source sous les auspices de la 
Confindustria of Udine le 23 février 2009.
Lors de la réunion, nous avons présenté notre expérience de CMDBuild.
Cliquer sur le lien suivant pour voir les planches des présentations:
![](://www.cmdbuild.org/diffusione/convegni-e-workshop/itclub-2011)

###Roma città... open (“Rome, cité ... libre ”), 2011

Cette réunion a été organisée à Rome le 8 avril par Yacme Srl, Tecnoteca Srl et Seacom Srl,
dans le but de présenter le logiciel open source via la présentation de certains exemples 
d'importants succès.
CMDBuild était l'un des 3 projets présentés lors de cet événement, en présence du
service de conseil juridique de l'état qui présentait l'événement.
Cliquer sur le lien suivant pour voir les planches et les vidéos de la présentation:
![](://www.cmdbuild.org/diffusione/convegni-e-workshop/8-aprile-2011-roma-citta...-open)

###Al servizio del Cittadino (“Au service des citoyens”), 2011

L'événement organisé le 19 mai 2011 par la municipalité de Tavagnacco et en coopération avec
l'université d'Udine, a présenté aux PAs et aux citoyens certaines solutions adoptées par la 
municipalité elle-même et basées sur des solutions Open Source.
Parmi elles, sous les auspices des services sociaux, nous avons présenté l'utilisation de CMDBuild
pour gérer le retour des contributions aux citoyens. Le conseiller qualifié a introduit le projet.
Cliquer sur le lien suivant pour voir les planches et les vidéos de la présentation:
![](://www.cmdbuild.org/diffusione/convegni-e-workshop/al-servizio-del-cittadino-2011)

###CMDBuild chez TIS à Bolzano 2011

Le 31 mai 2011, Mynt et Tecnoteca ont organisé un événement dédié à CMDBuild, qui s'est tenu
sous les auspices de TIS Innovation Park à Bolzano.
Le but de la réunion était de nourrir l'esprit pour gérer les éléments conformément aux guides de conduite ITIL,
en prenant également en considération l'Open Data.
Cliquer sur le lien suivant pour voir les planches des présentations:
![](://www.cmdbuild.org/diffusione/convegni-e-workshop/evento-cmdbuild-bolzano)

###CMDBUILDAY 2012

La deuxième convention des utilisateurs de CMDBuild s'est tenu à Udine le 10 mai 2012.
La réunion était organisée dans le but de diffuser et de partager les expériences concernant
le projet; elle était menée par les porte-parole des principales compagnies utilisatrices.
Cliquer sur le lien suivant pour voir les planches et les vidéos de toutes les interventions:
![](://www.cmdbuild.org/it/diffusione/cmdbuild-day/201 2)

###TOGAF, Archi et CMDBuild

"Tool open source per le organizzazioni che hanno adottato TOGAF: Archi e CMDBuild" (outils Open
source pour les organisations qui utilisent TOGAF: Archi et CMDBuild) est le titre de la réunion qui 
se sera tenu le 28 mai 2014 à Florence dans la Sala delle Feste du Palazzo Bastogi (via Cavour 18),
fourni par le conseil régional de Toscane.
TOGAF est un framework ouvert qui gère la conception et la gestion des architectures logicielles d'entreprise
tandis qu'Archi est l'outil open source de référence pour concevoir des modèles conformes à TOGAF.
Nous avions besoin d'un dépôt Open Source pour enrôler les éléments formés via Archi. CMDBuild peut
maintenant être utilisé dans ce but grâce au "plugin" développé en coopération avec l'université
de Bologne et avec SIAE qui fournit une synchronisation bidirectionnelle complète avec Archi.
Les planches et les vidéos de toutes les présentations sont disponibles via le lien suivant:
![](://www.cmdbuild.org/it/diffusione/convegni-e-workshop/togaf-archi-e-cmdbuild)

###Open Source pour de vrai !

Le 26 février 2014, une réunion organisée par SUSE s'est tenue à Rome. Elle était dédiée à
l'écosystème Open Source.
Lors de cette réunion, SUSE a présenté trois produits choisis dans les domaines des bases
de données, des middlewares d'entreprise et des CMDB. CMDBuild est la solution de CMDB choisie par SUSE.
Cliquer sur le lien suivant pour voir les planches et les vidéos de l'intervention de Tecnoteca:
![](://www.cmdbuild.org/it/diffusione/convegni-e-workshop/open-source-for-real)

###CMDBUILDAY 2014

La troisième convention des utilisateurs de CMDBuild s'est tenue à Rome le 15 mai 2014.
La réunion était organisée dans le bit de diffuser et de partager des expériences concernant
le projet: elle a été menée par les porte-parole des principales compagnies utilisatrices.
cliquer sur le lien suivant pour voir toutes les planches et les vidéos de toutes les présentations:
![](://www.cmdbuild.org/it/diffusione/cmdbuild-day)

##Article de presse

###ALTRA PA

Dans les liens suivants, vous pourrez trouver le projet désigné parmi les meilleures pratiques d'ALTRA PA,
décrivant en détail le projet et interviewant le directeur IT de la municipalité d'Udine (son client):
![](http://www.forumpa.it/archivio/3000/3800/3820/3827/cmdbuild-veloci.html)
![](http://www.forumpa.it/archivio/3000/3800/3810/3818/scaramuzzi-veloci.html)

###FORUM PA

La personne contactée du CED au service du conseil juridique de l'état à Rome décrit sur le site
web du Forum PA les activités réalisées pour introduire la gestion des processus dans le service
fourni pour les utilisateurs de l'institut:
![](http://www.innovatori.forumpa.it/innovatore.php?id=135)

###DATA MANAGER

CMDBuild a été mentionné dans la version en ligne du magazine.
L'interview du mainteneur du projet est disponible en ligne:
![](http://www.datamanager.it/cms/view/sezioni_web/open_source/cmdbuild_la_soluzione_open_source_per_il_configuration_management_data_base/s152/c78432)

###CNIPA

CMDBuild a été mentionné sur le portail du CNIPa.
L'interview du promoteur du projet est disponible en ligne:
![](http://www.osspa.cnipa.it/home/index.php?option=com_content&task=view&id=77&Itemid=36)

#Portail de la Commission européeene OSOR

L'interview sur le portail CNIPa est disponible sur le porail QSQR de la Commission européenne:
![](http://www.osor.eu/news/it-open-source-it-system-management-software-is-mature)

###EGOV

Dans la version en ligne du magazine pour l'administration publique, vous pouvez trouver une présentation de CMDBuild lors
de la journée CMDBuild 2012:
![](http://www.egovnews.it/evento/1586/)
![](http://www.egov.maggioli.it/testo-news/24004)

##Composants Open Source

###PostgreSQL (base de données)
![Site web de référence](www.postgresql.org/)

Tomcat et Apache (serveur web)
![Site web de référence](www.apache.org/)

Alfresco (dépôt de documentation)
![Site web de référence](www.alfresco.com/)

JasperReports (rapports)
![Site web de référence](http://www.jasperforge.org/)

Enhydra Shark (moteur de processus)
![Site web de référence](www.enhydra.org/workflow/shark/)

TWE (éditeur de processus)
![Site web de référence](http://www.together.at/prod/workflow/twe)

OCS Inventory (Inventaire automatisé)
![Site web de référence](www.ocsinventory-ng.org/)

Ext Js (Bibliothèque client Ajax)
![Site web de référence](http://extjs.com/)

Quartz (planificateur de jobs)
![Site web de référence](http://quartz-scheduler.org/)

Liferay (portail)
![Site web de référence](http://www.liferay.com/)

GeoServer (GIS côté serveur)
![Site web de référence](http://geoserver.org/)

OpenLayers (GIS côté client)
![Site web de référence](http://openlayers.org/)

BiMserver (dépôt pour les modèles IFC)
![Site web de référence](http://bimserver.org/)

##Références ITIL

###Site web officiel ITIL 
![Site web de référence](http://www.itil.co.uk/)

###ITIL IT Service Management Zone
![Site web de référence](http://www.itil.org.uk)

###Association italienne de l'itSMF

L'assocation "information technology Service Management Forum Italia" est une association à but non lucratif
pour promouvoir et exchanger des expériences concernant la gestion des services ICT et l'adoption
des bonnes pratiques suivant les directives ITIL
![Site web de référence](http://www.itsmf.it)

