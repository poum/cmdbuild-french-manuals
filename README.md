# README #

This repository hosts my work translating the CMDBuild manuals in French.

Ce dépôt héberge mon travail de traduction des manuels CMDBuild en français.

### What is this repository for? 

* CMDBuild manual french translation 
* Version 2.3.0

### Contribution guidelines 

Feel free to pull typo fix, translation improvments or translation for untranslated parts.
Of course, all contributors will be named unless explicly asked.

### Translation guide

- asset: actif 
- GUI: IHM
- process: processus (traitement ?)
- workflow: processus
- card: fiche
- relation: relation
- lookup: liste de valeurs

### Extracting text and images

    pdftotext -layout CMDBuild*.pdf source.txt
    pdfimages -j CMDBuild*.pdf image
    for i in *.ppm; do convert $i `basename $i '.ppm'`.jpg; done

### Building 

The translated doc could be build with pandoc.
The process is simplified thanks to Makefile:

    make all

The resulting file is in build directory.

### Keeping up to date

Just extract current and new pdf manual versions using pdftotext
and use diff to quickly identify changing parts.

Read the corresponding paragraph in pdf version to see any image modification

### Roadmap

- overview (translation completed, control reading to do)
- user manual (translation completed, todo: french screenshot and control reading)
- administrator manual (translation completed, todo: french screenshot and control reading)
- technical manual (in progress)
- webservices manual (in progress)
- workflow manual (in progress)
- create title page
- improve Makefile
