#Description du système de processus

##Informations générales

Da façon à prendre en charge les indications méthodologiques ITIL, CMDBuild est non seulement capable de gérer la mise à jour de l'inventaire des actifs et de leurs relations fonctionnelles mais aussi de permettre la définition et le contrôle des processus destinés à la gestion des services IT.

Un processus comprend une séquence d'activités, menées par des opérateurs et/ou des applications informatiques, chaque application représente une opération qui doit être prise en charge dans le processus en relation, dans ce cas, à la gestion des actifs IT et aux critères de qualité.

Etant donné le nombre d'options des processus, les procédures organisationnelles et la flexibilité recherchée par le projet CMDBuild, nous avons choisi de ne pas implémenter une série de processus rigides et prédéfinis, mais un moteur de processus générique pour modéliser les processus au cas par cas.

Dans la première partie de ce document, vous trouverez les concepts généraux et les mécanismes de base implémentés dans le système avec le remaniement de CMDBuild 2.0.

Dans la seconde partie se trouve un exemple de processus simplifié, dont les étapes de configuration sont décrites.

Dans la troisième partie, vous trouverez les outils techniques disponibles pour la configuration d'un processus: définition de widgets, descrition des fonctions de l'API qui peuvent être utilisées dans les scripts pour la définition des automatismes exécutés dans le processus.

Dans l'annexe, vous trouverez la documentation technique spécifique au système de processus utilisé jusqu'à CMDBuild 1.5, dont la compatibilité est encore maintenue dans CMDBuild 2.0; il sera retiré dès que possible.

##Objectifs

Le système de gestion des processus est une fonctionnalité importante de CMDBuild qui fournit:

- une interface standardisée pour les utilisateurs
- une mise à jour sécurisée de la CMDB
- un outil pour surveiller les services fournis
- un dépôt pour les données d'activité, utile pour contrôler les SLA

Les processus ITIL qui peuvent être configurés dans CMDBuild comprennent: la gestion des incidents, la gestion des problèmes, la gestion des changements, la gestion de configuration, la gestion du catalogue de service, etc.

D'autres types de processus concernent le mouvement des actifs, l'arrivée de nouvelles équipes, l'activation de nouveaux projets de travail, etc.

##Outils utilisés

L'application choisie pour la gestion des processus utilise les outils suivants:

- XPDL 2.0 (http://www.wfmc.org/xpdl.html) comme langage de définition (standardisé par la WfMC, la coalition de gestion des procussus ou Workflow Management Coalition conformément au modèle suivant)
- le moteur open source TWS Together Workflow Server 4.4 (http://www.together.at/prod/workflow/tws), un framework extensible pour une implémentation complète et standard des spécificités de WfMC (http://www.wfmc.ord/) et d'OMG, utilisant le XPDL comme langage natif
- l'éditeur graphique TWE Together Workflow Editor 4.4 (http://www.together.at/prod/workflow/twe) pour la conception des processus et la définition des mécanismes d'intégration avec CMDBuild.

Le schéma suivant montre la gestion des processus conformément au modèle standardisé par la WfMC.

![](../workflow_manual/02-Used_tools.jpg)

##Terminologie

Le "vocabulaire" suivant comporte les termes suivants:

- processus (process): séquence d'étapes qui réalisent une action
- activité (activity): étape de processus
- instance de processus: un processus actif créé en en exécutant la première étape
- instance d'activité: création d'une activité accomplie automatiquement ou par un opérateur 

Les termes précédents sont organisés dans CMDBuild de la manière suivante:

- chaque processus est lié à une classe spéciale définie par le module d'administration dans le menu "Processus"; la classe comprend tous les attributs des activités programmées
- chaque "instance de processus" correspond à une fiche de la classe "processus" (activité courante), combinée avec la liste de ses versions (activités terminées)
- chaque instance d'activité correspond à une fiche de la classe "processus" (activité courante) ou à une version historisée (activités achevées)

Chaque processus possède un nom, un ou plusieurs participants, quelques variables et une séquence d'activités et de transitions qui sont gérés par lui.

L'état du processus peut être:

- "actif", i.e. il est encore dans une activité intermédiaire
- "terminé", i.e. il a terminé ses activités
- "abandonné", i.e. terminéde manière anormale
- "suspendu", i.e. maintenu seulement pour la compatibilité ascendante avec le système de processus en vigueur jusqu'à la version 1.5 de CMDBuild

Chaque activité peut être caractérisée par:

- un nom
- un exécuteur qui correspond obligatoirement à un "groupe d'utilisateurs" et éventuellement à un opérateur
- un type: démarrage de procesus, fin de processus, activité réalisée par un opérateur, activité automatiquement gérée par le système
- tous les attributs venant de CMDBuild ou du processus, qui sont définis lors de son implémentation
- tous les widgets (contrôles visuels de certaines typologies prédéfinies) qui seront activés lors de son implémentation
- un script (dans le langage Beanshell, Groovy ou Javascript) fourni dans les activités automatiques via lesquels les opérations entre une activité utilisateur et la suivante peuvent être menées à bien

##Remaniement pour la version 2.0

Avec la version 2.0, nous avons revu le système des processus avec la montée en version de Together Workflow Server 4.4, l'adoption du standard 2.0 d'XPDL, la prise en charge complète dans CMDBuild du parallélisme natif du flux et des améliorations de performance importantes.

De façon à simplifier l'écriture, nous avons décidé de fournir un mode de définition différent des activités automatiques, en gérant l'écriture de scripts et en excluant l'utilisation d'"outils" disponibles dans les versions précédentes de CMDBuild.

Les scripts peuvent être écrits en langage Beanshell, Groovy ou JavaScript et peuvent utiliser les fonctions de l'API fournies pour la définition des automatismes (manipulations des variables de processus, création de fiches et de relations dans la CMDB, envoi d'emails, génération de rapports, etc.).

L'adoption d'un nouveau système de processus implique la perte de la rétro compatibilité avec les processus développés jusqu'alors.

De façon à garantir une plus longue période de migration des anciens processus vers les nouvelles solutions adoptées, nous avons décidé de maintenir dans CMDBuild 2.0 la possibilité de travailler - alternativement - à la fois avec Together Workflow Server 2.3 (la version utilisée jusqu'à CMDBuild 1.5 et reposant sur XPDL 1.0) et avec la nouvelle version 4.4 de Together Workflow Server (reposant sur XPDL 2.0).

Donc la solution adoptée permet:

- aux nouveaux utilisateurs de CMDBuild de travailler avec le nouveau Together Workflow Server 4.4 et avec les nouvelles fonctionnalités développées dans la version 2.0 (parallélisme natif, automatismes configurés via des scripts)
- aux anciens utilisateurs de diviser la migration en deux étapes:
    1. activer la version 2.0 immédiatement pour utiliser les nouveaux tableaux de bord et d'autres fonctionnalités implémentées, tout en maintenant actif Together Workflow Server 2.3 (avec des performances améliorées)
    2. de basculer vers Together Workflow Server 4.4 juste après le test du nouvel environnement sur l'instance de test, quand l'outil de conversion est disponible.

L'outil support pour la migration des processus qui a été développé avec les versions précédentes de CMDBuild sera ensuite livré.

Il est recommandé d'effectuer la migration sur une courte période de temps puis la double compatibilité de CMDBuild avec Together Workflow Server 2.3 (XPDL 1.0) et Together Workflow Server 4.4 (XPDL 2.0) ne sera maintenue que pour une durée limitée.

