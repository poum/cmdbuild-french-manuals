#Exemple de configuration d'un nouveau processus

##Informations générales

Le processus choisi qui décrit les différentes étapes nécessaires à sa configuration est un processus simplifié de demande de modification (RfC pour Request for Change).

C'est un processus extrêmement simplifié, modéliser uniquement à des fins pédagogiques; il est proposé dans le but de montrer les modalités de configuration, pas pour une utilisation réelle en production.

Le processus d'exemple complété avec la définition dans CMDBuild et le flux XPDL conçu avec TWE est disponible dans la base de données de démonstration fournie avec CMDBuild.

##Description du processus de RfC utilisé comme exemple

Les acteurs du processus sont les groupes utilisateurs:

- Service d'assistance qui réalisent l'enregistrement initial de la demande faite par un utilisateur
- Les gestionnaires de changement responsables des modifications des actifs de la compagnie
- Les experts IT, impliqués dans la production des documents d'analyse et pour l'exécution des modifications.

Voici un schéma logique du processus:

![](../workflow_manual/05-RfC.jpg)

Le processus comprend les opérations suivantes:

- enregistrement de la demande de changement
- évaluation des aspects formels de la demande:
    - fermeture directe si la demande n'est pas acceptable
    - passage à l'étape de décision si des activités d'analyse ne sont pas nécessaires
    - demande de réalisation d'un ou plusieurs types d'analyse parmi les analyses d'impact, de coût ou de risque
- réalisation des types d'analyses demandées (analyse d'impact, de coût, de risque)
- décision des gestionnaires des changements pouvant conduire à une clôture si la demande n'est pas approuvée
- réalisation de la demande par un expert IT si la demande est approuvée
- clôture finale

##Phase 1 - création des éléments dans CMDBuild

Via le module d'administration dans l'entrée de menu Processus, le processus DemandeDeChangement est créé de façon à gérer le processus:

![](../workflow_manual/05-Phase_1.png)

Certains attributs fournis dans le processus sont des listes de valeurs (Lookup) qu'il faut préalablement définir comme vous pouvez le voir dans les captures d'écran suivantes.

###Liste de valeurs des catégories de demandes de changement (liée à l'attribut "Catégorie" du processus)

![](../workflow_manual/05-Phase_1-1.png)

###Liste de valeurs de la décision pour la demande de changement (liée à l'attribut "Décision" du processus)

![](../workflow_manual/05-Phase_1-2.png)

###Liste de valeurs du résultat final de demande de changement (liée à l'attribut "Résultat final" du processus)

![](../workflow_manual/05-Phase_1-3.png)

###Liste de valeurs de l'évaluation formelle de la demande de changement (liée à l'attribut "Evaluation formelle" du processus)

![](../workflow_manual/05-Phase_1-4.png)

###Liste des valeurs de priorité de demande de changement (liée à l'attribut "Priorité de la demande" du processus)

![](../workflow_manual/05-Phase_1-5.png)

###Liste des valeurs de l'état de demande de changement (liée à l'attribut "Etat de la demande" du processus)

![](../workflow_manual/05-Phase_1-6.png)

Les "domaines" suivants sont créés de façon à définir dans le processus l'attribut "Demandeur" comme clef étrangère sur la classe "Employé" et les relations avec le gestionnaire des modifications et les experts IT qui, respectivement, évaluent et réalisent les demandes de changement:

![](../workflow_manual/05-Phase_1-7.png)

A ce stade, les attributs du processus peuvent être créés:

![](../workflow_manual/05-Phase_1-8.png)

Dernière chose, vous pouvez créer les groupes d'utilisateurs impliqués dans le processus:

![](../workflow_manual/05-Phase_1-9.png)

A ce stade, vous pouvez exporter le schéma produit par CMDBuild, utilisé avec l'éditeur graphique TWE pour concevoir les flux détaillés du processus lui-même:

![](../workflow_manual/05-Phase_1-10.png)

Le fichier XPDL n'inclura que les données générales disponibles en ce moment:

- nom du processus
- liste des attributs des processus non réservés présent la classe de gestion du processus
- liste des rôles définis dans le système

Ces données seront le point de départ des activités réalisées via l'éditeur TWE dans lequel tous les aspects liés au flux spécifique du processus seront enrichis.

##Phase 2 - Configuration du flux avec TWE

A l'aide de l'éditeur TWE, il est possible de réaliser les opérations suivantes:

- conception du flux en plaçant les activités des divers types fournis (début et fin du processus, activités des utilisateurs, activités automatiques, activités d'aiguillage pour la gestion du parallélisme) et leurs connexions conformément aux types de transition fournis
- finalisation des activités des utilisateurs en indiquant quels attributs du processus seront affichés dans le formulaire lié à cette activité (en indiquant s'ils sont modifiables ou en lecture seule) et quels widgets seront rendus disponibles dans ce même formulaire (en indiquant les paramètres fournis pour chacun)
- finalisation des activités automatiques en écrivant le script qui implémente les automatismes requis dans cette activité (en utilisant l'API disponible dans ce but)
- finalisation des transitions entre les activités, en indiquant les critères pour que le flux aille vers une activité ou une autre quand ce choix est lié.

Voici certaines captures d'écran décrivant les activités décrites ci-dessus.

###Conception générale du flux

![](../workflow_manual/05-Phase_2-1.jpg)

###Activités des utilisateurs - onglet "Variables"

Utilisé pour choisir les attributs qui doivent apparaître dans le formulaire (avec une éventuelle indication de restriction en lecture seule):

![](../workflow_manual/05-Phase_2-2.jpg)

###Activités des utilisateurs - onglet "Attributs étendus"

Utilisé pour indiquer les attributs obligatoires ("UPDATEREQUIRED") et pour demander leur saisie dans le formulaire d'un ou plusieurs widgets (dans l'exemple openAttachments pour les pièces jointes et createModifyCard pour consulter la fiche du demandeur)

![](../workflow_manual/05-Phase_2-3.jpg)

###Activités automatiques - onglet "Type"

Utilisé pour écrire le script qui implémente les automatismes fournis (dans l'exemple, l'activité SYS010 réalise la structuration automatique de la date du système, l'attribution automatique d'un numéro incrémental unique, la construction d'une description significative, la structuration d'un nouvel état atteint par le processus).

![](../workflow_manual/05-Phase_2-4.jpg)

###Transitions

Utilisé pour lier deux activités, de manière conditionnelle ou pas (dans l'exemple, une condition relative à l'acceptation formelle de la demande de changement est indiquée)

![](../workflow_manual/05-Phase_2-5.jpg)

##Phase 3 - Importation du fichier XPDL dans CMDBuild

Quand la configuration du processus dans TWE est achevée, vous chargerez dans CMDBuild le fichier XPDL produit.

Le flux de processus peut être modifié plusieurs fois, en n'exportant que la dernière version de CMDBuild, en le modifiant dans TWE et en l'important de nouveau dans CMDBuild. Vous devez être conscient que la nouvelle version sera utilisée quand de nouveaux processus seront démarrés tandis que les processus en cours continueront avec la version XPDL valide au moment où ils ont démarré.

![](../workflow_manual/05-Phase_3-1.png)

##Phase 4 - Implémentation du processus à partir de CMDBuild

Le processus importé dans CMDBuild est disponible pour être utilisé par les groupes d'opérateurs fournis. Dans l'exemple, le processus de gestion de la demande de changement sera lancé par un opérateur du groupe Service d'assistance, enrichi par un opérateur de groupe Gestionnaires de changement, analysé et réalisé par un opérateur du groupe Experts IT. Vous devez être conscient que les opérateurs du groupe Super utilisateurs peuvent "personnifier" tout autre groupe défini dans CMDBuild.

A partir de la gestion du processus demande de changement, les demandes de changement sont représentés comme ouvertes (ou dans un état choisi dans la liste supérieure: ouverte, suspendue, complétée, interrompue, toutes).

Via le bouton "Démarrer une demande de changement", le service d'assistance peut enregistrer une nouvelle demande.

![](../workflow_manual/05-Phase_4-1.png)

Avant de renseigner le formulaire, l'opérateur peut se référer aux instructions opératives pouvant être associées à chaque activité utilisateur (qui peuvent être renseignées avec TWE, en renseignant le champ "Description" dans l'onglet "Général" de l'activité).

![](../workflow_manual/05-Phase_4-2.png)

En validant le passage à l'étape suivante, l'activité est prise par le gestionnaire de changement qui, dans notre exemple simplifié - fournira les informations suivantes;

![](../workflow_manual/05-Phase_4-3.png)

Dans l'exemple, nous fournissons à cette étape la possibilité d'utiliser les widgets intégrés de chargement et ceux pour la référence à la fiche complète du demandeur:

![](../workflow_manual/05-Phase_4-4.png)

Dans notre exemple, deux types d'analyse ont été demandés au gestionnaire de changement, donc le processus s'orientent vers les experts IT, qui, en parallèle (en utilisant l'une des nouvelles fonctionnalités implémentées dans CMDBuild 2.0), peuvent réaliser leur analyse (respectivement analyse de risque et de coût) et transmettre leurs résultats.

![](../workflow_manual/05-Phase_4-5.jpg)

![](../workflow_manual/05-Phase_4-6.jpg)

Le gestionnaire des changements fournit actuellement les résultats des discussions demandées et peut prendre sa décision.

![](../workflow_manual/05-Phase_4-7.jpg)

Si la décision est positive, selon le flux conçu avec TWE, il est demandé aux experts IT de réaliser l'activité de demande de changement. Au début, l'opération fait la demande en indiquant les activités qui doivent être réalisées; à la fin, elle enregistre les activités déjà réalisées.

![](../workflow_manual/05-Phase_4-8.jpg)

![](../workflow_manual/05-Phase_4-9.jpg)

Comme dernière opération, le gestionnaire de changement clôture la demande de changement en indiquant un résultat positif.

![](../workflow_manual/05-Phase_4-10.jpg)

A ce stade, la demande de changement sur laquelle nous avons travaillé (numéro 7) n'apparaîtra pas dans la liste des demandes de changement ouvertes.

![](../workflow_manual/05-Phase_4-11.jpg)

Mais on peut s'y référer avec toutes ses informations dans la liste des demandes de changement réalisées (la liste peut être choisie dans la partie supérieure du formulaire)

![](../workflow_manual/05-Phase_4-12.jpg)

![](../workflow_manual/05-Phase_4-13.jpg)

En plus des informations de base, vous pouvez vous référer aux relations configurées avec cette instance de processus de demande de changement (onglet Relations).

![](../workflow_manual/05-Phase_4-14.jpg)

Vous pouvez également vous référer à la séquence complète avec les activités d'avancement du processus (onglet Historique).

![](../workflow_manual/05-Phase_4-15.jpg)

Le téléchargement des pièces jointes lors du processus (en utilisant le widget adapté), vous pouvez vous référer aux éventuels documents disponibles (onglet Pèces jointes).

![](../workflow_manual/05-Phase_4-16.jpg)
