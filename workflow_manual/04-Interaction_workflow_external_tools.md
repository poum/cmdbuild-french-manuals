#Interaction du processus avec des outils externes

##Informations générales

Dans certains cas, il peut être nécessaire qu'un processus (comme une nouvelle demande du service d'assistance) soit lancé par un utilisateur qui ne soit pas assez expérimenté pour utiliser CMDBuild (tel que l'utilisateur de l'actif ou du service IT).
Ceci peut être résolu avec deux méthodes d'interopérabilité, décrites ci-dessous.

##Lancer un processus depuis un portail intranet via une portlet JSR68

La première méthode (la plus répandue): CMDBUild fournit l'export de ses fonctionnalités sous forme de Portlet JSR 168.

Les portlets JSR 168 sont des composants web Java, introduits pour le développement d'architectures modulaires, qui peuvent être utilisées comme plugin dans des "conteneurs" tel que des portails web compatibles avec ce standard.

CMDBuild inclut comme solution standard une portlet certifiée avec le système de "Portail" open source Liferay (version 6.0), capable de réaliser certaines fonctionnalités pour l'application principale via une interface utilisateur.

La portlet est capable d'accéder à CMDBuild via un service web, en adaptant la configuration de l'instance liée (modèle de données, processus, menus personnalisés, groupes d'utilisateurs et droits d'accès).

La portlet standard de CMDBuild comporte les fonctions suivants:

- démarrage et avancement d'un processus, avec la consultation des instances actives ou terminées ouvertes par le même utilisateur qui s'est connecté dans Liferay
- gestion des feuilles de données (ajout, modification, suppression)
- exécuter un rapport

Quand vous n'avez pas à étendre le fonctionnement standard fourni, la portlet CMDBuild pour Liferay peut amener une interface utilisateur simplifiée et s'adapte à l'instance liée de CMDBuild sans développement de code pour la gestion de l'interaction entre les deux environnements.

Pour les détails techniques concernant la configuration d'une instance Liferay capable d'interagir avec CMDBuild, voir la section dédiée du manuel technique.

Voici un exemple de portlet pour Liferay, en particulier le démarrage d'un processus.

![](../workflow_manual/04-Liferay.jpg)

##Lancer un processus depuis un portail intranet via un service web

La deuxième méthode consiste à lier un formulaire web à un portail intranet ou une application web à CMDBuild, en appelant les méthodes d'un service web standard.

Via ces méthodes, vous pouvez réaliser les fonctions de base gérées dans le système, dédiées à la gestion et l'impression des fiches et de leurs relations, à la gestion des pièces jointes et de l'exécution des processus.

Le service web permet un accès direct à CMDBuild pour la récupération des informations qui seront affichées dans le formulaire web (valeurs par défaut, listes de valeurs à choisir, etc.).

La communication directe entre le portail et CMDBuild permet également l'inplémentation de fonctions interactives, telles que la liste des références des processus démarrés mais pas encore terminés, la consultation des informations de détail, la recherche avec des critères de recherche paramétrables parmi les processus déjà fermés, etc.

Voici deux exemples de formulaires implémentés sur un portail intranet qui communiquent avec CMDBuild via ses services web standard.

Le premier montre le formulaire de démarrage d'un processus, le second affiche la liste des processus actifs dans CMDBuild et permet la consultation ou le passage à l'étape suivante par un utilisateur connecté au portail intranet.

![](../workflow_manual/04-Portal_ws_1.jpg)

![](../workflow_manual/04-Portal_ws_2.jpg)
