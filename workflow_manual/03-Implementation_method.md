#Méthode d'implémentation

Les processus sont des classes particulières.

Les mécanismes pour la gestion des processus sont implémentés dans CMDBuild via des concepts et des procédures totalement cohérentes avec les mécanismes déjà présents dans le système pour la gestion des fiches.

La gestion des processus comporte:

- classes "spéciales" pour les processus; chacune correspond à un type de processus
- des attributs, correspondant aux informations présentées (en lecture ou écriture) dans des formulaires qui gèrent la réalisation de chacune des étapes du processus
- relations avec d'autres instances de processus ou de fiches classiques impliquées dans le processus
- groupes d'utiisateurs, qui pourront réaliser chaque activité, et qui coïncident avec les groupes d'utilisateurs de CMDBuild
- des outils spéciaux pour la personnalisation du comportement du processus (widgets et scripts écrits avec les APIs adaptées)

Toujours dans le cadre de cette même hpmogénéïté entre les classes "normales" et "de processus", nous avons adopté les tactiques techniques suivantes:

- la nouvelle superclasse "confidentielle" appelée "Activity" contient certains attributs en commun avec les processus spécifiques dont les processus sont les des sous-classes
- le mécanisme d'historique a été utilisé pour concevoir les rapports d'avancement d'un processus
- le mécanisme des "relations" ont été conservés pour créer des liens automatiques ou manuels entre une fiche et une instance de processus ou entre deux instances de processus.

##Construire le processus

Pour créer une nouvelle classe "Processus", vous devez suivre la séquence logique de passages suivantes:

- analyse du nouveau processus qui doit être implémenté, les conditions de transition, etc
    - liste des groupes d'utilisateurs impliqués dans le processus
    - processus: activités des utilisateurs, activités automatiques, conditions de transition, etc
    - attributs descriptifs du processus dans ses activités utilisateurs et les typologies liées (chaînes, entiers, etc) et le mode de présentation (caché, lecture seule, lecture et modification)
    - listes prédéfinies de valeurs requises pour la création d'attributs "Listes de valeurs" ("Lookup")
    - domaines requis pour gérer les corrélations entre le nouveau processus etles autres classes ou d'autres processus pré-existants (qui peuvent également être utilisés pour créer les attributs "référence")
    - widgets à configurer dans chaque activité utilisateur
    - widgets à configurer dans chaque activité de processus automatique
- création d'une nouvelle classe de processus qui sera définie dans la section "Processus" du module d'administration CMDBuild en complétant:
    - attributs spécifiques identifiés à l'étape précédente
    - domaines identifiés dans l'étape précédente
- création des groupes d'utilisateurs manquants qui doivent être ajoutés via le module d'administration
- via le module d'administration (à partir de l'onglet "XPDL" disponible pour chaque classe "Processus") exporter le nouveau modèlede processus qui comporte:
    - nom du processus
    - liste des attributs du processus qui seront placés dans les diverses activités utilisateur
    - liste des "acteurs" (utilisateurs) qui interagissent avec le processus (le rôle "Système" est automatiquement créé pur placer les activités du système
    - conception du flux détaillé du processus en utilisant l'éditeur externe TWE qui aidera à compléter le modèle exporté par CMDBuild
    - enregistrement, en utilisant les fonctions spéciales de l'éditeur externe TWE, du fichier XML (XPDL 2.0) correspondant au processus conçu
    - import du schéma de processus dans CMDBuild en utilisant l'onglet "Propriété" disponible dans l'entrée de menu "Processus" dans le module d'administration

Une fois réalisées les opérations décrites précédemment, le nouveau processus peut être utilisé dans le module de gestion (menu "Processus" ou entrées telles que "processus" dans le menu de navigation) si bien que le processus peut être exécuté en utilisant le moteur de processus Together Workflow Server 4.4.

Les opérations mentionnés ci-dessus peuvent être mises en oeuvre les nouvelles instances de processus qui seront lancées.

![](../workflow_administrator/03-Implementation_method.png)

##Initialisation et avancement d'un processus

Dans le module de gestion, CMDBuild peut réaliser, via le support du moteur TWS Together Workflow Server, les processus conçus avec l'éditeur TWE Together Workflow Editor puis importés via le module d'administration.

De façon à conserver la plus grande cohérence avec les fonctionnalités de CMDBuild, dédiées à la gestion des fiches d'éléments dans le système, l'interface utilisateur du module de gestion a été conçue de telle sorte qu'elle soit cohérente avec la gestion des "classes" de données normales:

- il existe une entrée de menu spéciale "Processus" cohérente avec les "feuilles de données" (d'autres éléments de "processus" peuvent être insérés dans le menu de navigation via des éléments des "feuilles de données" ou des rapports ou des tableaux de bord)
- la gestion des processus s'appuie sur les gestions standard qui existent déjà pour les fiches normales: "Liste", "Fiche", "Détails", "Notes", "Relations", "Historique", "Pièces jointes"
- dans l'onglet "Liste" d'un processus donné, l'utilisateur peut voir les instances d'activités dans lequels il/elle est impliqué(e) (puisqu'il/elle est partie prenante de cette activité ou d'activités précédentes de ce processus) avec:
    - filtres par état (démarré, complété, suspendu)
    - zones de données avec un affichage en tableau des informations (nom de processus, nom d'activité, description de la demande, état du processus et d'autres attributs définis comme "affichés de base" dans le module d'administration) sur lesquelles vous pouvez cliquer de façon à accéder à la gestion de la fiche de cette activité
    - preuves éventuelles des activités parallèles pour cette instance de processus
    - bouton pour créer une nouvelle activité ou pour faire ce choix
- dans l'onglet "FIche", vous pouvez voir ou renseigner les attributs fournis pour cette instance d'activité de processus (l'accès en modification ou en lecture seule peut être défini via l'éditeur TWE), autrement vous pouvez mener d'autres opérations via les widgets appropriés (contrôles visuels) configurés avec l'éditeur TWE
- dans l'onglet "Notes", vous pouvez voir ou insérer des note concernant l'instance d'activité
- dans l'onglet "Relations", vous pouvez voir ou insérer des relations entre l'instance d'activité et les instances des autres classes ("Fiches")
- dans l'onglet "Historique", vous pouvez voir les versions précédentes de cette instance d'activité (instances déjà réalisées)

La liste des activités est affichée en haut dans le formulaire d'exemple suivant tandis que vous pouvez une activité en remplissant la fiche dans la zone du bas.

![](../workflow_administrator/03-Initiation_progress.png)

   


##
