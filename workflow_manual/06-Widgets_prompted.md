#Widgets présentés pour être utilisés dans les activités utilisateur du processus

##Liste des widgets

CMDBuild rend disponibles certains widgets (contrôles visuels) qui sont placés dans la partie droite du formulaire et qui gèrent l'avancement du processus via l'activité fournie.

Graphiquement, ces contrôles sont réalisés à l'aide de boutons dotés des labels indiqués lors de l'étape de définition.

En ce qui concerne la configuration, ils sont définis comme "attributs étendus" (fournis par le standard XPDL) à l'aide de l'éditeur TWE.

Dans ce document, les types de données sont à la fois des types de base (entier, chaîne de caractères, date, nombre flottant, booléen) ou de type complexe ajoutés dans les processus de CMDBuild (liste de valeurs = identifiant + nom, listes de valeurs = tableau de listes de valeurs, référence = identifiant + identifiant de classe + description, références = tableau de références).

----------------------------------------------------------------------------------
Contrôle visuel   Description                           Paramètres         Notes  
----------------- ------------------------------------- ------------------ ------- 
 manageRelation  Il affiche liste des fiches             Entrée:
                   (qui peuvent être sélectionnées)
                   en relation avec la fiche choisie     NomDomaine chaîne
                   conformément au domaine choisi        NomClasse chaîne
                                                         ObjId entier
                                                         LabelBouton chaîne
                                                         FonctionsActivées tableau de caractères
                                                         Obligatoire entier
                                                         estDirect chaîne

                                                         ou

                                                         Entrée:

                                                         NomDomaine chaîne
                                                         ObjRef référence
                      
                                                         LabelBouton chaîne
                                                         FonctionsActivées  tableau de caractères
                                                         Obligatoire entier

                                                         Sortie:
                                                         TableauDeContrôle références
                                                        
-------------------------------------------------------
