OPTIONS=--toc -t html5 -s
OUTRO=appendix/*

user: build intro/* user_manual/* ${OUTRO}
	pandoc ${OPTIONS} -o build/user_manual.html intro/*.md user_manual/*.md ${OUTRO}.md

administrator: build intro/* administrator_manual/* ${OUTRO}
	pandoc ${OPTIONS} -o build/administrator_manual.html intro/*.md administrator_manual/*.md ${OUTRO}.md
	pandoc ${OPTIONS} -o build/administrator_manual.odt intro/*.md administrator_manual/*.md ${OUTRO}.md

overview: build intro/* overview/* ${OUTRO}
	pandoc ${OPTIONS} -o build/overview.html intro/*.md overview/*.md ${OUTRO}.md

webservices: build intro/* webservices_manual/* ${OUTRO} 
	pandoc ${OPTIONS} -o build/webservices_manual.html intro/*.md webservices_manual/*.md ${OUTRO}.md

technical: build intro/* technical_manual/* ${OUTRO}
	pandoc ${OPTIONS} -o build/technical_manual.html intro/*.md technical_manual/*.md ${OUTRO}.md

workflow: build intro/* workflow_manual/* ${OUTRO}
	pandoc ${OPTIONS} -o build/workflow_manual.html intro/*.md workflow_manual/*.md ${OUTRO}.md

all: user administrator webservices technical workflow

build:
	mkdir build

clean:
	rm build/*
